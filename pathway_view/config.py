import psycopg2
import psycopg2.extras
import re
import random




''' -------------------- Cookie setup --------------------------------------------------'''
OMICXVIEW_COOKIE_AUTH_NAME = 
PRE_SALT = 
POST_SALT = 

'''--------------------- Front end choices ---------------------------------------------'''
# If you change this, need to update it in the options.js file for the display
# doesn't automatically flow through
MOLECULE_DISPLAY_INFO = ['bigg_id', 'bigg_name', 'inchi_hmdb', 'charge', 'mass', 'definition', 'formulae', 'bigg_paths', 'hmdb_generic_name', 'inchi_str', 'metanetx_id', 'hmdb_synonyms', 'info']


''' --------------------- Global codes/error codes which are returned to users ----------'''
# Indicies in the tables where values are stored
ID = 0
NAME = 1
EMAIL = 2
PASSWORD = 3
ROLE = 4

# Error codes used in the Auth model
error_codes = {'id': "ID was not of int type", 'privileges': "Must be an admin to perform this action", 'name': "Error: Names can only contain numbers and letters", 'password': "Please enter a valid password", 'email': "Please enter a valid email", 'email_or_password': "Error with either email or password entered, please try again"}


# Supported ID types (shown to a user)
SUPPORTED_METABOLITE_ID_TYPES =  ['bigg_id', 'bigg_name', 'bigg_paths', 'hmdb_generic_name', 'hmdb_synonyms', 'inchi_str', 'inchi_hmdb', 'metanetx_id']

# Supported dataset types
SUPPORTED_DATASET_TYPES = ['metabolic']

# Database cols
PRIVATE = 'private'
PUBLIC = 'public'

# Mapping ID's which we store these aren't yet shown on the user
DATABASE_METABOLITE_ID_TYPES = ['metanetx_id', 'kegg_id', 'chebi_id', 'bigg_id', 'inchi_str', 'inchi_hmdb', 'metanetx_id']

#ID_TYPES_DICT = {'kegg_id': 12, 'chebi_id': 0, 'bigg_id': 13, 'inchi_key': 9, 'inchi_str': 10, 'names': 11, 'secondary_chebi_id': 2}

# Chebi information properties
CHEBI_INFO_PROPERTIES = ['name', 'definition', 'mass', 'monoisotopic_mass', 'charge', 'formulae', 'last_modified']

#CHEBI_INFO_DICT = {'name': 1, 'definition': 3, 'mass': 4, 'monoisotopic_mass': 5, 'charge': 6, 'formulae': 7, 'last_modified': 8}

''' --------------------- Default visualisation values -------------------------'''

# Default pathway ID
DEFAULT_PATHWAY_ID = 48

PATH = '/var/www/pathway_viewer/pathway_viewer/pathway_view/uploads/'

# Path to the dictoinary which contains extra infor for bigg ID's for example, the 
# chebi mappings and inchi strings
BIGG_DICT_PATH = "public/data/bigg_mapping_dict.json"

''' --------------------- Authentication config items ----------------------------'''
# public group ID
PUBLIC_GROUP_ID = 231

# public user ID 
GUEST_USER_ID = 390

# Database connection string
psycopg2_conn_string = 


''' --------------------- Project directory and path config items ----------------'''

# HTML web path
WEB_PATH = 'https://dev-ariane-s4m.stemformatics.org'

PROJ_PATH = ''

''' --------------------- REGREX config items -------------------------------------'''

# Regrex's
EMAIL_REGREX = re.compile(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
NAME_REGREX = re.compile(r"^[A-Za-z0-9+ ]*$")


# Email and random code gen
FROM = 'omicxview@pathwayviewer.com'
MINCODE = 1000
MAXCODE = 9999



''' --------------------- Global codes used within the program for telling types ------'''

VARCHAR = 255
FLOAT = 3



''' --------------------- Metabolite list config items ------------------------'''

# Table fields for metabolite lists (headers) in the database replace dash
# for underscore need to update this in the variables on the front end if it gets
# updated here.
METABOLITE_LIST_HEADER = ['name', 'p_value', 't_statistic', 'z_score', 'bh_adjusted_p_value', 'fold_change', 'standard_error', 'comments', 'raw_metabolite_id', 'raw_metabolite_id_type']


# These are the headers which the user will have access to, they are returned
# from the JSON calls of adding data to pathways
# id isn't included
METABOLITE_LIST_METADATA_MAP = {'name':1, 'owner_id':2, 'owner_type':3, 'notes':4, 'private': 5}

# list_id and id aren't included
METABOLITE_LIST_HEADER_MAP = {'chebi_id':2, 'raw_metabolite_id': 3, 'raw_metabolite_id_type':4, 'name':5, 'p_value':6, 't_statistic':7, 'z_score':8, 'bh_adjusted_p_value':9, 'fold_change':10, 'standard_error':11, 'comments':12}


# Types associated
METABOLITE_LIST_HEADER_TYPES = {'chebi_id':VARCHAR, 'raw_metabolite_id':VARCHAR, 'raw_metabolite_id_type':VARCHAR, 'name':VARCHAR, 'p_value':FLOAT, 't_statistic':FLOAT, 'z_score':FLOAT, 'bh_adjusted_p_value':FLOAT, 'fold_change':FLOAT, 'standard_error':FLOAT, 'comments':VARCHAR}



