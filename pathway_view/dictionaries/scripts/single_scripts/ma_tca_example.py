import csv
import re

from pathway_view.config import *

chebi_id = "ChEBI ID"
chebi_name = "ChEBI Name"
definition = "Definition"
mass = "Mass"
charge = "Charge"
formula = "Formulae"
date_mod = "Last Modified"
bigg_id = 'BiGG ID'

save_path = "ma_tca_cycle_matches.tsv"
save_path_bigg = "bigg_only_ma_tca_cycle_matches.tsv"

input_names = {'2ketoglutarate':'2-Ketoglutarate', 'acetylcoa':'Acetyl-CoA', 'citrate':'Citrate', 'fumarate':'Fumarate', 'oxaloacetate':'Oxaloacetate', 'pyruvate':'Pyruvate', 'malate':'Malate', 'isocitrate':'Isocitrate'}
output_chebi = open(save_path, 'w')
chebi_file_writer = csv.writer(output_chebi, delimiter='\t')
chebi_file_writer.writerow(['raw input name', 'parsed input name', chebi_id, chebi_name, bigg_id, definition, mass, charge, formula, date_mod])

output_bigg = open(save_path_bigg, 'w')
bigg_file_writer = csv.writer(output_bigg, delimiter='\t')
bigg_file_writer.writerow([bigg_id, mass, charge])

conn = psycopg2.connect(psycopg2_conn_string)
cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

sql = "select distinct cm.chebi_id, ci.other_id, cm.other_id from chebi_mapping_to_other_id as cm left join chebi_mapping_to_other_id as ci on ci.chebi_id=cm.chebi_id where cm.other_id in ('2ketoglutarate', 'acetylcoa', 'citrate', 'fumarate', 'oxaloacetate', 'pyruvate', 'malate', 'isocitrate') and ci.other_id_type ='bigg_id';"
cursor.execute(sql)
bigg_chebi_name_matches = cursor.fetchall()

# chebi_id in first collumn, bigg_id in second column, name in third
# Get the relevant information for the user for each of the chebi ID's
for m in bigg_chebi_name_matches:
    c_id = m[0]
    sql = "select distinct chebi_id, name, mass, charge, formulae, definition, last_updated_date from chebi_info where chebi_id = %s;"
    cursor.execute(sql, (c_id,))
    c_info = cursor.fetchall()
    c_info = c_info[0]
    chebi_file_writer.writerow([input_names[m[2]], m[1], c_id, c_info[1], m[1], c_info[2], c_info[3], c_info[4], c_info[5], c_info[6]])
    bigg_file_writer.writerow([m[1], c_info[2], c_info[3]])

cursor.close()
conn.close()

