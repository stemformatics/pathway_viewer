import csv
import json


"""
Script to merge on the neutral inchi string.
"""

def add_to_inchi_dict(filename, id_type, inchi_dict):
    """
    Adds external reference mappings to a dictionary object.
    Each id_type has a list associated with a particular inchi string.
    
    :param filename: filepath to a TSV file containing neutral inchi 
                    strings and a database reference to this i.e. hmdb
    :param id_type: the type of id stored in the TSV file in addition to
                    the inchi string (i.e. hmdb)
    :param inchi_dict: a dictionary storing the mappings to external references.
    :return: inchi_dict
    """
    first = True
    with open(filename, 'r') as f:
        f = csv.reader(f, delimiter='\t')
        for row in f:
            if first:
                first = False
            else:
                inchi_str = row[0]
                other_id = row[1]
                if inchi_str == "InChI=1S/C4H12N2/c5-3-1-2-4-6/h1-6H2":
                    print(other_id)
                """
                Need to first check if we already have an entry
                if so add to the list of IDs.
                """
                try:
                    has_entry_for_inchi_str = inchi_dict[inchi_str]
                    try:
                        has_entry_for_id_type = inchi_dict[inchi_str][id_type]
                    except:
                        inchi_dict[inchi_str][id_type] = []
                except:
                    inchi_dict[inchi_str] = {}
                    inchi_dict[inchi_str][id_type] = []

                inchi_dict[inchi_str][id_type].append(other_id)
        return inchi_dict

def merge_files(input_files):
    """
    Merges a number of files with ID information.
    Files need to follow the format:
        row[0] = header
        col0 has the neutral inchi string
        col1 has the other id
    
    :param  input_files: is a dictionary with the id_type as the key and
        the file path as the value.
        
    :return  inchi_dict: a dictionary with neutral inchi strings as keys and
        a list for each id type that had a reference to that inchi string.
    """
    inchi_dict = {}
    for id_type in input_files:
        inchi_dict = add_to_inchi_dict(input_files[id_type], id_type, inchi_dict)

    return inchi_dict


def save_file(file_path, inchi_dict):
    """
    Dumps the inchi dictionary to a JSON file for easy access.
    """
    with open(file_path, 'w') as fp:
        json.dump(inchi_dict, fp)



def save_external_mappings_to_chebi_id(inchi_dict, base_dir, json_dump_path):
    """
    For each entry in the inchi dict there should be a one to one
    mapping with ChEBI. We are keeping ChEBI as the underlying dictionary
    so we want to add the mappings to the DB. Easiest way is a TSV
    file with the neutral ChEBI as the identifier and the other_id 
    as the value.
    """
    external_refs = ['inchi_hmdb', 'metanetx_id', 'bigg_id', 'bigg_name', 'bigg_paths', 'hmdb_generic_name', 'hmdb_synonyms']
    # Create a file for each of the external references and store in the
    # writers object with the name as the key.
    writers = {}
    ex_count = 0
    count_with_chebi = 0
    # make a json dict storing the mappings
    json_dict = {}
    # tag for the species to keep track of the name
    species_tag = {"H": "Homo sapiens", "E": "Escherichia coli", "S": "Saccharomyces cerevisiae"}
    for x_ref in external_refs:
        tmp_writer = open(base_dir + x_ref + ".tsv", 'w')
        tmp_writer = csv.writer(tmp_writer, delimiter='\t')
        tmp_writer.writerow(["chebi_id", x_ref])
        writers[x_ref] = tmp_writer

    x_ref = 'inchi_str'
    tmp_writer = open(base_dir + x_ref + ".tsv", 'w')
    tmp_writer = csv.writer(tmp_writer, delimiter='\t')
    tmp_writer.writerow(["chebi_id", x_ref])
    writers[x_ref] = tmp_writer

    for inchi_str in inchi_dict:
        value = inchi_dict[inchi_str]
        try:
            chebi_id = value['chebi_id'][0] # There should just be the one ChEBI ID.
            count_with_chebi +=1
            had_bigg_entry = False
            had_hmdb_entry = False

            for x_ref in external_refs:
                # Not all references may have had an entry
                try:
                    refs = value[x_ref]
                    """
                    If it is the bigg id we want to save it to the json file
                    """
                    if x_ref == 'bigg_id':
                        json_dict[chebi_id] = refs
                        had_bigg_entry = True
                    if x_ref == 'inchi_hmdb':
                        had_hmdb_entry = True
                    for other_id in refs:
                        writers[x_ref].writerow([chebi_id, other_id])
                    # Also want to write out the inchi string
                    writers['inchi_str'].writerow([chebi_id, inchi_str])
                except:
                    silently_fail = True # It is expected to fail many times - not an issue.

            if had_bigg_entry:
                had_bigg_entry = False
                # try to see if it had a hmdb entry
                if had_hmdb_entry == False:
                    hmdb = ""
                else:
                    hmdb = ", ".join(value["inchi_hmdb"])
                # Want a separate row for each of the pathways
                chebi_id = ", ".join(value['chebi_id'])
                bigg_name = ", ".join(value['bigg_name'])
                bigg_id = ", ".join(value['bigg_id'])
                added_paths = []
                for path in value['bigg_paths']:
                    path_type = species_tag[path[0]]
                    if path[1:] not in added_paths:
                        added_paths.append(path[1:])
                        mapping_writer.writerow(
                            [chebi_id, hmdb, bigg_id, bigg_name, path[1:], path_type, inchi_str])

        except:
            ex_count += 1
            #print("no chebi id for entry:", value, inchi_str)
    # Dump the json to a file
    with open(json_dump_path, 'w') as fp:
        json.dump(json_dict, fp)
    print(ex_count, count_with_chebi)


parsed_db_dir = "parsed_db_outputs/"
db_dump_dir = "database_dumps/"
base_dir = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/"

metanetx_file = base_dir + parsed_db_dir + "neutral_inchi_to_metanetx.tsv"
bigg_file = base_dir + parsed_db_dir + "neutral_inchi_to_bigg.tsv"
hmdb_file = base_dir + parsed_db_dir + "neutral_inchi_to_hmdb.tsv"
chebi_file = base_dir + parsed_db_dir + "neutral_inchi_to_chebi.tsv"
bigg_paths = base_dir + parsed_db_dir + "neutral_inchi_to_bigg_paths.tsv"
bigg_name = base_dir + parsed_db_dir + "neutral_inchi_to_bigg_name.tsv"
hmdb_name = base_dir + parsed_db_dir + "neutral_inchi_to_hmdb_name.tsv"
hmdb_syno = base_dir + parsed_db_dir + "neutral_inchi_to_hmdb_syno.tsv"

mapping_writer = open(base_dir + parsed_db_dir + "merged_on_inchi.tsv", 'w')
mapping_writer = csv.writer(mapping_writer, delimiter='\t')
mapping_writer.writerow(["chebi_id", "hmdb", "bigg_id", "bigg_name", "bigg_paths", "path_species_tag", "inchi_string"])


json_dump_path =  base_dir + parsed_db_dir + "inchi_merged.json"

# merge the files
inchi_dict = merge_files({'chebi_id': chebi_file, 'inchi_hmdb': hmdb_file, 'metanetx_id': metanetx_file, 'bigg_id': bigg_file, 'bigg_name': bigg_name, 'bigg_paths': bigg_paths, 'hmdb_generic_name': hmdb_name, 'hmdb_synonyms': hmdb_syno})

save_file(json_dump_path, inchi_dict)
save_external_mappings_to_chebi_id(inchi_dict, base_dir + parsed_db_dir, json_dump_path)
print(len(inchi_dict))
