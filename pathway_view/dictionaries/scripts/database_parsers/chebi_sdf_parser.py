import csv
import time
import sys
import os.path
import time
import json
import re


"""
Have a root folder where the files are output.
There will be 1 file output with the ChEBI metrics.
Columns are:
    1. Charge
    2. Mass
    3. Definition
    4. Monoisotopic Mass
    5. Name
    6. Formula
    
For each external reference a new file will be created
with the file name as the reference identifier (e.g. KEGG)
Files are:
    1. Names                (synonyms + ChEBI name)
    2. KEGG
    3. InCHI string         (inchi_str)
    4. InCHI Key            (inchi_key)
    5. Secondary ChEBI ID   (chebi_id)
    6. Smiles               (smiles)
"""
parsed_db_dir = "parsed_db_outputs/"
db_dump_dir = "database_dumps/"
base_dir = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/"

save_path = base_dir + parsed_db_dir + "chebi_info.tsv"

save_path_inchi = base_dir + parsed_db_dir + "neutral_inchi_to_chebi.tsv"

sdf_path = base_dir + db_dump_dir + "ChEBI_complete_3star_10072017.sdf"

"""
The SDF file needs to be parsed, to make this fast only the lines
of interest are looked at (i.e. charge, mass etc).
These have been limited to substrings (first 6 characters) as this was
the minimum to keep each identifier unique.
"""
# Lines of interest in sdf file
chebi_id_l = "> <ChEBI ID>"
chebi_name_l = "> <ChE"
chebi_charge_l = "> <Cha"
definition_l = "> <Def"
mass_l = "> <Mas" # Mass
mono_mass_l = "> <Mon" # Monoisotopic Mass
syno_l = "> <Syn" # Synonyms
kegg_l = "> <KEG" # Kegg database links
inchi_l = "> <InC" # Inchi key and Inchi string
sec_l = "> <Sec" # Secondary chebi id
date_l = "> <Las" # Date last modified
form_l = "> <For" # Formula
smiles_l = "> <SMI" # SMILES for the chebi id
hmdb_l = "> <HMD" # HMDB

# To make it human readable map to something easier to
# read.
chebi_id = "ChEBI ID"
chebi_name = "name"
definition = "Definition"
mass = "Mass"
m_mass = "Monoisotopic Mass"
charge = "Charge"
formula = "Formulae"
date_mod = "Last Modified"

# The names which corrospond to the other_id_type in the database
syno = "names"
kegg_c = "kegg"
secondary_id = "chebi_id"
inchi_id = "inchi_key"
inchi_str = "inchi_str"
smiles = "smiles"
hmdb = "hmdb"

external_refs = [syno, secondary_id, inchi_id, inchi_str, smiles, hmdb, kegg_c]
# Create a file for each of the external references and store in the
# writers object with the name as the key.
writers = {}
for x_ref in external_refs:
    tmp_writer = open(base_dir + x_ref + ".tsv", 'w')
    tmp_writer = csv.writer(tmp_writer, delimiter='\t')
    tmp_writer.writerow(["chebi_id", x_ref])
    writers[x_ref] = tmp_writer

# Make a file to save the neutral inchi string and neutral chebi id
# this should be a one to one mapping.
inchi_to_neutral_chebi_writer = open(save_path_inchi, 'w')
inchi_to_neutral_chebi_writer = csv.writer(inchi_to_neutral_chebi_writer, delimiter='\t')
inchi_to_neutral_chebi_writer.writerow(["neutral_inchi_str", "chebi_id"])

sdf_line_dict = {hmdb_l: hmdb, smiles_l: smiles, sec_l: secondary_id, form_l: formula, date_l: date_mod, chebi_charge_l:charge, chebi_name_l:chebi_name, definition_l:definition, mass_l:mass, mono_mass_l:m_mass, syno_l:syno, kegg_l:kegg_c, inchi_l: inchi_id}

# store a dictionary of chebi to Inchi strings
chebi_inchi_dict = {}

"""
Write the first row to the chebi_info file using the headers for collumn names.
This is only the information specific to the molecule (not external references).
"""
output_chebi = open(save_path, 'w')
chebi_file_writer = csv.writer(output_chebi, delimiter='\t')
chebi_file_writer.writerow([chebi_id, chebi_name, definition, mass, m_mass, charge, formula, date_mod])


"""
Writes the chebi dictionary created when parsing to the chebi information file
and creates a mapping file for each of the external references.

Outputs:
    1. ChEBI information file with information listed above.
    2. Files for each of the external references.
            -> each of these files has the ChEBI ID that this maps to
            in the first collumn and the external ref in the second.
            The header is the reference type (i.e. KEGG)
    3. A TSV file with the neutral inchi string as the key and the neutral ChEBI 
       as the value.
       
Note: only ChEBI IDs which have an InCHI string are kept as this is 
necessary if we are to perform mappings from BiGG IDs later on.

"""
def write_chebi_dict_to_file(chebi_dict):
    count_err1 = 0
    count_err2 = 0
    count_inchi = 0
    count_total = 0
    count_charged = 0
    # compile the regex to be used for getting the neutral inchi string
    pattern = re.compile("\/p[-+]\d")
    for chebi_id in chebi_dict:
        value = chebi_dict[chebi_id]
        chebi_id = chebi_id.split(":")[1]
        try:
            """"
            1. Check if we have an inchi string. 
                -> If not then we aren't interessted in this identifier.
            2. Is charge == 0?
                -> yes: store the external refs and chebi info
                -> no: 
                    a. Search for neutral chebi id based on inchi str
                    b. If there is a neutral chebi:
                        1. Map the charged external refs to the neutral chebi 
                        2. Map the charged chebi ID to the neutral chebi
            """
            inchi_str_n = value[inchi_str][0]
            charge_n = value[charge][0]
            neutral_chebi = True
            neutral_chebi_not_found = False
            count_total += 1
            if charge_n != '0':
                count_charged += 1
                neutral_chebi = False
                inchi_str_neutral = pattern.sub("", inchi_str_n)
                # store the charged chebi ID as we'll add this to the external
                # mappings if there exists a neutral chebi ID for it.
                charged_chebi_id = chebi_id
                try:
                    chebi_id = chebi_inchi_dict[inchi_str_neutral].split(":")[1]
                    # Add a reference from the charged chebi to neutral chebi in the mapping file
                    writers[secondary_id].writerow([chebi_id, charged_chebi_id])
                    count_inchi += 1
                except:
                    neutral_chebi_not_found = True
            """
            For each of the external references, see whether or not
            the SDF file had an entry. They are stored as lists as there
            may be one to many mappings.
            
            For each external reference we want to add a row to the 
            reference file for that external reference type (i.e. kegg.tsv).
            """
            if not neutral_chebi_not_found:
                for x_ref in external_refs:
                    try:
                        refs = value[x_ref]
                        for found_ref in refs:
                            writers[x_ref].writerow([chebi_id, found_ref])
                    except:
                        # silently fail as we expect lots of referneces to be missing
                        # i.e. not all chebi entries have hmdb references for example.
                        quietly_fail = True

            """
            For each of the entries into the chebi info file we want to ensure that these
            are not going to cause an error when we are writing to the file, so we need
            to check each one for existance and if it doesn't exist, assign an empty string.
            """
            if neutral_chebi:
                # if it is the neutral chebi we want to write that inchi string and chebi
                # to the TSV file
                inchi_to_neutral_chebi_writer.writerow([inchi_str_n, chebi_id])
                try:
                    definition_n = value[definition][0]
                except:
                    definition_n = ""
                try:
                    mass_n = value[mass][0]
                except:
                    mass_n = ""
                try:
                    mono_mass_n = json.dumps(value[m_mass])
                except:
                    mono_mass_n = ""
                try:
                    date_mod_n = value[date_mod][0]
                except:
                    date_mod_n = ""
                try:
                    formula_n = value[formula][0]
                except:
                    formula_n = ""
                try:
                    # If it has no name or ID we don't store it as it doesn't have enough information
                    chebi_file_writer.writerow(
                        [chebi_id, value[chebi_name][0], definition_n, mass_n, mono_mass_n, charge_n, formula_n,
                         date_mod_n])
                    writers['names'].writerow([chebi_id, value[chebi_name][0]])
                except:
                    # If we weren't able to add it owing to an error in writing to the file we want
                    # to record that.
                    count_err2 += 1
        except:
            count_err1 += 1

    print(count_err1, count_err2, count_inchi, count_charged, count_total)


def parse_chebi_file(sdf_path):
    # See which is quicker
    start_time = time.time()
    next_line_info = False
    line_type = None
    curr_chebi_id = None
    chebi_dict = {}
    with open(sdf_path) as f:

        for line in f:
            # Only care about certain lines make as quick as possible
            # Just check the first character first
            if line[0] == '>':
                # Now we know the line following might be of interest
                # check if it is one we want to look at
                try:
                    line_type = sdf_line_dict[line[:6]]
                    next_line_info = True
                    # check if it is a chebi id
                    if line[:10] == "> <ChEBI I":
                        line_type = chebi_id
                    # Check if it is an inchi key or inchi id
                    elif line_type == inchi_id:
                        if line[:9] == "> <InChIK":
                            line_type = inchi_id
                        else:
                            line_type = inchi_str
                except:
                    next_line_info = False
                    line_type = None
                    pass
            elif line == "\n":
                pass
            # If we don't have a > maybe it is a line of interest
            elif next_line_info:
                tmp_info = []
                if line_type == chebi_id:
                    curr_chebi_id = line[:-1]
                try:
                    temp = chebi_dict[curr_chebi_id]
                    try:
                        tmp_info = temp[line_type]
                    except:
                        tmp_info = []
                except:
                    temp = {}
                # If it is synonyms there are multiple so we want to join them
                # Same with KEGG (just incase)
                tmp_info.append(line[:-1])
                temp[line_type] = tmp_info
                chebi_dict[curr_chebi_id] = temp
                """
                If we have an inchi string we want to save this to use for mapping charged versions
                of chebi IDs to their neutral version.
                """
                if line_type == inchi_str:
                    chebi_inchi_dict[line[:-1]] = curr_chebi_id

    end_time = time.time()
    print("Elapsed time (ms) was" + str((end_time - start_time) * 100))
    return chebi_dict


chebi_dict = parse_chebi_file(sdf_path)
write_chebi_dict_to_file(chebi_dict)
