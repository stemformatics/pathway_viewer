import csv
import re

parsed_db_dir = "parsed_db_outputs/"
db_dump_dir = "database_dumps/"
base_dir = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/"

save_path = base_dir + parsed_db_dir + "neutral_inchi_to_hmdb.tsv"
save_path_name = base_dir + parsed_db_dir + "neutral_inchi_to_hmdb_name.tsv"
save_path_syno = base_dir + parsed_db_dir + "neutral_inchi_to_hmdb_syno.tsv"

sdf_path = base_dir + db_dump_dir + "hmdb_structures_01072017.sdf"


def parse_hmdb_sdf_file(sdf_path):
    """
    Function to parse the hmdb sdf file and make a TSV file of the inchi string.
    Saves the relationships as one to many, i.e. a neutral inchi string may reference
    multiple HMDB ids (i.e. there may be various charges associated with the metabolite)
    Each relationship will have a row:
    
    Output file:
        col0: neutral_inchi_str
        col1: hmdb_id
        
    :param sdf_path: 
    :return: metrics from the sdf file as a dictionary:
    
    """
    inchi_str_key_sdf = "> <INCHI_I"
    hmdb_key_sdf = "> <DATABASE_I"
    hmdb_name_sdf = "> <GENERIC"
    hmdb_syno_sdf = "> <SYNONYM"

    pattern = re.compile("\/p[-+]\d")
    neutral_inchi_to_hmdb = {}
    neutral_inchi_to_name = {}
    neutral_inchi_to_syno = {}

    # Need to keep track of when to add a new row
    next_line_info = False
    next_line_hmdb_id = False

    inchi_line = "i"
    name_line = "n"
    syno_line = "s"

    # Keep track of counts for the metrics
    hmdb_count = 0
    inchi_count = 0
    with open(sdf_path, 'r') as f:
        current_hmdb = ""
        for line in f:
            # Only care about certain lines make as quick as possible
            # Just check the first character first
            if line[0] == '>':
                if line[:10] == inchi_str_key_sdf:
                    inchi_count += 1
                    next_line_info = inchi_line

                elif line[:10] == hmdb_name_sdf:
                    next_line_info = name_line

                elif line[:10] == hmdb_syno_sdf:
                    next_line_info = syno_line
        
                elif line[:13] == hmdb_key_sdf:
                    next_line_hmdb_id = True

            # The next line contains the inchi string so we want to add this to
            # our dictionary.
            elif next_line_info == inchi_line:
                current_inchi_str = line[:-1]
                next_line_info = False
                # Look if there already exists an entry with a neutral inchi string
                # as it may be a one to many mapping.
                inchi_str_neutral = pattern.sub("", current_inchi_str)
                try:
                    hmdbs = neutral_inchi_to_hmdb[inchi_str_neutral]
                except:
                    neutral_inchi_to_hmdb[inchi_str_neutral] = []
                neutral_inchi_to_hmdb[inchi_str_neutral].append(current_hmdb)

            # Next line contains HMDB so we set the current HMDB id to be this line
            # this will precede the InCHI string for this current HMDB identfier if one
            # exists.
            elif next_line_hmdb_id:
                next_line_hmdb_id = False
                current_hmdb = line[:-1]
                hmdb_count += 1
                # Reset the inchi string
                current_inchi_str = ""
            
            # Otherwise it is a name or synonym and we already have the inchi string
            # associated.
            elif next_line_info == syno_line and current_inchi_str != "":
                next_line_info = False
                try:
                    synos = neutral_inchi_to_syno[inchi_str_neutral]
                except:
                    neutral_inchi_to_syno[inchi_str_neutral] = []
                # Want to add the list of synonyms they are separated by a semi colon.
                neutral_inchi_to_syno[inchi_str_neutral].extend(line[:-1].split(';'))

            elif next_line_info == name_line and current_inchi_str != "":
                next_line_info = False
                try:
                    hmdbs = neutral_inchi_to_name[inchi_str_neutral]
                except:
                    neutral_inchi_to_name[inchi_str_neutral] = []
                neutral_inchi_to_name[inchi_str_neutral].append(line[:-1])


    neutral_inchi_count = 0
    hmdb_to_neutral_inchi_count = 0
    name_to_neutral_inchi_count = 0
    syno_to_neutral_inchi_count = 0

    for neutral_inchi in neutral_inchi_to_hmdb:
        neutral_inchi_count += 1
        for hmdb in neutral_inchi_to_hmdb[neutral_inchi]:
            hmdb_to_neutral_inchi_count += 1
            file_writer.writerow([neutral_inchi, hmdb])

    for neutral_inchi in neutral_inchi_to_name:
        neutral_inchi_count += 1
        for name in neutral_inchi_to_name[neutral_inchi]:
            name_to_neutral_inchi_count += 1
            file_writer_n.writerow([neutral_inchi, name])

    for neutral_inchi in neutral_inchi_to_syno:
        neutral_inchi_count += 1
        for syno in neutral_inchi_to_syno[neutral_inchi]:
            syno_to_neutral_inchi_count += 1
            file_writer_s.writerow([neutral_inchi, syno])


    return {"Number of HMDB entries": hmdb_count, "Number of HMDB entries with an InCHI string": inchi_count, "Number of neutral inchi strings": neutral_inchi_count, "Number of HMDB entries with names":name_to_neutral_inchi_count, "Number of HMDB entries with synos": syno_to_neutral_inchi_count}

"""
Variable declarations
"""

output = open(save_path, 'w')
file_writer = csv.writer(output, delimiter='\t')
file_writer.writerow(["neutral_inchi_str", "hmdb_id"])

output_name = open(save_path_name, 'w')
file_writer_n = csv.writer(output_name, delimiter='\t')
file_writer_n.writerow(["neutral_inchi_str", "hmdb_name"])

output_syno = open(save_path_syno, 'w')
file_writer_s = csv.writer(output_syno, delimiter='\t')
file_writer_s.writerow(["neutral_inchi_str", "hmdb_syno"])

# Run the function
print(parse_hmdb_sdf_file(sdf_path))
