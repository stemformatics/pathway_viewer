import csv
import re
import json

from pathway_view.config import *


chebi_info = {'chebi_id': 0, 'name': 1, 'definition': 2, 'mass': 3, 'mono_mass': 4, 'charge': 5, 'formulae': 6, 'last_modified': 7}

# data path
#parsed_chebi_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/TCA_parsed_bigg_to_chebi_db_txt_04052017.tsv"

def parse_chebi_to_db(parsed_chebi_path):    
    # read the contents of the dictionary file into the database
    with open(parsed_chebi_path, 'r') as file_in:
        file_reader = csv.reader(file_in, delimiter='\t')

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        count_none = 0

        first = True
        for row in file_reader:
            # chebi id's have the CHEBI: at the start we'll remove this
            # can add back on later
            if row[0] == 'ChEBI ID':
                print(row)
            else:
                chebi_id = row[chebi_info['chebi_id']]
                # Try to parse the mass, if there are multiple masses it is classed
                # as ambiguous and we'll store it as None (user will have to go look it up
                try:
                    mass = float(row[chebi_info['mass']])
                except:
                    mass = None
                # For each row we want several things. 
                # 1 chebi_info table
                # Many references to that chebi_id
                # Updated the empty strings to None 
                for i in range(0, len(row)):
                    if row[i] == "":
                        row[i] = None
                sql = "INSERT INTO chebi_info (chebi_id, name, definition, mass, charge, last_updated_date, formulae) VALUES(%s, %s, %s, %s, %s, %s, %s);"
                cursor.execute(sql,(chebi_id, row[chebi_info['name']], row[chebi_info['definition']], mass, row[chebi_info['charge']], row[chebi_info['last_modified']], row[chebi_info['formulae']],))
                # We also want to insert that chebi id into the other mapping to map
                # to itself. This saves a secondary call when we are looking  for the
                # chebi identifiers to assign to a unique id
                sql = "INSERT INTO chebi_mapping_to_other_id (chebi_id, other_id, other_id_type) VALUES (%s, %s, %s);"
                cursor.execute(sql,(chebi_id, chebi_id, 'chebi_id',))

        cursor.close()
        conn.commit()
        conn.close()

"""
If we want to add new external mappings call this script with the file name as 
input. The input file format for the external mapping is as follows:


    DATABASE_METABOLITE_ID_TYPES = ['metanetx_id', 'kegg_id', 'chebi_id', 'bigg_id', 'inchi_key', 'inchi_str', 'names', 'chebi_id']
    -> this is in the config file

"""
def add_other_id_type_to_db(file_path):
    # row count to tell us which is the header
    row_count = 0
    x_ref = None
    names = False
    conn = psycopg2.connect(psycopg2_conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)


    with open(file_path, 'r') as filein:
        filein = csv.reader(filein, delimiter='\t')
        for row in filein:
            if row_count == 0:
                # Store the name of the external reference type
                x_ref = row[1]
                row_count += 1
                if x_ref == "names":
                    names = True
            else:
                # If we have a name we need to clean the name so users can 
                # easily upload names in a consistant manner.
                other_id = row[1]
#                if names:
#                    other_id = re.sub(r'[\W_]+', '', other_id.lower())
                # Add to the DB
                sql = "INSERT INTO chebi_mapping_to_other_id (chebi_id, other_id, other_id_type) VALUES (%s, %s, %s);"
                cursor.execute(sql,(row[0], other_id.lower(), x_ref,))
            
    cursor.close()
    conn.commit()
    conn.close()


def clean_names(names):
    cleaned_names = []
    for name in names:
        # Remove all special characters and 
        name = re.sub(r'[\W_]+', '', name.lower())
        if name not in cleaned_names:
            cleaned_names.append(name)

    return cleaned_names

"""
Optional: Add a list of file names that we want to itterate through and add the external
references to the DB.
"""
def add_list_of_external_references(base_dir, external_refs):

    for x_ref in external_refs:
        add_other_id_type_to_db(base_dir + x_ref + ".tsv")

"""
    syno = "names"
    kegg_c = "kegg"
    secondary_id = "chebi_id"
    inchi_id = "inchi_key"
    inchi_str = "inchi_str"
    smiles = "smiles"
    hmdb = "hmdb"
    external_refs = [syno, secondary_id, inchi_id, inchi_str, smiles, hmdb, kegg_c]
"""
external_refs = ['hmdb_generic_name', 'hmdb_synonyms'] #['inchi_hmdb', 'metanetx_id', 'bigg_id', 'bigg_paths', 'bigg_name', 'inchi_str']
#external_refs = ['inchi_hmdb', 'metanetx_id', 'bigg_id']
#external_refs = ['bigg_paths', 'bigg_name']
#external_refs = ['names']
base_dir = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/"
#parse_chebi_to_db(base_dir + "outputs/chebi_info.tsv")
add_list_of_external_references(base_dir + "parsed_db_outputs/", external_refs)
