<%inherit file="base.mako"/>

        <%block name="omix_main_content">
                <div class="header-content" id="miniheader">
                    <div class="header-content-inner">
                        <h1 id="homeHeading">Register</h1>
                    </div>
                </div>

            % if error:
                <div class="errormsg">
                    Error: ${error}
                </div>
            % endif
              
            <div class='content_wrapper'>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-4 text-center">

                            <form action="" method="POST">
                                <div class="form-group">
                                  <input type="text" name="name" required class="form-control" placeholder="full name">
                                </div>

                                <div class="form-group">
                                  <input type="email" name="email" required class="form-control" id="exampleInputEmail1" placeholder="email">
                                </div>
                                <div class="form-group">
                                  <input type="email" name="email-conf" required class="form-control" id="exampleInputEmail1" placeholder="confirm email">
                                </div>
                            <button type="submit" class="btn btn-danger">Submit</button>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </%block>
