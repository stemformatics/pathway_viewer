<%inherit file="base.mako"/>
    <%block name="omix_main_content">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        <link href=${view.web_path + "/css/vis/styleEscher.css"} rel="stylesheet">
        <link href=${view.web_path + "/css/vis/action_panel.css"} rel="stylesheet">

        <link href=${view.web_path + "/css/vis/external.css"} rel="stylesheet">

        <link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href=${view.web_path + "/css/mapping/table.css"} rel="stylesheet">

            <div style="height:70px;">

                % if error:
                    <div class="errormsg">
                        Error: ${error}
                    </div>
                % endif
            </div>

            <!--

               <nav class="navbar navbar-default always-open" id="navbar-vis">
                    <div class="container-fluid white-bg">

                        <div class="navbar-header margin-top">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-vis-collapse">
                                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                            </button

                            <a class="navbar-brand">
                                <img alt="Escher" class="logo" src=${view.web_path + "/img/vis/escher-logo.png"}>
                            </a>
                            <a href="http://escher.github.io/">pathways by escher</a>
                            </a>
                        </div>

                    <div class="collapse navbar-collapse" id="navbar-vis-collapse">
                        <ul class="nav navbar-nav">

                            <li class="nav-item">
                                <input class="form-control" id="search-input" placeholder="search for a metabolite...">
                            </li>
                            <li class="nav-item">
                                <button class="btn-small" id="search-btn" onClick=search_for_metabolite()>search</button> 
                            </li>
                            <li class="nav-item">
                                <button class="btn-small" id="search-next-btn" onClick=search_next_metabolite()></button> 
                            </li>

                            <li class="nav-item">
                                <div class="dropdown">
                                    <button class="btn-action dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        pathways
                                        <span class="caret"></span>
                                    </button>

                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                                        % if pathways:
                                            % for path_id in pathways:
                                                <li><button class="btn-dropdown" onClick=get_pathway_data(${path_id}) value=${path_id}> (${path_id}) ${pathways[path_id]['name']} </a></li>
                                            % endfor
                                        % else:
                                             <li><a href="#">No Pathways</a></li>
                                        % endif
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <div class="dropdown">

                                    <button class="btn-action dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        datasets
                                        <span class="caret"></span>
                                    </button>

                                     <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    
                                        % if datasets:
                                            % for ds_id in datasets:
                                                <li><button class="btn-dropdown" onClick=get_datatset_data(${ds_id}) value=${ds_id}> (${ds_id}) ${datasets[ds_id]['name']}</button></li>
                                            % endfor
                                        % else:
                                            <li><a href="#">No Datasets</a></li>
                                        % endif
                                            
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <div class="dropdown">

                                    <button class="btn-action dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        metabolite lists
                                        <span class="caret"></span>
                                    </button>

                                     <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
     
                                        % if metabolite_lists:
                                            % for list_id in metabolite_lists:
                                                <li><button class="btn-dropdown" onClick=get_metabolite_list_data(${list_id}) value=${list_id} > (${list_id}) ${metabolite_lists[list_id]['name']} </button> </li>
                                            % endfor
                                        % else:
                                            <li><a href="#">No metabolite lists</a></li>
                                        % endif
     
                                    </ul>

                                </div>
                            </li>
                            <li class="nav-item">
                                <div class="dropdown">
                                    <button class="btn-action dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        actions
                                        <span class="caret"></span>
                                    </button>

                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                                        <li><button class="btn-dropdown" onclick=save_svg_to_png()> Save as PNG </button>
                                        </li>


                                        <li><button id="toggle-graph-vis-button" class="btn-dropdown" value="false" onclick="toggle_bar()">hide graphs</button>
                                        </li>

                                        <li><button class="btn-dropdown" onclick="remove_data()">Remove data</button>
                                        </li>
        
                                        <li><button class="btn-dropdown" value="false" onclick=toggle_modal_display("action-container")> Toggle action panel</button>
                                        </li>

                                        <li><button class="btn-dropdown" value="false" onclick=toggle_modal_display("stats-container")> Toggle statistics information</button>
                                        </li>


                                        <li><button id="highlight_reactions_from_metabolites_with_data" class="btn-dropdown" value="false" onclick="highlight_reactions_from_metabolites_with_data()">highlight reactions between metabolites with data</button>
                                        </li>

                                        <li><button class="btn-dropdown" value="false" onclick="dataset_action_toggle_graph_text()">Toggle graph text</button>
                                        </li>

                                        <li><button class="btn-dropdown" value="false" onclick="pathway_action_toggle_pathway_text()">Toggle pathway text </button>
                                        </li>

                    
                                        </ul>

                                </div>
                            </li>

                        </ul>

                        </div>
                    </div>
                </nav>

            -->
            <div id="search-container" class="action-container">
                <div class="row">
                    <div class="col-md-6">

                        <button class="modal-sub-header" onClick=toggle_modal_display("actions-search")>
                                <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                Search
                        </button>                   

                    </div>
                </div>
            </div>



            <div id="stats-container" class="action-container" style="width:800px;">
                <div class="row">
                    <div class="col-md-11">
                        <button class="close-btn" value="false" onclick=toggle_modal_display("stats-container")> 
                                x
                        </button>
                    </div>
                </div                
                
                <div class="row">
                    <div class="col-md-11">>
                
                        <button class="modal-sub-header" onClick=toggle_modal_display("actions-stats")>
                                Stats
                        </button>

                        <div id="stats-table">
            
                        </div>        
                    </div>
                </div>
            </div>


            <div id="action-container" class="action-container">

                <div class="row">
                    <div class="col-md-10">
                        <button class="close-btn" value="false" onclick=toggle_modal_display("action-container")> 
                                x
                        </button>
                    </div>
                </div>
                <div>
                    <div class="col-md-6">
                    
                        <button class="modal-sub-header" onClick=toggle_modal_display("actions-datasets")>
                                <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                Dataset actions
                        </button>
                        
                        <div class="sub-actions" id="actions-datasets">
                                <div class="row action-row">
                                    <label class="radio-inline">
                                        <input type="radio" name="position-option" value="diff" checked> difference between samples
                                    </label>
                                </div>

                            <div class="row action-row">
                                <label> value </label>
                                <input type="range" name="diff-val" min="0" max="1000000" onchange="update_diff_val(this.value);" oninput="update_diff_val(this.value)">                         
                                <p id="diff-val"></p>    
                                               
                            </div>
                       </div>

                    </div>
    
                    <div class="col-md-6">

                        <button class="modal-sub-header" onClick=toggle_modal_display("actions-metabolite-lists")>
                                <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                List actions
                        </button>

                        <div class="sub-actions" id="actions-metabolite-lists">

                            % for header in metabolite_list_header:
                                % if header[0] != 'r' and header[0] != 'c' and header[0] != 'n':         
                                    <div class="row">             
                                        <label class="radio-inline">
                                            <input type="radio" name="position-option" value=${header}> ${header.replace("_", " ")}
                                        </label>
                                    </div>
                                % endif
                            % endfor

                            <div class="row action-row">

                                 <label> probability value </label>
                                    <input type="range" name="prob-diff-val" min="-100" max="100" onchange="update_prob_diff_val(this.value);" oninput="update_prob_diff_val(this.value)">
                                <p id="prob-diff-val"></p>

                            </div>
                        </div>
                    </div>
                
                </div>
                        
                   <div class="row action-row sub-actions" id="actions-options">

                        <label> filter option </label>

                        <label class="radio-inline">
                            <input type="radio" name="action-option" value="less" checked> <
                        </label>

                        <label class="radio-inline">
                            <input type="radio" name="action-option" value="equal"> =
                        </label>
                        
                        <label class="radio-inline">
                            <input type="radio" name="action-option" value="greater"> >
                        </label>

                    </div>

                <div class="row action-row">

                        <div class="colour-container" id="picker"></div>

                        <div id="picker-indicator"></div>

                    <div class="slider-wrapper">

                        <div class="colour-container" id="slider"></div>

                        <div id="slider-indicator"></div>

                    </div>

                    <div class="slider-wrapper">
                        <div class="colour-container" id="colour-example"></div>
                    </div>

                </div>

                <button class="modal-sub-header" onClick=toggle_modal_display("more-actions")>
                        <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                        More vis options
                </button>

                <div class="sub-actions" id="more-actions">

                    <div class="row action-row">

                        <label> circle size </label>

                        <input type="range" name="node-size" min="0" max="200" onchange="update_node_val(this.value);" oninput="update_node_val(this.value)">                                 
                        <p id="node-val"></p>

                    </div>

                    <div class="row action-row">

                        <label> opacity </label>

                        <input type="range" name="node-opacity" min="0" max="100" onchange="update_opacity_val(this.value);" oninput="update_opacity_val(this.value)">                          

                        <p id="opacity-val"></p>

                    </div>

                </div>

            </div>

        <div id="sidebar-top">
           <div id="sidebar">
                
                <button id="minimise-btn" onClick="toggle_minimise_sidebar()"> << </button>    
           
                <div id="minimise">

                    <div class="row">
                        <button id="metabolite-info-btn" class="sidebar-header" onClick="make_sidebar_section_visible('metabolite-info')"> See Metabolite Info </button>
                        <button id="datasets-pathways-btn" class="sidebar-header" onClick="make_sidebar_section_visible('datasets-pathways')"> Choose Pathway and Datasets </button>
                    </div>
                    <div class="row">
                            <div id="metabolite-info" >
                                <div >
                                    <div >
                                        <h4 class="sidebar-title" id='molecule-modal-title'>Molecule Information</h4>
                                    </div>

                                    <div id="modal-graph" class="row svg-graph">

                                    </div>

                                    <div class="row">
                                        <button class="sidebar-sub-header" onClick=toggle_modal_display("molecule-display-info")>
                                            <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                            Information 
                                        </button>
                                    </div>

                                    <div id="molecule-display-info">
                                        % for info in molecule_display_info:
                                            <div class="row">
                                                <div class="col-md-3 label-col">
                                                    ${info}
                                                </div>
                                                <div id=${info.lower().replace(" ", "")} class="col-md-5 info-col">

                                                </div>
                                            </div>
                                        % endfor
                                    </div>

				                    <div class="row">
                                        <button class="sidebar-sub-header" onClick=toggle_modal_display("metabolite-list-header")> 
                                                <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                                Metabolite list info 
                                        </button>
                                    </div>

                                    <div id="metabolite-list-header">

                                        % for header in metabolite_list_header:
                                            <div class="row">
                                                <div class="col-md-4 label-col">
                                                    ${header.replace("_", " ")}
                                                </div>
                                                <div id=${header.replace("_", "")} class="col-md-8 info-col">

                                                </div>
                                            </div>
                                        % endfor
                                    </div>
                                </div>
                            </div>

                            <div id="datasets-pathways">

                                <label class="sidebar-title"> Datasets </label>

                                <div id="datasets-sidebar">


                                </div>

                                <label class="sidebar-title"> Pathways </label>

                                <div id="pathways-sidebar">
                                

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
            <div id="visdiv" style="display: None;">
                ${json_str}
            </div>

            <div id="dictdiv" style="display: None;">
                ${mappings}
            </div>

            <script src=${view.web_path + "/js/vis/external/colorpicker.min.js"}></script>

            <script src="//d3js.org/d3.v3.min.js"></script>

            <script src="https://cdn.rawgit.com/eligrey/canvas-toBlob.js/f1a01896135ab378aa5c0118eadd81da55e698d8/canvas-toBlob.js"></script>

            <script src="https://cdn.rawgit.com/eligrey/FileSaver.js/e9d941381475b5df8b7d7691013401e171014e89/FileSaver.min.js"></script>

            <script src= ${var_url}></script>

            <script src= ${parse_url}></script>

            <script src=${graph_url}></script>

            <script src=${view.web_path +"/js/d3-context-menu.js"}></script>

            <script src=${view.web_path + "/js/vis/dataset_actions.js"}></script>
            
            <script src=${view.web_path + "/js/vis/graph_renderer.js"}></script>

            <script src=${view.web_path + "/js/vis/set_metabolite_list.js"}></script>

            <script src=${view.web_path + "/js/vis/actions.js"}></script>      

            <script src=${view.web_path + "/js/vis/metabolite_list_actions.js"}></script>            
           
            <script src=${view.web_path + "/js/vis/actions_ajax.js"}></script>

            <script src=${view.web_path + "/js/vis/add_chebi_info.js"}></script>


            <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
            <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
    </%block>
