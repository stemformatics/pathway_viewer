<%inherit file="base.mako"/>

        <%block name="omix_main_content">
                <div class="header-content" id="miniheader">
                    <div class="header-content-inner">
                        <h2 id="homeHeading">My pathways </h1>
                    </div>
                </div>     

            % if error:
                <div class="errormsg">
                    Error: ${error}
                </div>
            % endif

            <div class='content_wrapper'>

                <p class="help"> To get started with creating a visualisation, you'll need to choose a dataset and a pathway.<br>
                  Either upload new data or choose from your existing pathways and datsets below.</p>

                <div class="container">
                    <div class="row">

                    <div class="col-lg-8 col-lg-offset-2">

                        <h3 class="mypage leftpadd"> Add pathway or <a href="http://escher.github.io/">Make a pathway with Escher </a></h3> 
                            <form action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data" name="pathway_post">

                                <input type="text" class="form-control" name="pathway_filename" required placeholder="Pathway name">

                                <div class="form-group">
                                    <label class="radio-inline leftpadd">
                                        <input type="radio" name="pathway_options" id="user_select" value="user" checked> User access
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="pathway_options" id="group_select" value="group"> Group access
                                    </label>
                                </div>

                                <input type="text" class="form-control" name="pathway_group_name" placeholder="Group name (leave blank if you selected 'Just me')">

                                <textarea class="form-control" rows="5" name="pathway_notes" placeholder="notes"></textarea>

                              <div class="form-group">
                                    <label class="radio-inline leftpadd">
                                        <input type="radio" name="pathway_access_options" value="t" checked> private
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="pathway_access_options" value="f"> public
                                    </label>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="validated"> validate this pathway 
                                    </label>
                                </div>

                              <div class="form-group">
                                <input type="file" name="pathway_upload" id="pathway_upload" value=''>
                              </div>

                            <button type="submit" class="btn btn-danger leftpadd">Submit</button>
                          </form>

                        </div>
                    </div>
                </div>
            </div>
    
    
    <hr class="primarylong">
 
    <div id="datadiv" style="display: none;">
        ${json_pathways}
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Pathway Information</h4>
                </div>
                <div class="modal-body">

                    <div class="panel panel-info">
                        <div class="panel-heading"> Pathway ID </div>
                        <div class="panel-body" id="view_path_id">

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> Pathway Name </div>
                        <div id="view_path_title" class="panel-body">

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> Pathway Privacy (public or private) </div>
                        <div id="view_path_private" class="panel-body">

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> Your access levels (user or group) </div>
                        <div id="view_path_access" class="panel-body">

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> Validiation state </div>
                        <div id="view_path_validation" class="panel-body">

                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading"> Further Notes </div>
                        <div id="view_path_notes"class="panel-body">

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

 	<div class='content_wrapper'>
        <div class="row">

            <div class="col-lg-8 col-lg-offset-2">

                <h3 class="mypage leftpadd"> My Pathways </h3> <br>
                <button type="button" id="hidemypathways" class="btn btn-info leftpadd">Hide my pathways </button>

                <table class="table table-striped" id="mypathways-table">
                    <thead>
                        <tr>
                            <th > Pathway ID </th>
                            <th > Name </th>
                            <th > Access</th>
                            <th > Availability </th>
                            <th > Validated </th>
                            <th > View </th>
                      </tr>
                    </thead>

                      <tbody>
                          % if pathways == None:
                              <tr>
                                  <td> No pathways. </td><td></td><td></td>
                              </tr>
                          % else:
                              % for p in pathways:
                                  <tr>
                                        <td> ${p} </td>
                                        <td> ${pathways[p]['name']} </td>
                                        <td> ${pathways[p]['private']} </td>
                                        <td> ${pathways[p]['owner_type']} </td>
                                        <td> ${pathways[p]['validated']} </td>
                                        <td>  
                                            <button type="button" id=${p} onClick="view_pathway(this.id)"  class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
                                                View pathway
                                            </button>
                                        </td>
                                  </tr>
                              % endfor 
                          % endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src=${view.web_path + "/public/js/pathway/pathway_modal_view.js"}></script>

</%block>
