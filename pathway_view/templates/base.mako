<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Omicxview</title>

    <!-- Bootstrap Core CSS -->
    <link href=${view.web_path + "/css/bootstrap/vendor/bootstrap/css/bootstrap.min.css"} rel="stylesheet">

    <!-- Custom Fonts -->
    <link href=${view.web_path + "/css/bootstrap/vendor/font-awesome/css/font-awesome.min.css"} rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href=${view.web_path + "/css/bootstrap/vendor/magnific-popup/magnific-popup.css"} rel="stylesheet">

    <!-- Theme CSS -->
    <link href=${view.web_path + "/css/bootstrap/css/creative.min.css"} rel="stylesheet">

    <link href=${view.web_path + "/css/bootstrap_override/bootstrap_override.css"} rel="stylesheet">

   <link href=${view.web_path + "/css/base.css"} rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

   <!-- jQuery -->
    <script src=${view.web_path + "/css/bootstrap/vendor/jquery/jquery.min.js"}></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=${view.web_path + "/css/bootstrap/vendor/bootstrap/js/bootstrap.min.js"}></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src=${view.web_path + "/css/bootstrap/vendor/scrollreveal/scrollreveal.min.js"}></script>
    <script src=${view.web_path + "/css/bootstrap/vendor/magnific-popup/jquery.magnific-popup.min.js"}></script>

    <!-- Theme JavaScript -->
    <script src=${view.web_path + "/css/bootstrap/js/creative.min.js"}></script>
    <script src=${view.web_path + "/js/invert.js"}></script>

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="http://www.stemformatics.org/">
                                <img alt="" class="logo" src=${view.web_path + "/img/stemformatics-logo.png"}>
                            </a>
                <a class="navbar-brand page-scroll" href="/">omicxview</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right nav-bar-text-color">
                    <li>
                        <a href="/auth/help">help</a>
                    
                    </li>
                    <li>
                        % if view.logged_in_user_id:
                            <a href="/mapping/index">id mapping</a>
                        % endif
                    </li>
 
                    <li>
                        % if view.logged_in_user_id:
                            <a href="/datasets/index">datasets</a>
                        % endif
                    </li>
                    <li>
                        % if view.logged_in_user_id:
                            <a href="/pathways/index">pathways</a>
                        % endif
                    </li>
                    <li>
                        % if view.logged_in_user_id:
                            <a href="/metabolite_lists/index">metabolite lists</a>
                        % endif
                    </li>

                    <li>
                        <a href="/vis/pathway_viewer">start visualising</a>
                    </li>
                    <li>
                        % if not view.logged_in_user_id:
                            <a href="/auth/login">login</a>
                        % else:
                            <a href="/mypage">${view.logged_in_user_name['name']}'s page</a>
                        % endif 
                    </li>
                    <li>
                        % if not view.logged_in_user_id:
                            <a href="/auth/register">register</a>
                        % else:
                            <a href="/auth/logout" style="color:black;">logout: ${view.logged_in_user_name['name']}</a>
                        % endif
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <div class="beta">
    
    </div>
    <!-- The general content goes in here for each page -->
    <%block name="omix_main_content">
    
    </%block> 


    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                        <a class="link" href="http://www.stemformatics.org/">A Stemformatics project
                </div>
                <div class="col-lg-8 col-lg-offset-2 text-center" style="padding:20px;">
                    <a class="link" href="http://escher.github.io/">Pathways by Escher</a>
                </div>
                <div class="col-lg-8 col-lg-offset-2 text-center" style="padding:20px;">
                    <a class="link" href="https://github.com/ArianeMora">If there's any issues let me know</a>
                </div>
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    Invert the colour scheme
                    <button class="btn" onclick=invert() id="invert" value="light">light</button>
                </div>
            </div>

        </div>
    </section>

    <script src=${view.web_path + "/js/general/general.js"}></script>

</body>

</html>
