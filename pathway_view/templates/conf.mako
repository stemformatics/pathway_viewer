<%inherit file="base.mako"/>

        <%block name="omix_main_content">
                <div class="header-content" id="miniheader">
                    <div class="header-content-inner">
                        <h1 id="homeHeading">Confirm Registration</h1>
                    </div>
                </div>

            % if error:
                <div class="errormsg">
                    Error: ${error}
                </div>
            % endif
              
            <div class='content_wrapper'>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-4 text-center">

                            <form action="" method="POST">
                              <div class="form-group">
                                <input type="email" name="email" required class="form-control" id="exampleInputEmail1" placeholder="email">
                              </div>
                              <div class="form-group">
                                <input type="password" name="code" required class="form-control" placeholder="enter registration code">
                              </div>

                              <div class="form-group">
                                <input type="password" name="password" required class="form-control"  placeholder="password">
                              </div>
                              <div class="form-group">
                                <input type="password" name="password-conf" required class="form-control" placeholder="confirm password">
                              </div>


                           <button type="submit" class="btn btn-danger">Submit</button>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </%block>
