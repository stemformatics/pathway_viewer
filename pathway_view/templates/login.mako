<%inherit file="base.mako"/>

        <%block name="omix_main_content">
                <div class="header-content" id="miniheader">
                    <div class="header-content-inner">
                        <h1 id="homeHeading">Login</h1>
                    </div>
                </div>

            % if error:
                <div class="errormsg">
                    Error: ${error}
                </div>
            % endif
              
            <div class='content_wrapper'>
                <div class="container formclass">
                    <div class="row">
                        <div class="col-lg text-center">
                            <form action="" method="POST">

                                <div class="form-group">
                                    <input type="email" name="email" required class="form-control" placeholder="email">
                                </div>

                                <div class="form-group">
                                    <input type="password" name="password" required class="form-control" placeholder="password">
                                </div>

                                <div class="checkbox left-float">
                                    <label>
                                        <input type="checkbox"> Stay signed in
                                    </label>
                                </div>
    
                                <button type="submit" class="btn btn-danger left-float">Submit</button>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </%block>
