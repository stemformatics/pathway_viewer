<%inherit file="base.mako"/>

        <%block name="omix_main_content"> 

                <div class="header-content" id="miniheader">
                    <div class="header-content-inner">
                        <h2 id="homeHeading">Help </h2>
                    </div>
                </div>

            <div class='content_wrapper' >

                <div class="container">

                    <div class="row">


<h2 dir="ltr"><span>How to upload a dataset</span></h2>
<ol>
    <li dir="ltr">
        <p dir="ltr"><span>Select datasets page (https://omicxview.stemformatics.org/datasets/index)</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Add an informative name</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Ignore the id types (these are just there to let you know which are allowed)</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Choose who has access to the dataset</span></p>
    </li>
    <ol>
        <li dir="ltr">
            <p dir="ltr"><span>Group access:</span></p>
        </li>
        <ol>
            <li dir="ltr">
                <p dir="ltr"><span>Click the </span><span>group access </span><span>radio button (as shown below)</span></p>
            </li>
            <li dir="ltr">
                <p dir="ltr"><span>Type the name of the group (the group must already exist)</span></p>
            </li>
        </ol>
        <li dir="ltr">
            <p dir="ltr"><span>Just me:</span></p>
        </li>
        <ol>
            <li dir="ltr">
                <p dir="ltr"><span>For individual access, leave the group name blank and select </span><span>just me</span></p>
            </li>
        </ol></ol>
    <li dir="ltr">
        <p dir="ltr"><span>Add in notes, these can be metadata, or the date, users involved or what you had to do to get the data formatted correctly.</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Select either public or private (private also covers groups that are private if public is selected </span><span>everyone </span><span>has access to this dataset).</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Select a dataset to upload, the format must be as below:</span></p>
    </li>
    <ol>
        <li dir="ltr">
            <p dir="ltr"><span>Sample types are separated with a pipe character</span></p>
        </li>
        <li dir="ltr">
            <p dir="ltr"><span>Must be a TSV file</span></p>
        </li>
    </ol></ol>
<p><span><span><br /><br /></span></span></p>
<p dir="ltr">&nbsp;</p>
<div dir="ltr">
    <table><colgroup><col width="87" /><col width="73" /><col width="76" /><col width="89" /><col width="75" /><col width="71" /><col width="74" /><col width="73" /><col width="94" /></colgroup>
        <tbody>
            <tr>
                <td>
                    <p dir="ltr"><span>ID</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>Neg|1</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>Neg|2</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>Neg|3</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>Neg|4</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>Pos|1</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>Pos|2</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>Pos|3</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>Pos|4</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p dir="ltr"><span>2hyoxplac</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1707309</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>450233</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>826286</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1813375</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1944799</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>773873</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>950009</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>464913</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p dir="ltr"><span>2obut</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1195565</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1239463</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1587679</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1646232</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1171334</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1918293</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1329458</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>173920</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p dir="ltr"><span>3hanthrn</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1921280</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>866053</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1873130</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>845564</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>112003</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1086704</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>476947</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1128848</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p dir="ltr"><span>3hpp</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>994483</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1656631</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1726712</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1611816</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>563114</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1916356</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1037301</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>1586119</span></p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<p><span><span>&nbsp;</span></span></p>
<ol start="8">
    <li dir="ltr">
        <p dir="ltr"><span>Unmapped identifiers and any numerical errors are returned. These aren&rsquo;t stored so if you want to store these that may be a good idea.</span></p>
    </li>
</ol>
<p dir="ltr"><span><img src=${view.web_path + "/img/help/datasets_page.png"} alt="" width="667" height="343" /></span></p>
<p><span><span>&nbsp;</span></span></p>
<p dir="ltr"><span><img src=${view.web_path + "/img/help/dataset_errors.png"} alt="" width="671" height="243" /></span><span></span></p>
<h2 dir="ltr"><span>How to upload a metabolite list</span></h2>
<ol>
    <li dir="ltr">
        <p dir="ltr"><span>Select metabolite lists page (https://omicxview.stemformatics.org/metabolite_lists/index)</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Add an informative name</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Choose who has access to the metabolite_lists</span></p>
    </li>
    <ol>
        <li dir="ltr">
            <p dir="ltr"><span>Group access:</span></p>
        </li>
        <ol>
            <li dir="ltr">
                <p dir="ltr"><span>Click the group access radio button (as shown below)</span></p>
            </li>
            <li dir="ltr">
                <p dir="ltr"><span>Type the name of the group (the group must already exist)</span></p>
            </li>
        </ol>
        <li dir="ltr">
            <p dir="ltr"><span>Just me:</span></p>
        </li>
        <ol>
            <li dir="ltr">
                <p dir="ltr"><span>For individual access, leave the group name blank and select just me</span></p>
            </li>
        </ol></ol>
    <li dir="ltr">
        <p dir="ltr"><span>Add in notes, these can be metadata, or the date, users involved or what you had to do to get the metabolite list formatted correctly.</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Select either public or private (private also covers groups that are private if public is selected </span><span>everyone </span><span>has access to this metabolite list).</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Select a metabolits list from the computer, headers can be anything as these are mapped in the next step, an example is as follows:</span></p>
    </li>
</ol>
<p dir="ltr">&nbsp;</p>
<div dir="ltr">
    <table><colgroup><col width="85" /><col width="58" /><col width="70" /><col width="94" /></colgroup>
        <tbody>
            <tr>
                <td>
                    <p dir="ltr"><span>ID</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>pval</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>Bh-pval</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>fold change</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p dir="ltr"><span>2hyoxplac</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>0.028</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>0.082</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>0.2</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p dir="ltr"><span>2obut</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>0.032</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>0.08</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>0.62</span></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p dir="ltr"><span>dad_2</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>0.025</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>0.057</span></p>
                </td>
                <td>
                    <p dir="ltr"><span>-0.91</span></p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<p><span><span>&nbsp;</span></span></p>
<ol start="7">
    <li dir="ltr">
        <p dir="ltr"><span>Select the correct headers, at </span><span>minimum</span><span> the ID and one column must have a value, the rest can be left empty.</span><span><img src=${view.web_path + "/img/help/m_lists.png"} alt="" width="624" height="325" /></span></p>
    </li>
</ol>
<p dir="ltr"><span>*** ID&rsquo;s not mapped aren&rsquo;t returned to the front end &rarr; it is assumed that the ID&rsquo;s for the metabolite list are the same as those used in the dataset (perhaps this will need to change in the future)</span></p>
<p><span><span>&nbsp;</span></span></p>
<h3 dir="ltr"><span></span></h3>
<h2 dir="ltr"><span>How to perform an ID mapping</span></h2>
<p dir="ltr"><span>ID mappings are helpful for finding which identifiers are able to be used on the pathways. It gives information about which identifiers are able to be mapped to Bigg_IDs and which pathways they are available on.</span></p>
<p><span><span>&nbsp;</span></span></p>
<ol>
    <li dir="ltr">
        <p dir="ltr"><span>Navigate to the </span><span>id mapping </span><span>tab (</span><a href="https://omicxview.stemformatics.org/mapping/index"><span>https://omicxview.stemformatics.org/mapping/index</span></a><span>)</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Choose the identifiers you wish to map to (i.e. bigg, chebi, inchi)</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Choose your file, the file can have any number of columns, but only the </span><span>first</span><span> column will have the identifiers mapped (i.e. it is handy to use for determining the mappings on a dataset or metabolite list, you just need to ensure the </span><span>first column </span><span>has the metabolite identifiers.)</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Download or sort.</span></p>
    </li>
</ol>
<p><span><span>&nbsp;</span></span></p>
<div dir="ltr">
    <table><colgroup><col width="594" /><col width="30" /></colgroup>
        <tbody>
            <tr>
                <td>
                    <p dir="ltr"><span>hcarbohydrate metabolism, hamino acid metabolism (partial)</span></p>
                </td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>
<p dir="ltr"><span>An example of the pathways, note the first letter indicates which species: </span></p>
<ul>
    <li dir="ltr">
        <p dir="ltr"><span>H = human</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>E = Escherichia coli</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>S = Saccharomyces Cerevisiae</span></p>
    </li>
</ul>
<p><span><span>&nbsp;</span></span></p>
<p dir="ltr"><span><img src=${view.web_path + "/img/help/id_mapping.png"} alt="" width="624" height="325" /></span></p>
<p><span><span><br /><br /></span></span></p>
<h2 dir="ltr"><span>An example with outputs</span></h2>
<p dir="ltr"><span>The following is the output of the example data. </span></p>
<p><span> An example dataset can be downloaded from: 
<a href="https://dev-ariane-s4m.stemformatics.org/pathway_view_public/data/help_examples/random_public_dataset.csv"> example dataset download </a>
The metabolite list can be downloaded from: 
<a href="https://dev-ariane-s4m.stemformatics.org/pathway_view_public/data/help_examples/random_public_metabolite_list.csv"> example metabolite list download </a>. <span>&nbsp;</span></span></p>

<p dir="ltr"><span>Steps:</span></p>
<ol>
    <li dir="ltr">
        <p dir="ltr"><span>Upload dataset (see </span><span>how to upload a dataset</span><span>)</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Upload list (see </span><span>how to upload a metabolite list</span><span>)</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Test on the visualisation tab (</span><a href="https://omicxview.stemformatics.org/vis/pathway_viewer"><span>https://omicxview.stemformatics.org/vis/pathway_viewer</span></a><span>)</span></p>
    </li>
    <li dir="ltr">
        <p dir="ltr"><span>Click through to other pathways and check that:</span></p>
    </li>
    <ol>
        <li dir="ltr">
            <p dir="ltr"><span>Dataset remains (are there still graphs?)</span></p>
        </li>
        <li dir="ltr">
            <p dir="ltr"><span>Colours of list remain the same (i.e. is the list still there)</span></p>
        </li>
        <li dir="ltr">
            <p dir="ltr"><span>Click on a graph and check it still can be viewed in the side tab.</span></p>
        </li>
    </ol></ol>
<h3 dir="ltr"><span>Output explanation</span></h3>
<p dir="ltr"><span><img src=${view.web_path + "/img/help/output_explained.png"} alt="" width="624" height="321" /></span></p>
<h3 dir="ltr"><span><img src=${view.web_path + "/img/help/info_explained.png"} alt="" width="624" height="432" /></span></h3>
<h3 dir="ltr"><span>Output</span></h3>
<p dir="ltr"><span><img src=${view.web_path + "/img/help/change_path.png"} alt="" width="624" height="324" /></span></p>
<p dir="ltr"><span><img src=${view.web_path + "/img/help/tca_cycle.png"} alt="" width="624" height="288" /></span></p>
<p><span>&nbsp;</span></p>

        



                    </div>

                </div>

            </div>

</%block>
