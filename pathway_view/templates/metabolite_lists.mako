<%inherit file="base.mako"/>

        <%block name="omix_main_content">
                <div class="header-content" id="miniheader">
                    <div class="header-content-inner">
                        <h2 id="homeHeading">My Metabolite lists </h1>
                    </div>
                </div>     

            % if error:
                <div class="errormsg">
                    Error: ${error}
                </div>
            % endif

            <div class='content_wrapper'>

                <p class="help"> To get started with creating a visualisation, you'll need to choose a  dataset  and a pathway (and optionally a metabolite and or pathway list) .<br>
                  Either upload new metabolite list data or choose from your existing pathways and datsets below.</p>

                <div class="container">
                    <div class="row">

                        <div class="col-lg-8 col-lg-offset-2">

                        <h3 class="mypage leftpadd"> Add Metabolite List </h3>
                            <form action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data" name="upload_list">

                                <input type="text" class="form-control" name="filename" required placeholder="Metabolite list name">

                                <div class="form-group">
                                    <label class="radio-inline leftpadd">
                                        <input type="radio" name="owner_options" id="user_select" value="user" checked> just me
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="owner_options" id="group_select" value="group"> group access
                                    </label>
                                </div>
                              
                                <input type="text" class="form-control" name="group_name" placeholder="Group name (leave blank if you selected 'Just me')">
                    
                                <textarea class="form-control" rows="5" name="notes" placeholder="notes"></textarea>

                                <div class="form-group">
                                    <label class="radio-inline leftpadd">
                                        <input type="radio" name="access_options" id="user_select" value="t" checked> private
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="access_options" id="group_select" value="f"> public
                                    </label>
                                </div>


                              <div class="form-group">
                                <input type="file" name="upload" id="upload" value=''>
                              </div>

                            <!-- Make select options for the user -->
                            
                            % for header in range(0, len(metabolite_header_list)):
                                % if header == 0:
                                    <div class="form-inline">

                                % elif header != len(metabolite_header_list) and header % 3 == 0:
                                    </div>
                                    <div class="form-inline">
                                % endif
                            
                                <div class="form-group mid-size">                                
                                    <label class='header-select-label'> ${metabolite_header_list[header].replace("_", " ")} </label>
                                            <select class='header_selection' name=${metabolite_header_list[header]}>

                                                % if metabolite_header_list[header] == 'raw_metabolite_id_type':
                                                    % for id_type in range(0, len(supported_metabolite_id_types)):
                                                        <option>${supported_metabolite_id_types[id_type]}</option>
                                                    % endfor
                                                % else:
                                                    <option>None</option>
                                                % endif
                                            </select>
                                        </div>
                                    
                            % endfor
                            
                            </div>                       

                            <button type="submit" class="btn btn-danger leftpadd">Submit</button>
                        </form>


                        </div>



                    </div>
                </div>
            </div>
    
    
    <hr class="primarylong">


    <div id="datadiv" style="display: none;">
        ${json_lists}
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="info_modal">Metabolite List Information</h4>
                </div>
                <div class="modal-body">

                    <div class="panel panel-info">
                        <div class="panel-heading"> List ID </div>
                        <div class="panel-body" id="view_id">

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> List Name </div>
                        <div id="view_title" class="panel-body">

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> List Privacy (public or private) </div>
                        <div id="view_private" class="panel-body">
    
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> Your access levels (user or group) </div>
                        <div id="view_access" class="panel-body">

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> Further Notes </div>
                        <div id="view_notes"class="panel-body">

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

 	<div class='content_wrapper'>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">

                <h3 class="mypage leftpadd"> My Metabolite Lists </h3> <br>
                <button type="button" id="hidemydatsets" class="btn btn-info leftpadd">Hide my metabolite lists </button>

                <table class="table table-striped" id="mygroups-table">
                    <thead>
                        <tr>
                            <th > Metabolite List ID </th>
                            <th > Name </th>
                            <th > Availability </th>
                            <th > Access Type </th>
                            <th > View </th>  
                      </tr>
                    </thead>
		            <tbody>
                        % if lists == None:
                            <tr>
                                <td> No metabolite lists. </td><td></td><td></td>
                            </tr>
                        % else:
                            % for s in lists:
                                <tr>
                                    <td> ${s} </td>
                                    <td> ${lists[s]['name']} </td>
                                    <td> ${lists[s]['private']} </td>
                                    <td> ${lists[s]['owner_type']} </td>
                                    <td>
                                        <button type="button" id=${s} onClick="view_element_info(this.id)"  class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
                                            View List Info
                                        </button>
                                    </td>
                                </tr>
                            % endfor 
                        % endif
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="//d3js.org/d3.v3.min.js"></script>
    <script src=${view.web_path + "/js/lists/fill_modal_list_data.js"}></script>
    <script src=${view.web_path + "/js/lists/read_list_to_help_col_select.js"}></script> 
</%block>
