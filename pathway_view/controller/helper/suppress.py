import pyramid_handlers


# noinspection PyPep8Naming from 
# https://github.com/mikeckennedy/python-for-entrepreneurs-course-demos/blob/master/12_user_accounts/blue_yellow_app_12/blue_yellow_app/infrastructure/supressor.py
class suppress(pyramid_handlers.action):
    def __init__(self, _=None, **kw):
        kw['request_method'] = 'NOT_A_HTTP_VERB'
        super().__init__(**kw)
