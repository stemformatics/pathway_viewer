import pyramid_handlers
# This allows the modules from this to be called in the mako templates
from pyramid.renderers import render_to_response

# Controller and helpers
import pathway_view.controller.helper.cookie_auth as cookie_auth
from pathway_view.controller.base import BaseController

# Model
from pathway_view.model.auth import AuthModel
from pathway_view.config import *


class GroupController(BaseController):
   
    # -------------------- Adding and removing users from groups ------------------- #

    @pyramid_handlers.action(request_method='POST')#, name='/add/group')
    def add_group(self):
        uid = int(self.logged_in_user_id)

        if not uid:
            self.redirect('/login')

        # Get the name of the group they want to add
        name = str(self.request.POST.get('group-add-name'))
        name_conf = str(self.request.POST.get('group-add-name-conf'))

        # first check that the names match
        if name != name_conf:
            return {'error': 'The names you entered do not match.'}

        uid = int(self.logged_in_user_id)

        # If somehow someone has called this when they shoudn't be able to
        if not uid:
            print(uid)
            self.redirect('/login')

        # Also check they are admin
        auth = AuthModel()
        role = auth.get_user_role_by_uid(uid)

        if auth.error:
            print({'error': auth.error})
            self.redirect('/')

        # get the role of the user
       
        if role != 'admin':
            print({'error': "Only an admin can alter groups"})
            self.redirect('/')

        # Try adding the group and add the user to the auth model
        auth.uid = uid
        auth.role = role

        group = auth.create_group(name)
        
        return self.redirect('/mypage')
        
        
    @pyramid_handlers.action(request_method='POST')
    def add_user_to_group(self):
        uid = int(self.logged_in_user_id)

        if not uid:
            self.redirect('/login')

        name = str(self.request.POST.get('add-user-to-group-name'))
        email = str(self.request.POST.get('add-user-to-group-email'))
        email_conf = str(self.request.POST.get('add-user-to-group-email-conf'))

        # first check that the names match
        if email != email_conf:
            return {'error': 'The emails you entered do not match.'}

        uid = int(self.logged_in_user_id)

        # If somehow someone has called this when they shoudn't be able to
        if not uid:
            print(uid)
            self.redirect('/login')

        # Also check they are admin
        auth = AuthModel()
        role = auth.get_user_role_by_uid(uid)

        if auth.error:
            print({'error': auth.error})
            self.redirect('/')

        if role != 'admin':
            print({'error': "Only an admin can alter groups"})
            self.redirect('/')

        # Try adding the group and add the user to the auth model
        auth.uid = uid
        auth.role = role

        group = auth.add_user_to_group(email, name)

        print(group)
        print(auth.to_dict())
        return self.redirect('/mypage')


    @pyramid_handlers.action(request_method='POST')#, name='mypage/remove/user/group')
    def remove_user_from_group(self):
        uid = int(self.logged_in_user_id)

        if not uid:
            self.redirect('/login')

        name = str(self.request.POST.get('remove-user-from-group-name'))
        email = str(self.request.POST.get('remove-user-from-group-email'))
        email_conf = str(self.request.POST.get('remove-user-from-group-email-conf'))

        # first check that the names match
        if email != email_conf:
            return {'error': 'The emails you entered do not match.'}

        uid = int(self.logged_in_user_id)

        # If somehow someone has called this when they shoudn't be able to
        if not uid:
            print(uid)
            self.redirect('/login')

        # Also check they are admin
        auth = AuthModel()
        role = auth.get_user_role_by_uid(uid)

        if auth.error:
            print({'error': auth.error})
            self.redirect('/')

        # get the role of the user
        role = user[ROLE]

        if role != 'admin':
            print({'error': "Only an admin can alter groups"})
            self.redirect('/')

        # Try adding the group and add the user to the auth model
        auth.uid = uid
        auth.role = role

        group = auth.remove_user_from_group(email, name)

        print(group)
        print(auth.to_dict())
        return self.redirect('/mypage')




    # ----------------- Get actions which render pages -------------------------------#

    # Gets the users groups and dictionaries and displays them on the page
    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/mypage.mako',
                             request_method='GET',
                             name='mypage')
    def mypage_index(self):
        # first see if there is a user logged in otherwise redirect to the login page
        uid = int(self.logged_in_user_id)
        groups = None
        dictionaries = None

        if not uid:
            self.redirect('/login')

        # otherwise we want to get the groups and dictionaries associated with the user
        # first need to set the user in the auth model
        auth = AuthModel()

        user = auth.get_user_role_by_uid(uid)
        
        # Check if there is an error
        if auth.error:
            return {'error': auth.error, 'groups': groups, 'dictionaries': dictionaries}
        
        # otherwise we want to get the groups for the user
        groups = auth.get_users_groups(uid)

        # Check no error
        if auth.error:
            return {'error': auth.error, 'groups': None, 'dictionaries': dictionaries}

        return {'error': auth.error, 'groups': groups, 'dictionaries': dictionaries}
       

 

