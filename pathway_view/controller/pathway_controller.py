import json
import pyramid_handlers
from pyramid.renderers import render_to_response

# Config
from pathway_view.config import *

# Models
from pathway_view.model.auth import get_group_by_name
from pathway_view.model.pathways import PathwayModel

# Controllers and helpers
import pathway_view.controller.helper.cookie_auth as cookie_auth
from pathway_view.controller.base import BaseController

class PathwayController(BaseController):

    def init_variables(self):
        self.pathways = None
        self.error = None
        self.json_pathways = None

    def to_dict(self):
        return self.__dict__


    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/pathways.mako',
                             request_method='GET',
                             name='pathways/index')
    def index(self):
        # Takes the user to the main pathway page
        # Has a list of pathways the user can view
        # Lets the user add pathways etc
        uid = self.logged_in_user_id

        if not uid:
            self.redirect('auth/login')

        pathway_model = PathwayModel()
        pathways = pathway_model.get_all_pathways_for_user(uid)

        if isinstance(pathways, str):   
            self.error = pathways
        else:
            self.pathways = pathways
            self.json_pathways = json.dumps(pathways)

        return self.to_dict()


    def view_pathway(self):
        # Code will direct the user to a pathway summary page
        return {}




    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/pathways.mako',
                             request_method='POST', 
                             name='pathways/index')
    def add_pathway(self):

        uid = self.logged_in_user_id

        if not uid:
            self.redirect('auth/login')

        pathway_model = PathwayModel()
        pathway_model.from_dict_upload(self.request.POST)
        pathway_model.owner_id = uid

        error = pathway_model.parse_pathdata()
        
        if pathway_model.error:
            self.error = error

        # Save the pathway
        error = pathway_model.save_pathway()
        
        if pathway_model.error:
            self.error = error
   
        # Add the pathway
        error = pathway_model.add_pathway(uid)

        if pathway_model.error:
            self.error = error

        # Return an updated version of the users's dataset
        pathways = pathway_model.get_all_pathways_for_user(uid)

        if pathway_model.error:
            self.error = error
 
        self.pathways = pathways
        self.json_pathways = json.dumps(pathways)
        return self.to_dict()

