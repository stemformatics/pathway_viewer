# General
import json

# Pyramid
import pyramid_handlers
from pyramid.renderers import render_to_response

# Config
from pathway_view.config import *

# Models
from pathway_view.model.auth import get_group_by_name
from pathway_view.model.datasets import DatasetModel

# Controllers
import pathway_view.controller.helper.cookie_auth as cookie_auth
from pathway_view.controller.helper.suppress import suppress
from pathway_view.controller.base import BaseController

class DatasetController(BaseController):

    def init_variables(self):
        self.datasets = None
        self.error = None
        self.json_datasets = None
        # Allows the user to select a range of dataset types e.g. metabolic
        # flux of gene
        self.supported_dataset_types = SUPPORTED_DATASET_TYPES
        # Allows the user to select a range of ids as identifiers
        # e.g. kegg, chebi etc
        self.supported_id_types = SUPPORTED_METABOLITE_ID_TYPES
        # If there are id errors we want to display these to the user
        self.id_errors = None
        self.id_information = None
   
 
    def to_dict(self):
        return self.__dict__
 

    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/datasets.mako',
                             request_method='GET',
                             name='datasets/index')
    def index(self):
        uid = self.logged_in_user_id

        if not uid:
            self.redirect('auth/login')

        dataset_model = DatasetModel()
        # Get the dataset for a user and pathways
        self.get_datasets_for_user(dataset_model, uid)

        return self.to_dict()


    @suppress()
    def get_datasets_for_user(self, dataset_model, uid):
        datasets =  dataset_model.get_all_datasets_for_user(uid)

        if isinstance(datasets, str):
            self.error = datasets
        else:
            self.datasets = datasets
            self.json_datasets = json.dumps(datasets)



    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/datasets.mako',
                             request_method='POST',
                             name='datasets/index')    
    def add_dataset(self):

        uid = self.logged_in_user_id

        if not uid:
            self.redirect('auth/login')

        dataset_model = DatasetModel()
        self.get_datasets_for_user(dataset_model, uid)

        # --------------------------------------------------------------
        # To disable dataset upload features uncomment the code below
        # ---------------------------------------------------------------
        #if uid == GUEST_USER_ID:
        #    self.error = "Sorry but you cannot add a dataset as a guest user."
        #    return self.to_dict()        
        #else:
        #    self.error = "Adding has been disabled temorarily."
        #    return self.to_dict()


        #dataset_model.from_dict_upload(self.request.POST)
 
        dataset_model.name = self.request.POST.get('dataset_filename')
        dataset_model.upload_file = self.request.POST.get('dataset_upload').file
        dataset_model.uploaded_filename = self.request.POST.get('dataset_upload').filename
        dataset_model.private = self.request.POST.get('access_options')
        dataset_model.owner_type = self.request.POST.get('dataset_options')
        dataset_model.group_name = self.request.POST.get('dataset_group_name')
        dataset_model.notes = self.request.POST.get('notes')
        dataset_model.raw_metabolite_id_type = self.request.POST.get('raw_metabolite_id_type')
        dataset_model.owner_id = uid
    
        # parse the metadata first and see if there is an error with 
        # the users choice of name etc
        error = dataset_model.parse_metadata()
        
        # Get the datasets the user currently has access to
        self.get_datasets_for_user(dataset_model, uid)

        # Add the dataset - see if we can save it
        error = dataset_model.save_dataset()
        
        if dataset_model.error:
            self.error = error
            return self.to_dict()


        # Add the dataset to the metadata table
        error = dataset_model.add_metadata()
        
        if dataset_model.error:
            self.error = error
            return self.to_dict()

        # Parse the data from the dataset 
        unmatched_ids = dataset_model.parse_dataset()

        if dataset_model.error:
            self.error = unmatched_ids
            return self.to_dict()

        # The ID
        self.id_errors = unmatched_ids

        # Return an updated version of the users's dataset 
        self.get_datasets_for_user(dataset_model, uid)
        
        self.id_information = True

        self.error = None

        return self.to_dict()
