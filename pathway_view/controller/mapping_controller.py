# General
import json

# Pyramid
import pyramid_handlers
from pyramid.renderers import render_to_response

# Config
from pathway_view.config import *

# Models
from pathway_view.model.mapping import MappingModel

# Controllers
import pathway_view.controller.helper.cookie_auth as cookie_auth
from pathway_view.controller.helper.suppress import suppress
from pathway_view.controller.base import BaseController

class MappingController(BaseController):

    def init_variables(self):
        self.parsed_file = None
        self.mapped_ids = None
        self.mapping_stats = None
        self.id_types = SUPPORTED_METABOLITE_ID_TYPES
        self.selected_id_types = None       
        self.mapping_model = None # Store an instance of the 
        # MappingModel so that we can access the path etc
        

    def to_dict(self):
        return self.__dict__


    @pyramid_handlers.action(renderer='json',
                             name = 'mapping/save_mapping')
    def get_dataset_data(self):
        """

        Saves the users' mapping as a metabolite list, otherwise
        deletes the mapping file and data.

        """
        uid = self.logged_in_user_id

        if not uid:
            self.redirect('auth/login')
    
        mapping_model = self.mapping_model

        list_model = parse_list_save_info(self.request.POST)
            
        if isinstance(list_model, str):
            # Either an error or letting the user know that 
            # it has been deleted.
            self.error = list_model
            return list_model
        
        # Otherwise we want to save the mappings.
        # Mappings have already been made and are stored in self.mapped_stats file
        success = mapping_model.save_mapping_as_metabolite_list(self.mapped_ids, list_model)
        
        # Return the message -> whether or not the mappings were successfully 
        # saved.
        return success 


    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/mapping.mako',
                             request_method='POST',
                             name='mapping/index')
    def get_id_mappings_from_file(self):
        uid = self.logged_in_user_id

        """

        Maps the users IDs from an uploaded file to
        the identifiers in Omicxview.

        """
        
        if not uid:
            self.redirect('auth/login')

        #test_ds_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/bpa_data/HML_MA_20120707.csv"

        
        mapping_model = MappingModel()

        # Parse the user input.
        success = mapping_model.parse_user_input(self.request.POST, self.id_types)
        
        # The ID types as chosen by the user
        selected_id_types = mapping_model.selected_id_types

        # Save the file as a tempory file so that if the user chooses 
        # to save the file or get rid of it.
        success = mapping_model.save_temp_id_list_as_file()

        # Parse the file to get the ID list to map.
        parsed_file = mapping_model.file_to_id_list(mapping_model.tmp_path)

        # Get the mappings between the users raw ID and the
        # IDs used in Omicxview.
        id_mappings = mapping_model.get_id_mapping(parsed_file['parsed_id_list'], selected_id_types)

        # The stats from the mapping file in addition to the mappings
        # between the file and the app.
        mapping_stats = mapping_model.get_stats_on_mapping(parsed_file['parsed_to_raw_id_dict'], id_mappings, selected_id_types)

        # Return the mapped IDs and the stats separately
        self.mapped_ids = mapping_stats['mapping_dict_with_raw_ids']
        self.mapping_stats = mapping_stats['mapped_id_stats']

        self.selected_id_types = selected_id_types

        # Delete the temporary file
        # success = mapping_model.delete_tmp_file()
        
        # Store the mapping model instance
        self.mapping_model = mapping_model

        return self.to_dict()



    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/mapping.mako',
                             request_method='GET',
                             name='mapping/index')
    def index(self):

        uid = self.logged_in_user_id

        if not uid:
            self.redirect('auth/login')

        return self.to_dict()
