# Pyramid
import pyramid_handlers
from pyramid.renderers import render_to_response

# Controllers and helpers
import pathway_view.controller.helper.cookie_auth as cookie_auth
from pathway_view.controller.base import BaseController

# Model
from pathway_view.model.auth import AuthModel

# Config
from pathway_view.config import *


class AuthController(BaseController):

    # --------------------- POST action handlers for forms etc ------------------ #
    # From https://github.com/mikeckennedy/python-for-entrepreneurs-course-demos
    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/login.mako',
                             name='auth/login')
    def login(self):
        # First check if a user is already logged in
        uid = self.logged_in_user_id

        if isinstance(uid, int):
            self.redirect('/')

        if self.request.method != 'POST':
            return {'error': None}

        auth = AuthModel()
        auth.from_dict_login(self.request.POST)
        
        # Try to login the user
        auth.login()

        # If there is an error a string will be returned with the error
        # message
        if auth.error:
            return {'error': auth.error}

        # Otherwise add the uid and role to session
        cookie_auth.set_auth(self.request, str(auth.uid))

        return self.redirect('/')


    # Completing registration - they have recieved a code by email and need
    # to set a password
    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/conf.mako',
                             name='auth/confirm_registration')
    def confirm_registration(self):
        uid = self.logged_in_user_id

        if isinstance(uid, int):
            self.redirect('/')

        # On get just return the page
        if self.request.method != 'POST':
            return {'error': None}

        auth = AuthModel()
        auth.from_dict_login(self.request.POST)
        code = str(self.request.POST.get('code'))
        
        # Try to complete registeration
        user = auth.register(code)
        

        # Check if an error was generated
        if auth.error:
            print(user)
            return {'error': user}

        # Otherwise the user was able to successfully register so we redirect them home
        # First we need to store them in the cookie so that we know they are logged in
        cookie_auth.set_auth(self.request, str(user[0]))

        return self.redirect('/')



    # User registering for the first time they aren't actually allowed to login
    # yet
    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/register.mako',
                             name='auth/register')
    def register(self):
        uid = self.logged_in_user_id

        if isinstance(uid, int):
            self.redirect('/')

        if self.request.method != 'POST':
            return {'error': None}

        auth = AuthModel()
        
        auth.from_dict_register(self.request.POST)

        # Check that the emails matched
        if auth.error != None:
            return {'error:': auth.error}
        
        # Otherwise try registering the user
        auth.add_new_user()

        # Check if an error occured
        if auth.error != None:
            return {'error:': auth.error}

        return self.redirect('/auth/confirm_registration')


    # -------------------- Actions from buttons -------------------------------------#

    # Since we don't need to render anything we don't need the renderer at the top
    @pyramid_handlers.action(name='auth/logout')
    def logout(self):
        cookie_auth.logout(self.request)
        self.redirect('/')


    # --------------------- Home page --------------------------------------------#    
    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/home.mako',
                             request_method='GET',
                             name='home')
    def index(self):
        return {}

    # --------------------- Demo page --------------------------------------------#

    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/help.mako',
                             request_method='GET',
                             name='auth/help')
    def help(self):
        return {}

