# General
import json

# Pyramid
import pyramid_handlers
from pyramid.renderers import render_to_response

# Controllers and helpers
import pathway_view.controller.helper.cookie_auth as cookie_auth
from pathway_view.controller.base import BaseController

# Models
from pathway_view.model.pathways import PathwayModel
from pathway_view.model.datasets import DatasetModel
from pathway_view.model.metabolite_lists import MetaboliteListModel
from pathway_view.model.mapping import MappingModel
from pathway_view.model.auth import AuthModel


# Config
from pathway_view.config import *

class VisController(BaseController):
    
    # properties of the vis controller
    def init_variables(self):
        self.var_url = WEB_PATH + '/pathway_view_public/js/vis/variables.js'
        self.parse_url = WEB_PATH + '/pathway_view_public/js/vis/parsing_escher.js'
        self.graph_url = WEB_PATH + '/pathway_view_public/js/vis/graph_overlay.js'
        # The datasets and pathways which a user has access to
        self.pathways = None
        self.datasets = None
        self.metabolite_lists = None
        self.mappings = None
        self.error = ""
        # The pathway a user has chosen (or a default)
        self.json_str = None
        # The dictionary which contains mappings from bigg to other
        # to display on clicking
        self.bigg_str = None
        # the info to display in the modal
        self.molecule_display_info = MOLECULE_DISPLAY_INFO
        # Metabolite list info to display in the modal
        self.metabolite_list_header = METABOLITE_LIST_HEADER
        self.login_method = None        


    def to_dict(self):
        return self.__dict__


    # Ajax call when user selects to change dataset
    @pyramid_handlers.action(renderer='json',
                             name = 'vis/get_dataset_data')
    def get_dataset_data(self):
        # Get the dataset based on the id passed by the AJAX call
        uid = self.logged_in_user_id

        if not uid:
            uid = GUEST_USER_ID
            #self.redirect('/auth/login')

        dataset_id = self.request.POST.get('dataset_id')

        metabolite_list = self.get_metabolite_list(dataset_id, uid)

        dataset = self.get_dataset(dataset_id, uid)

        return {'dataset': dataset, 'metabolite_list': metabolite_list}


    # Ajax call to get a list to add to a patway
    @pyramid_handlers.action(renderer='json',
                             name = 'vis/get_metabolite_list_data')
    def get_metabolite_list_data(self):
        uid = self.logged_in_user_id
        if not uid:
            uid = GUEST_USER_ID
            #self.redirect('/auth/login')

        metabolite_list_model = MetaboliteListModel()
        metabolite_list_id = self.request.POST.get('metabolite_list_id')
        print(metabolite_list_id)
        try:
            metabolite_list_id = int(metabolite_list_id)
            metabolite_list_info = metabolite_list_model.get_processed_metabolite_list(metabolite_list_id, uid)
        except:
            self.error += "No metabolite list was selected."
            metabolite_list_info = 'na'

        return metabolite_list_info


    # Ajax call to get the mapping information for a metabolite list
    @pyramid_handlers.action(renderer='json',
                             name = 'vis/get_metabolite_list_mapping_data')
    def get_metabolite_list_mapping_data(self):
        uid = self.logged_in_user_id
        if not uid:
            uid = GUEST_USER_ID
            #self.redirect('/auth/login')

        mapping_model = MappingModel()
        metabolite_list_id = self.request.POST.get('metabolite_list_id')

        try:
            metabolite_list_id = int(metabolite_list_id)
            metabolite_list_info = mapping_model.get_mapping_metabolite_list_info(metabolite_list_id, uid)
        except:
            self.error += "No metabolite list was selected."
            metabolite_list_info = 'na'

        return json.dumps(metabolite_list_info)


    # Ajax call when the user selects to change pathway
    @pyramid_handlers.action(renderer='json',
                             name='vis/get_pathway_data')
    def get_pathway_data(self):
        # Get the pathway based on the id passed by the AJAX call
        uid = self.logged_in_user_id
        if not uid:
            uid = GUEST_USER_ID
        #    self.redirect('/auth/login')

        pathway_model = PathwayModel()
        
        pathway_id = self.request.POST.get('pathway_id')

        try:
            pathway_id = int(pathway_id)
            pathway_info = pathway_model.get_pathway(pathway_id)
        except:
            return "No pathway was selected "

        return self.path_ds_list_to_json(pathway_info)


    @pyramid_handlers.action(renderer='templates/visualisations.mako',
                             request_method='GET',
                             name='vis/pathway_viewer') 
    def pathway_visualiser(self):
        # Returns the main vis page
        # Has a dropdown of the pathways and datasets the user can add
        # Has the vis if the user has selected these
        # Gets the pathways from pathway model
        # Gets datasets from dataset controller
        uid = self.logged_in_user_id

       
        """
        URLs can contain a token which auto logs in a user.

        This enables datasets to be publicly available.

        Test the URL if no user is logged in. Filter the URL 
        to search for a code. If there is no code at the end of the URL 
        then reroute the user to login, otherwise set the uid to 
        be the UID associated with the group.

        """
 
        # If no user was logged in and the token either didn't exist or
        # wasn't correct log the user in as a public user
        if not uid or isinstance(uid, str):
            uid = GUEST_USER_ID
            self.public_user = True
           

        pathway_model = PathwayModel()
        dataset_model = DatasetModel()
        mapping_model = MappingModel()
        metabolite_list_model = MetaboliteListModel()

        pathway_model.uid = uid
        dataset_model.uid = uid

        # Get all the pathways and datasets associated with a user
        self.pathways = pathway_model.get_all_pathways_for_user(uid)
        self.datasets = dataset_model.get_all_datasets_for_user(uid)
        self.metabolite_lists = metabolite_list_model.get_all_processed_metabolite_lists_for_user(uid)

        # Get the pathway information based on a path_id passed in via the URL
        # or et patwhay information for the default pathway (returns dict)
        pathway_info = self.get_pathway_information(self.request.params.get("path_id", None))

        # Similarly get the dataset as a json object for the provided ds_id
        # or no dataset ID
        dataset = self.get_dataset(self.request.params.get("ds_id", None), uid)
        

        # Get the metabolite list json associated as the default list associated with 
        # a dataset.
        metabolite_list = self.get_metabolite_list(self.request.params.get("ds_id", None), uid)
 
        if isinstance(self.pathways, str):
            self.error = pathways
            self.pathways = None
            pathways = 'na'
            return self.to_dict()

        if isinstance(self.datasets, str):
            self.datasets = None
            self.error += datasets
            datasets = 'na'

        self.json_str = self.path_ds_list_to_json(pathway_info, dataset, metabolite_list, self.pathways, self.datasets)

        # Add the json dict for the bigg to chebi to inchi etc
        self.mappings = json.dumps(mapping_model.get_vis_pathway_mapping()) 

        # The data is in the form of the paths to the JS files for rendering the paths
        # And the JSON string (stored in the pathway obj)
        # Need to then access these in the html file
        return self.to_dict()


    def get_metabolite_list(self, ds_id, uid):
        """

	    Gets the default metabolite list for a dataset
        and returns it as a JSON string.

        Otherwise returns 'na' as a string to signify no
        metabolite list associated with that dataset selected.

        """
        dataset_model = DatasetModel()
        metabolite_list_model = MetaboliteListModel()

        # isdigit checks that the ds_id is a positive integer
        if ds_id and ds_id.isdigit():
            metabolite_list_id = dataset_model.get_default_list_id(int(ds_id))
           
            metabolite_list_json = metabolite_list_model.get_processed_metabolite_list(metabolite_list_id, uid)#json.dumps(metabolite_list_info)

            return metabolite_list_json

        return 'na'

    def get_dataset(self, ds_id, uid):
        """

        Gets the dataset specified by the user in the URL
        and returns it as a JSON string.

        Otherwise returns 'na' as a string to signify no 
        dataset selected.

        """
        dataset_model = DatasetModel()

        # isdigit checks that the ds_id is a positive integer
        if ds_id and ds_id.isdigit():
            dataset_info = dataset_model.get_dataset(int(ds_id), uid)
            return dataset_info

        return 'na' 


    def get_pathway_information(self, path_id):
        """

        Gets the pathway information for the user given an 
        inpiutted path_id in the URL. Otherwise returns default 
        pathway.

        """
        # Set pathway_info to be an empty string so we can set the 
        # error either by the returned method or the empty string.
        pathway_info = ""

        pathway_model = PathwayModel()

        # is digit checks the id is apositive integer
        if path_id and path_id.isdigit():
            pathway_info = pathway_model.get_pathway(int(path_id))
            # If the pathway_information is a string it was an error
            # otherwise it returns a dictionary object.
            if not isinstance(pathway_info, str):
                return pathway_info
        
        # Either the user didn't specify a pathway or they didn't have access 
        # or the specified pathway didn't exist. Add the error to the 
        # controller.
        self.error += pathway_info

        # Get the default pathway for the user. 
        pathway_info = pathway_model.get_pathway(DEFAULT_PATHWAY_ID)

        # Check there were no errors with the default pathway.
        if isinstance(pathway_info, str):
            self.error += pathway_info
            return None

        return pathway_info
    


    def path_ds_list_to_json(self, pathway_info, dataset_info="na", list_info="na", pathway_list="na", dataset_list="na"):
        """

        Takes path information dictionary, dataset string and list string
        combines and parses it to a json object for the view.

        """ 

        json_str = pathway_info['json']
        pathway = pathway_info['pathway']

        # Add header info to the jsonStr need to remove the outer 
        # elements of the jsonStr [] to add the info.
        json_header = json.dumps({'pathway': {'header': {'id': str(pathway[ID]), 'name': pathway[0], 'notes': pathway[1]}, 'data': json_str}, 'dataset': dataset_info, 'default_metabolite_list': list_info, 'pathway_list': pathway_list, 'dataset_list': dataset_list})
        print(list_info)
        #json_str = json_header[:-1] + "," + json_str[1:-1] + "]"

        return json_header
