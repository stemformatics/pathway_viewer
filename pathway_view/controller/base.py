# Pyramid
import pyramid.renderers
import pyramid.httpexceptions as exc

# Controllers and helpers
import pathway_view.controller.helper.cookie_auth as cookie_auth
from pathway_view.config import *

# Model
from pathway_view.model.auth import AuthModel

# From python course
# https://github.com/mikeckennedy/python-for-entrepreneurs-course-demos/blob/master/12_user_accounts/blue_yellow_app_12/blue_yellow_app/controllers/base_controller.py
class BaseController:
    def __init__(self, request):
        self.request = request

        # Check if we need to login the user via
        # the token in the URL.

        self.logged_in_user_id = self.login_user()

        # A method which is overridden by each of the controllers
        # such that the variables specific to that controller are initialised.
        self.init_variables()

    def init_variables(self):
        self.error = None


    @property
    def is_logged_in(self):
        return False

    # noinspection PyMethodMayBeStatic
    def redirect(self, to_url, permanent=False):
        if permanent:
            raise exc.HTTPMovedPermanently(to_url)
        raise exc.HTTPFound(to_url)

    def login_user_by_cookie(self):
        # Check if there is a cookie set in the browser
        return cookie_auth.get_user_id_via_auth_cookie(self.request)

    @property
    def web_path(self):
        return WEB_PATH + "/pathway_view_public"


    def login_user(self):
        """
        Method for logging the user in via
        a token in the URL.

        Called when the controller is initialised.

        Otherwise logs in the user via the cookie

        (or returns no user_id if there is no user
        stored in the cookie or no token in the URL)

        """        

        # Check if token in URL
        if 'token' in self.request.GET:
    
            auth = AuthModel()

            uid = auth.get_user_by_token(self.request.GET['token'])

            return auth.login_user_by_token(self.request, uid)

        else:
            return self.login_user_by_cookie()

    @property
    def logged_in_user_name(self):
        uid = int(self.logged_in_user_id)
        name = None

        if not uid:
            return None

        # Get the user by their ID
        auth = AuthModel()
        name = auth.get_user_name_by_uid(uid)
         
        if auth.error:
            print(name, auth.error)
            name = None 
    
        return {'error': auth.error, 'name': name}

    @property
    def logged_in_user_role(self):
        uid = int(self.logged_in_user_id)
        role = None

        # If they aren't logged in they don't have a role and will be
        # redirected to the login page
        if not uid:
            return None

        auth = AuthModel()
        role = auth.get_user_role_by_uid(uid)

        # If there weren't any errors we want to get the users role
        if auth.error:
           role = None     

        print('role', role)
        return {'error': auth.error, 'role': role}
