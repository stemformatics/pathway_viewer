import os
import uuid
import shutil

# Model imports
from pathway_view.model.auth import get_group_by_name
from pathway_view.model.base import BaseModel
from pathway_view.model.mapping import MappingModel
from pathway_view.model.metabolite_lists import MetaboliteListModel
from pathway_view.model.auth import AuthModel


from pathway_view.config import *


import csv


class DatasetModel(BaseModel):

    # Stores these to easily return them as a dictionary
    def __init__(self):
        self.ds_id = None
        self.name = None
        self.error = False
        self.owner_id = None
        self.path = None
        self.internal_call = False
        # Only supports metabolic data at the moment.
        self.data_type = "metabolic"

    # from http://docs.pylonsproject.org/projects/pyramid_cookbook/en/latest/forms/file_uploads.html
    def save_dataset(self):
        filename = self.name
        input_file = self.upload_file
        path = PATH + "datasets/" 
        # Check the user has provided a name and a file
        if not isinstance(filename, str):
            self.error = True
            return "There was an issue with the file or filename you uploaded, please try again."

        # File may throw an error so surround with try catch
        try:

            file_path = os.path.join(path, '%s.tsv' % filename)

            temp_file_path = file_path + '~'
            input_file.seek(0)

            with open(temp_file_path, 'wb') as output_file:
                shutil.copyfileobj(input_file, output_file)

            os.rename(temp_file_path, file_path)
           
            # update the path so that we can have access to the file
            self.path = file_path
            return "success"
        
        except:
            self.error = True
            return "There was an error parsing the file."


    def get_dataset_all_metadata(self, ds_id, uid):
        # Check ds_id an int
        if not isinstance(ds_id, int) or ds_id < 0 or not isinstance(uid, int):
            self.error = True
            return error_codes['id']

        # Check the user has access to that dataset
        if not ds_id in self.get_all_datasets_for_user(uid):
            self.error = True
            return "User doesn't have access to the dataset."

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

        sql = "SELECT * from datasets WHERE id = %s;"

        cursor.execute(sql,(ds_id,))
        dataset = cursor.fetchall()

        # Now we need the data
        sql = "SELECT chebi_id, sample, value from data WHERE ds_id = %s;"
        cursor.execute(sql,(ds_id,))
        all_data = cursor.fetchall()

        if (len(dataset) != 1):
            self.error = True
            return "Couldn't retrieve dataset."


        return {'metadata': dataset[0], 'data': all_data}


    def get_dataset(self, ds_id, uid):
        # Check ds_id an int
        if not isinstance(ds_id, int) or ds_id < 0 or not isinstance(uid, int):
            self.error = True
            return error_codes['id']

        # Check the user has access to that dataset
        if not ds_id in self.get_all_datasets_for_user(uid):
            self.error = True
            return "User doesn't have access to the dataset."

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "SELECT name, notes, private, data_type from datasets WHERE id = %s;"

        cursor.execute(sql,(ds_id,))
        dataset = cursor.fetchall()

        # Now we need the data
        sql = "SELECT chebi_id, sample, value from data WHERE ds_id = %s;"
        cursor.execute(sql,(ds_id,))
        all_data = cursor.fetchall()
        
        if (len(dataset) != 1):
            self.error = True
            return "Couldn't retrieve dataset."


        return {'metadata': dataset[0], 'data': all_data}

        # --------------------------------------------------------------
        # Currently not being executed code
        # --------------------------------------------------------------

        # Get the bigg id for each chebi
        mapping_data = {}
        for data in all_data:
            chebi_id = data[chebi_id]
            if not dataset.verified:
                # Get all the information
                mapping_data[chebi_id]['mapping_info'] = get_mapping_data(ds_id, data)
                mapping_data[chebi_id]['bigg_id'] = get_bigg_for_unverified_chebi(ds_id, chebi_id)
            else:
                # Just get the bigg id so we can match it to the pathway
                mapping_data[chebi_id]['bigg_id'] = get_bigg_for_verified_chebi(ds_id, chebi_id)

        cursor.close()
        conn.close()

        if (len(dataset) != 1):
            self.error = True
            return "Couldn't retrieve dataset."

       
        return {'metadata': dataset[0], 'data': mapping_data}
    
           

    def delete_dataset(self, ds_id, uid):
        # Low level check
        if not isinstance(ds_id, int) or ds_id < 0:
            self.error = True
            return error_codes['id']

        # Check the user has access
        if not ds_id in self.get_all_datasets_for_user(uid):
            self.error = True
            return "User doesn't have access to the dataset."

        auth = AuthModel()

        # first we want to delete all the data associated with it
        if auth.get_user_role_by_uid(uid) != 'admin' and self.internal_call != True:
            self.error = True
            self.internal_call = False
            return "Only admins can delete datasets"
        
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Get the dataset so we can delete it
        sql = "SELECT * from datasets WHERE id = %s;"
        cursor.execute(sql,(ds_id,))
        dataset = cursor.fetchall()
        
        # create the path name based on the name and id
        dataset_path_name = dataset[0][1] + "_" + str(dataset[0][0])
        path = PATH + "datasets/" 
        dataset_path = os.path.join(path, '%s.tsv' % dataset_path_name)

        # deleting all the columns from the data table which contain the values
        sql = "DELETE FROM data WHERE ds_id = %s;"
        cursor.execute(sql,(ds_id,))

        # delete the mappings from the dataset mapping table
        sql = "DELETE FROM dataset_chebi_mapping WHERE ds_id = %s;"
        cursor.execute(sql,(ds_id,))

        # deleting the metadata
        sql = "DELETE FROM datasets WHERE id = %s;"
        cursor.execute(sql,(ds_id,))

        # Test it has been deleted
        sql = "SELECT * from datasets WHERE id = %s;"
        cursor.execute(sql,(ds_id,))
        dataset = cursor.fetchall()

        cursor.close()
        conn.commit()
        conn.close()
        
        # Delete the dataset from the server
        os.remove(dataset_path)

        # Check it was removed
        if dataset:
            self.error = True
            return error_codes['id']

        return "success"


    def parse_metadata(self):
        if not isinstance(self.name, str) or not isinstance(self.notes, str) or not NAME_REGREX.match(self.name):
            self.error = True
            
            return "There was an error with uploaded data."

        # Check the data type is allowed
        if self.data_type not in SUPPORTED_DATASET_TYPES:
            self.error = True
            return "Error with data type"

        # Determine whether it is a pathway for a group or a user
        if self.owner_type == 'group':
            # From the authmodel
            group = get_group_by_name(self.group_name)
            if group == error_codes['name']:
                self.error = True
                return "Error with the uploaded group name"
            # Set owner ID to be the group ID
            self.owner_id = group[ID]

        return "success"      


    def add_metadata(self):

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        
        # Test if the user already has a dataset with the same name
        sql = "SELECT * from datasets where name = %s and owner_id = %s and owner_type = %s;"
        cursor.execute(sql,(self.name, self.owner_id, self.owner_type,))
        dataset = cursor.fetchall()
        
        if dataset:
            self.error = True
            # Need to delete the dataset that was added 
            return "You already have a dataset with that name, please add a new name."

        verified = False
        # Need to add the metadata to the data table
        sql = "INSERT into datasets (name, owner_id, owner_type, notes, private, verified, data_type) VALUES (%s, %s, %s, %s, %s, %s, %s);"
        cursor.execute(sql,(self.name, self.owner_id, self.owner_type, self.notes, self.private, verified, self.data_type,))

        sql = "SELECT * from datasets where name = %s and owner_id = %s and owner_type = %s;"
        cursor.execute(sql,(self.name, self.owner_id, self.owner_type,))        
        dataset = cursor.fetchall()
        cursor.close()
        conn.commit()
        conn.close()
        
        # check it has been added 
        # add the ID to the dataset
        if len(dataset) != 1:
            self.error = True
            # remove the file
            os.remove(self.path)
            return "Wasn't able to add metadata."

        self.ds_id = int(dataset[0][ID])

        # Make the name and id the dataset name, remove white space from the name
        dataset_path_name = self.name.strip() + "_" + str(self.ds_id)
        # if the dataet was able to be added completely
        path = PATH + "datasets/"
        new_file_path = os.path.join(path, '%s.tsv' % dataset_path_name)
        os.rename(self.path, new_file_path)

        self.path = new_file_path

        return dataset[0]
       
 
           
    # The file has been saved so we can read it and parse it to the database 
    def parse_dataset(self):
        filepath = self.path
        ds_id = int(self.ds_id)
        # Make a new mapping model, this will parse the chebi_id for each metabolite
        # the user has uploaded
        mapping_model = MappingModel()

        count = 0
        num_cols = 0

        # Keep track of the samples associated with this ds
        samples = []

        # Store the value errors as a dict as we want to group the errors by their
        # metabolite id's
        value_errors = {}
        # Store a dictionary of the raw metabolite ids and their floats
        raw_metabolite_values = {}
        # Store a list of raw metabolite ids to be able to convert these to their
        # neutral chebi id
        raw_metabolite_ids = {}

        try:
            with open(filepath, 'r') as filein:
                filein = csv.reader(filein, delimiter='\t')
                
                # Read each row
                for row in filein:
                    # If it is the first row we want to store the samples
                    if count == 0:
                        num_cols = len(row) # number of samples
                        for s in row:
                            samples.append(s)
                        """ 
                        Have a restriction that a user must have at minimum one
                        column of data, so they need to columns min (one for the 
                        metabolite name and one for the data.
    
                        Return that error to the user and delete the dataset.
                        """ 
                        if (num_cols < 2):
                            self.error = True
                            self.internal_call = True
                            self.delete_dataset(ds_id, self.owner_id)
                            return "Please check that you're file is in .TSV format. Also files must have at minimum 2 columns, one for the metabolite ID and one for a sample with corrosponding expression values."
                    else:
                        
                        # Metabolite is stored in the first collumn of the users' dataset
                        # we standardise the ids to be the lowercase version
                        met_id = row[0]
                        # Each row should have a unique metabolite id
                        raw_metabolite_values[met_id] = {}

                        for i in range(1, num_cols):
                            # We need to add this to the data table
                            value = row[i]
                            # Do the first check to see if the value is numeric
                            try:
                                val_flt = float(value)
                                # Only look for that ID if it had a proper value
                                # Lower the id as this is what we want to search the db on but have a link back
                                # to the user inputted metab lite id
                                # if it is a name we want to perform more cleaning 
                                if self.raw_metabolite_id_type == 'names':
                                    raw_metabolite_ids[mapping_model.clean_name(met_id)] = met_id
                                else:
                                    raw_metabolite_ids[met_id.lower().strip()] = met_id
                                # They should have a one to one relationship for sampes and metabolites, the same 
                                # metabolite can't have two expression values for the same sample
                                raw_metabolite_values[met_id][samples[i]] = val_flt
                            except:
                                # Collect the metabolites and samples which had errors and return these to the user 
                                value_errors = self.store_dataset_metabolite_value_error(value_errors, met_id, samples[i], value)
                    count += 1
        except Exception as e:
            """
            Weren't able to open the file. Most likely this is because it was not
            a tsv file.
            """
            self.error = True
            self.internal_call = True
            self.delete_dataset(ds_id, self.owner_id)
            return "File wasn't able to be opened, files must be of .TSV format."

        # Now we have a dictionary of the raw metabolite ids and their corrosponding floats
        # We need to assign default chebi ids to this, use the list also generated. Do this in this way
        # as it is quicker than having to do many individual database calls.
        id_errors = mapping_model.assign_neutral_chebi_id(ds_id, self.raw_metabolite_id_type, raw_metabolite_ids, raw_metabolite_values)
    
        # Maybe do a check to test the length of the data added is the same
        # as the size of the dataset
        
        return {'value_errors': value_errors, 'id_errors': id_errors}


    # Helper function when parsing the dataset errors so we can return the error to 
    # the user in a helpful way
    def store_dataset_metabolite_id_error(self, id_errors, metabolite_id, error, row):
        err = {}
        err['metabolite_id'] = metabolite_id
        err['error'] = error
        err['row'] = row
        id_errors.append(err)
        return id_errors

    # Helper function when parsing the dataset for value errors
    def store_dataset_metabolite_value_error(self, value_errors, metabolite_id, sample, value):
        # Check if the metabolie already has errors
        try:
            this_metabolite_errors = value_errors[metabolite_id]
        except:
            this_metabolite_errors = []
        # Add the new error
        err = {}
        err['sample'] = sample
        err['value'] = value
        this_metabolite_errors.append(err)
    
        # Return updated dictionary of errors
        value_errors[metabolite_id] = this_metabolite_errors
        return value_errors 
    


    def get_default_list_id(self, ds_id):
        """
        
        Gets the defult list id for a particular dataset.        

        """
        if not isinstance(ds_id, int) or ds_id < 0:
            self.error = True
            return "Dataset id not an integer."
            

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "select default_list_id from datasets where id = %s;"
        cursor.execute(sql,(ds_id,))

        list_id = cursor.fetchall()

        cursor.close()
        conn.close()

        if len(list_id) != 1:
            self.error = True
            return "No list for that dataset."

        return list_id[0][0]


    def delete_default_list_id(self, uid, ds_id):
        """

        Internal function that removes the defult metabolite
        list ID for a dataset.

        """
        # Check user has access to Dataset
        if not isinstance(ds_id, int) or ds_id not in self.get_all_datasets_for_user(uid):
            self.error = True
            return "Dataset unaccessible to user"

        auth = AuthModel()
        if auth.get_user_role_by_uid(uid) != 'admin':
            self.error = True
            return "Only Admins can delete the default list"
 
        # Remove the list from the dataset.
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "update datasets set default_list_id = NULL where id = %s;"
        cursor.execute(sql,(ds_id,))

        cursor.close()
        conn.commit()
        conn.close()

        return "success"


    def set_default_list_id(self, uid, ds_id, list_id):
        """

        Internal function that sets the defult metabolite
        list ID for a dataset.

        """
        # Check list_id is an integer and returns a list that the user
        # has access to.
        metabolite_list_model = MetaboliteListModel()

        if not isinstance(list_id, int) or list_id not in metabolite_list_model.get_all_processed_metabolite_lists_for_user(uid):
            self.error = True
            return "Metabolite list unaccessible to user"
        
        # Check user has access to Dataset
        if not isinstance(ds_id, int) or ds_id not in self.get_all_datasets_for_user(uid):
            self.error = True
            return "Dataset unaccessible to user"
        

        # Add the list to the dataset.
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "update datasets set default_list_id = %s where id = %s;"
        cursor.execute(sql,(list_id, ds_id,))

        cursor.close()
        conn.commit()
        conn.close()

        return "success"



    def get_all_datasets_for_user(self, uid):
        # low level check
        if not isinstance(uid, int) or uid < 0:
            return error_codes['id']

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "select ds.id, name, owner_id, owner_type, notes, private from group_users as gu left join datasets as ds on (gu.group_id = ds.owner_id and ds.owner_type = 'group' and gu.user_id = %s) order by owner_type;";
        cursor.execute(sql,(uid,))

        # We want all the datasets that are available just to the user
        # and all the datasets which are available to the user within the group
        # Or that are publically avaialable

        datasets_groups = cursor.fetchall()

        sql = "select id, name, owner_id, owner_type, notes, private from datasets where (owner_id = %s and owner_type ='user') or private = 'f';"
        cursor.execute(sql,(uid,))

        datasets_users = cursor.fetchall()

        cursor.close()
        conn.commit()
        conn.close()

        # want to parse the datasets to a dictionary so we can pass it easily
        # to the templates
        user_ds_dict = {}
        for ds in datasets_groups:
            if ds[0] != None:
                ds_dict = {}
                ds_dict['name'] = ds[1]
                ds_dict['owner_id'] = ds[2]
                ds_dict['owner_type'] = ds[3]
                ds_dict['notes'] = ds[4]
                if (ds[5] == 't'):
                    ds_dict['private'] = "private"
                else:
                    ds_dict['private'] = "public"
                user_ds_dict[ds[0]] = ds_dict # 0 contains ds_id
            # note if the user has been given access to a dataset for both
            # group and user, the user access will override it

        for ds in datasets_users:
            if ds[0] != None:
                ds_dict = {}
                ds_dict['name'] = ds[1]
                ds_dict['owner_id'] = ds[2]
                ds_dict['owner_type'] = ds[3]
                ds_dict['notes'] = ds[4]
                if (ds[5] == 't'):
                    ds_dict['private'] = "private"
                else:
                  ds_dict['private'] = "public"
                user_ds_dict[ds[0]] = ds_dict

        return user_ds_dict

