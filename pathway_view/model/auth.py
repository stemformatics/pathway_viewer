# Emailing capability functions
import smtplib
import random
import string
import hashlib
import time

from email.mime.text import MIMEText
from email.parser import Parser

from pathway_view.model.base import BaseModel
from pathway_view.config import *

# Controllers and helpers
import pathway_view.controller.helper.cookie_auth as cookie_auth

class AuthModel(BaseModel):

    # Stores these to easily return them as a dictionary
    def __init__(self):
        self.uid = None
        self.email = None
        self.password = None
        self.role = None
        self.error = None
        self.full_name = None

    def from_dict_login(self, data_dict):
        self.email = str(data_dict.get('email'))
        self.password = str(data_dict.get('password'))
        # Set all users initially to have a normal role
        self.role = 'normal'

    def from_dict_register(self, data_dict):
        self.email = str(data_dict.get('email'))
        self.role = 'normal'
        self.full_name = str(data_dict.get('name'))   
        if self.email != str(data_dict.get('email-conf')):
            self.error = "Your emails didn't match, please try again."

    # Returns all the users in the database
    def get_all_users():
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT * FROM users;"
        cursor.execute(sql,)
        users = cursor.fetchall()
        cursor.close()
        con.commit()
        conn.close()
        return users

    # Returns all the groups
    def get_all_groups():
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT * FROM groups;"
        cursor.execute(sql,)
        groups = cursor.fetchall()
        cursor.close()
        conn.close()
        return groups

    # Adds new user
    # Errors:
    #   bad email address
    #   bad username
    #   username has already been taken
    #   bad password (not > 5 characters)
    def add_new_user(self):

        email = str(self.email)
        full_name = str(self.full_name)

        # Check email
        if not isinstance(email, str) or not EMAIL_REGREX.match(email):
            self.error = error_codes['email']
            return None

        # Have checked it is a string so won't fail here
        email = email.lower()

        # Check username doesn't already exist
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT * FROM users WHERE email = %s;"
        cursor.execute(sql,(email,))
        user = cursor.fetchall()

        if user:
            cursor.close()
            conn.close()
            self.error = "Username is already taken"
            return None

        # Create a code and set that as the password
        code = random.randint(MINCODE, MAXCODE)
        # Encode the password
        code_str = str(code)

        
        # Add the user to the database
        sql = "INSERT INTO users (name, email, password) VALUES (%s, %s, md5(%s));"
        cursor.execute(sql,(full_name, email, code_str,))

        # Check user was added
        sql = "SELECT * FROM users WHERE email = %s;"
        cursor.execute(sql,(email,))
        user = cursor.fetchall()
        cursor.close()
        conn.commit()
        conn.close()

        if len(user) != 1:
            self.error = "User wasn't able to register"
            return None

        # Send register email add code to the email
        # First create header
        msg = MIMEText("Please enter the code to finish registration: " + code_str)
        msg['Subject'] = "Registration email for Omicxview Metabolic Pathway Viewer"
        msg['From'] = FROM
        msg['To'] = email
        email_str = "Please enter the code to finish registration: " + code_str + " link to register page"
        s = smtplib.SMTP('localhost')
        s.sendmail(FROM, email, msg.as_string())
        s.quit()
        
        # Return the user wth the code for testing
        #user[0][PASSWORD] = code_str
        return user[0]




    # returns the user id to store in the session
    def register(self, code):
        email = self.email
        password = self.password

        # low level check for email and password
        if not isinstance(email, str) or not EMAIL_REGREX.match(email):
            self.error = True
            return error_codes['email']

        if not isinstance(password, str) or len(password) < 5:
            self.error = True
            return error_codes['password']

        if not isinstance(code, str) or len(code) != 4:
            self.error = True
            return "The code you entered was incorrect."


        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT * FROM users WHERE email = %s AND password = md5(%s);"
        cursor.execute(sql,(email,code,))
        user = cursor.fetchall()


        # Check that there was exactly 1 user that was retuned
        if len(user) != 1:
            self.error = True
            return error_codes['email_or_password']

        # Update to the new password
        update_password = self.reset_user_password()

        if (self.error == True):
            return update_password

        # Add user to the public group (group id in the config file)
        sql = "INSERT INTO group_users (user_id, group_id) VALUES (%s, %s);"

        cursor.execute(sql,(user[0][0], PUBLIC_GROUP_ID,))
        cursor.close()
        conn.commit()
        conn.close()

        return user[0]


    def login_user_by_token(self, request, uid):
        """ 

        Logs the user in with the token.

        Sets the user in the cookie.

        """
       
        if isinstance(uid, int):

            # Log the previous user out of the controller
            cookie_auth.logout(request)

            # Log the user into the controller
            cookie_auth.set_auth(request, str(uid))

        return uid


       
    # returns the user id to store in the session
    def login(self):
        email = str(self.email)
        password = str(self.password)
    
        # low level check for email and password
        if not isinstance(email, str) or not EMAIL_REGREX.match(email):
            self.error = error_codes['email']
            return None

        if not isinstance(password, str) or len(password) < 5:
            self.error = error_codes['password']
            return None

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT * FROM users WHERE email = %s AND password = md5(%s);"
        cursor.execute(sql,(email,password,))
        user = cursor.fetchall()
        cursor.close()
        conn.close()

        # Check that there was exactly 1 user that was retuned
        if len(user) != 1:
            self.error = error_codes['email_or_password']
            return None

        # If everything was successful we return the user to store them
        # in the session
        self.uid = user[0][ID]





    def generate_login_token(self, uid):
        """
        Only used internally.

        Method for generating a login token to append to the end
        of a URL.

            1. Generate a rnadom string mixed with numbers and letters
            2. Convert this into a hashed version and use this as the token
            3. Save the token to a particular user (or group).
            4. Currently manually done.

        Command to make the column unique:
            ALTER TABLE users ADD CONSTRAINT unique_token UNIQUE (token);
        """
        # From https://docs.python.org/2/library/hashlib.html
        # Makes token 32 chars long 
        uid_time = (str(time.time()) + str(uid)).encode('utf-8')
        token = hashlib.md5(uid_time).hexdigest() 
    
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Update in a try catch incase there is a dup entry
        # as the field is unique
        try:
            sql = "UPDATE users SET token = %s WHERE id = %s;"
            cursor.execute(sql,(token, uid,))

        except psycopg2.Error as e:
            cursor.close()
            conn.close()
            return "There was an error (dup), please try again."

        sql = "SELECT * FROM users WHERE id = %s;"
        cursor.execute(sql,(uid,))
        user = cursor.fetchall()

        cursor.close()
        conn.commit()
        conn.close()

        # There was an error saving the user if we can't access it.
        if len(user) != 1:
            self.error = True
            return "There was an error saving the token to the user id."

        return token



 

    def get_user_by_token(self, token):
        """ 
        Internal method to get the user associated with a certain token.

        """
        # Prelim check that the token is the correct length.

        if not isinstance(token, str) or len(token) != 32:
            self.error = True
            return "Token was incorrect length."

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT ID FROM users WHERE token = %s;"
        cursor.execute(sql,(token,))
        user = cursor.fetchall()
        cursor.close()
        conn.close()

        if len(user) != 1:
            self.error = True
            return "No user attached to token."

        return user[0][0]
    


    def delete_token_for_user(self, uid):
        """
        
        Internal method for deleting a users token.

        """
        if not isinstance(uid, int):
            self.error = True
            return "User ID not an int."

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "UPDATE users set token = NULL where id = %s;"
        cursor.execute(sql,(uid,))

        cursor.close()
        conn.commit()
        conn.close()
        return "success" 




    def get_token_for_user(self, uid):
        """

        Internal method for getting the token associated
        with a user id.

        """ 
        if not isinstance(uid, int):
            self.error = True
            return "User ID not an int."

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT token FROM users WHERE id = %s;"
        cursor.execute(sql,(uid,))
        token = cursor.fetchall()
        cursor.close()
        conn.close()

        if len(token) != 1:
            self.error = True
        
            return "No matching user or token didn't exist."

        return token[0][0]


    def forgot_user_password(self):
        # Set the users email and error to None
        email = self.email
        self.error = None

        # low level check for email
        if not isinstance(email, str) or not EMAIL_REGREX.match(email):
            self.error =  error_codes['email']
            return None

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT * FROM users WHERE email = %s;"
        cursor.execute(sql,(email,))
        user = cursor.fetchall()

        # Check that there was exactly 1 user that was retuned
        if len(user) != 1:
            self.error = error_codes['email']
            return None


        # Create a code and set that as the password
        code = random.randint(MINCODE, MAXCODE)
        # Encode the password
        code_str = str(code)

        # Add the user to the database
        sql = "UPDATE users SET password = md5(%s) WHERE email = %s;"
        cursor.execute(sql,(code_str, email,))

        # Check user was added
        sql = "SELECT * FROM users WHERE email = %s;"
        cursor.execute(sql,(email,))
        user = cursor.fetchall()
        cursor.close()
        conn.commit()
        conn.close()

        if len(user) != 1:
            self.error = "Unable to reset password"
            return None    

        # Send the code to the user via email
        msg = MIMEText("Please enter the code to change your password: " + code_str)
        msg['Subject'] = "Password reset for Omicxview Metabolic Pathway Viewer"
        msg['From'] = FROM
        msg['To'] = email
        email_str = "Please enter the code to reset your password: " + code_str + " link to password reset page"
        s = smtplib.SMTP('localhost')
        s.sendmail(FROM, email, msg.as_string())
        s.quit()


    def reset_user_password(self):
        email = self.email
        password = self.password

        # low level check for email and password
        if not isinstance(email, str) or not EMAIL_REGREX.match(email):
            self.error = error_codes['email']
            return None

        if not isinstance(password, str) or len(password) < 5:
            self.error = error_codes['password']
            return None

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "UPDATE users SET password = md5(%s) WHERE email = %s;"
        cursor.execute(sql,(password, email,))

        # Check user was updated correctly 

        sql = "SELECT * FROM users WHERE email = %s AND password =md5(%s);"
        cursor.execute(sql,(email,password,))
        user = cursor.fetchall()
        cursor.close()
        conn.commit()
        conn.close()

        if len(user) != 1:
            self.error = True
            return  "Unable to change password for the user"


    def get_user_by_email(self, email):
        if not isinstance(email, str) or not EMAIL_REGREX.match(email):
            self.error = error_codes['email']
            return None

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT * FROM users WHERE email = %s;"
        cursor.execute(sql,(email,))
        user = cursor.fetchall()
        cursor.close()
        conn.close()
     
        if len(user) != 1:
            self.error = error_codes[EMAIL]
            return None
        
        return user[0]


    def get_user_role_by_uid(self, uid):

        # Low level check the userid is an integer
        if not isinstance(uid, int) or uid < 0:
            self.error = error_codes['id']
            return None

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT role FROM users WHERE id = %s;"
        cursor.execute(sql,(uid,))
        user = cursor.fetchall()
        cursor.close()
        conn.close()

        # Means duplicate id in which case error or no user for that id
        if len(user) != 1:
            self.error = error_codes['id']
            return None
        
        return user[0][0]


    def get_user_name_by_uid(self, uid):

        # Low level check the userid is an integer
        if not isinstance(uid, int) or uid < 0:
            self.error = error_codes['id']
            return None

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT name FROM users WHERE id = %s;"
        cursor.execute(sql,(uid,))
        user = cursor.fetchall()
        cursor.close()
        conn.close()


        # Means duplicate id in which case error or no user for that id
        if len(user) != 1:
            self.error = error_codes['id']
            return None

        return user[0][0]

    def create_group(self, name):
        uid = int(self.uid)
        role = self.role

        # Low level check the userid is an integer
        if not isinstance(uid, int) or uid < 0:
            self.error =  error_codes['id']
            return None

        # User must have admin privliges to create a group
        if role != 'admin':
            self.error = error_codes['privileges']
            return None

        # Check it is a valid name
        if not isinstance(name, str) or not NAME_REGREX.match(name):
            self.error = error_codes['name']
            return None

        # Check if there is already a group with the same name
        group = get_group_by_name(name)

        # If a group exists with that name it will return the group
        # If not we need to change the error back to none as in get_group_by_name
        # if it can't find teh group it will set the error to a name error
        if group != error_codes['name']:
            self.error = "A group already exists with that name, please enter a new group name"
            return None
        
        # Reinitialising the error to none as we have identified that it doesn't
        # exist
        self.error = None
        
        # Add new group
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "INSERT INTO groups (name, owner_id) VALUES (%s, %s);"
        cursor.execute(sql,(name, uid,)) 

        # Check group was added correctly
        sql = "SELECT * FROM groups WHERE name = %s;"
        cursor.execute(sql,(name,))
        group = cursor.fetchall()

        if len(group) != 1:
            self.error = "Group could not be added"
            return None

        # Add the user to the group in group users
        sql = "INSERT INTO group_users (user_id, group_id) VALUES (%s, %s);"
        cursor.execute(sql,(uid, group[0][ID],))
        cursor.close()
        conn.commit()
        conn.close()

        return group[0]


    def get_users_groups(self, uid):

        # Low level check the userid is an integer
        if not isinstance(uid, int) or uid < 0:
            self.error = error_codes['id']
            return None

        # Selects the names of all groups that a user is part of
        # Connects groups which have the user in them from group users and joins to the groups 
        # table to get the name
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT g.name from groups as g left join group_users as gu on (g.id = gu.group_id) WHERE gu.user_id = %s;"
        cursor.execute(sql,(uid,))
        groups = cursor.fetchall()
        cursor.close()
        conn.close()
        
        return groups


    def get_user_from_group(self, uid, name):

        # Low level check the userid is an integer
        if not isinstance(uid, int) or uid < 0:
            self.error = error_codes['id']
            return None

        if not isinstance(name, str) or not NAME_REGREX.match(name):
            self.error = error_codes['name']
            return None

        # Check user is part of the group
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT gu.user_id from groups as g left join group_users as gu on (g.id = gu.group_id) WHERE gu.user_id = %s and g.name = %s;"
        cursor.execute(sql,(uid, name,))
        user = cursor.fetchall()
        cursor.close()
        conn.close()
        
        # If the user is empty return nothing to identify an error
        if not user:
            self.error = "Not able to add user please try again."
            return None

        return user


        

    def add_user_to_group(self, email, name):
        uid = self.uid
        role = self.role

        # Low level check the userid is an integer
        if not isinstance(uid, int) or uid < 0:
            self.error = error_codes['id']
            return None

        # User must have admin privliges to add users to a group
        if not isinstance(role, str) or role != 'admin':
            self.error = error_codes['privileges']
            return None

        # Check if the user is already part of the group
        user = self.get_user_by_email(email)
        if user == None:
            self.error = "Error in users' email: " + str(email)
            return None

        # Get the group by name
        group = get_group_by_name(name)

        if group == error_codes['name']:
            self.error = "The group " + str(name) + " doesn't exist. Please check the name and try again"
            return None

        # Check user is not already part of group
        gid = group[ID]
        uid = user[ID]

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT * from group_users WHERE user_id = %s AND group_id = %s;"
        cursor.execute(sql,(uid, gid,))
        user = cursor.fetchall()
        
        if user:
            cursor.close()
            conn.close()
            self.error = "User " + email + " was already part of group " + name
            return None

        # Add the user to the group
        sql = "INSERT INTO group_users (user_id, group_id) VALUES (%s, %s);"
        cursor.execute(sql,(uid, gid,))

        # Check user has been added to the group
        sql = "SELECT * from group_users WHERE user_id = %s AND group_id = %s;"
        cursor.execute(sql,(uid, gid,))
        user = cursor.fetchall()
        cursor.close()
        conn.commit()
        conn.close()

        if len(user) != 1:
            self.error = "User " + str(email) + " couldn't be added to group " + str(name)
            return None

        return "Success"

    def delete_user(self, uid):
        # Check the user performing the action is an admin
        if self.role != 'admin':
            return error_codes['privileges']

        # Low level check the userid is an integer
        if not isinstance(uid, int) or uid < 0:
            self.error = error_codes['id']
            return None

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Delete the user from the groups column
        sql = "DELETE from group_users where user_id = %s;"
        cursor.execute(sql,(uid,))

        sql = "SELECT * from group_users WHERE user_id = %s;"
        cursor.execute(sql,(uid,))
        user = cursor.fetchall()
        
        # Delete any groups which the user owns
        sql = "SELECT * from groups WHERE owner_id = %s;"
        cursor.execute(sql,(uid,))
        groups = cursor.fetchall()

        # Need to delete all these groups
        for g in groups:
            ret = delete_group(self, g[ID])
            # There was some error deleting the group
            if not ret:
                return None

        # Return an error
        if user:
            self.error = "Error deleting user."
            return None
        
        sql = "DELETE from users where id = %s;"
        cursor.execute(sql,(uid,))
        
        # Check user was actually deleted
        sql = "SELECT * from users where id = %s;"
        cursor.execute(sql,(uid,))
        user = cursor.fetchall()
        cursor.close()
        conn.commit()
        conn.close()
        
        if user:
            self.error = "Error deleting user."
            return None


    def delete_group(self, gid):
        # Check the user performing the action is an admin user
        if self.role != 'admin':
            return error_codes['privileges']

        # Low level check the userid is an integer
        if not isinstance(gid, int) or gid < 0:
            self.error = error_codes['id']
            return None

        # Need to delete the group from the group_users table first
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "DELETE from group_users where group_id = %s;"
        cursor.execute(sql,(gid,))

        sql = "SELECT * from group_users WHERE group_id = %s;"
        cursor.execute(sql,(gid,))
        group = cursor.fetchall()
        
        # Check that we could remove all entries
        if group:
            self.error = "Error deleting group."
            return None

        # Now we can actually delete the group
        sql = "DELETE from groups where id = %s;"
        cursor.execute(sql,(gid,))

        # Check user was actually deleted
        sql = "SELECT * from groups where id = %s;"
        cursor.execute(sql,(gid,))
        group = cursor.fetchall()
        cursor.close()
        conn.commit()
        conn.close()

        if group:
            self.error = "Error deleting group."
            return None
        return "Success"



    def remove_user_from_group(self, email, name):
        uid = self.uid
        role = self.role

        # Low level check the userid is an integer
        if not isinstance(uid, int) or uid < 0:
            self.error = error_codes['id']
            return None

        # User must have admin privliges to add users to a group
        if not isinstance(role, str) or role != 'admin':
            self.error = error_codes['privileges']

        # Check if the user is already part of the group
        user = self.get_user_by_email(email)
        if user == None:
            self.error = "Error in users' email: " + str(email)
            return None

        # Get the group by name
        group = get_group_by_name(name)

        if group == error_codes['name']:
            self.error =  "The group " + str(name) + " doesn't exist. Please check the name and try again"
            return None

        gid = group[ID]
        uid = user[ID]

        # Check user was part of the group
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT * from group_users WHERE user_id = %s AND group_id = %s;"
        cursor.execute(sql,(uid, gid,))
        user = cursor.fetchall()

        if not user:
            cursor.close()
            conn.close()
            self.error = "User " + str(email) + " wasn't part of the group " + str(name) + " no changes made."
            return None


        sql = "DELETE FROM group_users WHERE user_id = %s AND group_id = %s;"
        cursor.execute(sql,(uid, gid,))

        # check it has actually been deleted
        sql = "SELECT * from group_users WHERE user_id = %s AND group_id = %s;"
        cursor.execute(sql,(uid, gid,))
        user = cursor.fetchall()
        cursor.close()
        conn.commit()
        conn.close()

        if user:
            self.error = "User " + str(email) + " couldn't be removed from group " + str(name)
            return None

        return 'Success'


def get_group_by_name(name):
                                           
    if not isinstance(name, str) or not NAME_REGREX.match(name):
        return error_codes['name']


    conn = psycopg2.connect(psycopg2_conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql = "SELECT * FROM groups WHERE name = %s;"
    cursor.execute(sql,(name,))
    group = cursor.fetchall()
    cursor.close()
    conn.close()

    if len(group) != 1:
        return error_codes['name']

    return group[0]

