import os
import uuid
import shutil
import datetime

from pathway_view.model.base import BaseModel
from pathway_view.config import *
from pathway_view.model.auth import get_group_by_name

import csv


class PathwayModel(BaseModel):

    # Stores these to easily return them as a dictionary
    def __init__(self):
        self.path_id = None
        self.name = None
        self.error = None
        self.owner_id = None

    def from_dict_upload(self, data_dict):
        self.name = str(data_dict.get('pathway_filename'))
        self.upload_file = data_dict.get('pathway_upload').file
        self.upload_filename = str(data_dict.get('pathway_upload').filename)
        self.private = str(data_dict.get('pathway_access_options'))
        self.owner_type = str(data_dict.get('pathway_options'))
        self.notes = str(data_dict.get('pathway_notes'))
        self.validated = data_dict.get('validated')
        self.group_name = str(data_dict.get('pathway_group_name'))
        self.path = None
        self.error = False

    # from http://docs.pylonsproject.org/projects/pyramid_cookbook/en/latest/forms/file_uploads.html
    def save_pathway(self):
        filename = self.name
        input_file = self.upload_file
        path = PATH + "pathways/"
        # Check the user has provided a name and a file
        if not isinstance(filename, str):
            self.error = True
            return "There was an issue with the file or filename you uploaded, please try again."

        # File may throw an error so surround with try catch
        try:

            file_path = os.path.join(path, '%s.JSON' % filename)

            temp_file_path = file_path + '~'
            input_file.seek(0)

            with open(temp_file_path, 'wb') as output_file:
                shutil.copyfileobj(input_file, output_file)

            os.rename(temp_file_path, file_path)
           
            # update the path so that we can have access to the file
            self.path = file_path
            return "success"
        
        except:
            self.error = True
            return "There was an error parsing the file."


    def get_pathway(self, path_id):
        # Check ds_id an int
        if not isinstance(path_id, int) or path_id < 0:
            self.error = True
            return error_codes['id']

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "SELECT name, notes, private, validated, filepath from pathways WHERE id = %s;"
        cursor.execute(sql,(path_id,))
        pathway = cursor.fetchall()

        cursor.close()
        conn.close()

        if (len(pathway) != 1):
            self.error = True
            return "Couldn't retrieve pathway."

        # Want to return the JSON file as TEXT the file path is stored in the 2nd col
        jsonStr = ""
        with open(pathway[0][4], 'r') as f:
            for line in f:
                jsonStr += line
       
        # Don't want to pass the file path to the front end so remove the filepath
        pathway[0][4] = ""

        return {'pathway': pathway[0], 'json': jsonStr}
       

    def delete_pathway(self, path_id):
        # Low level check
        if not isinstance(path_id, int) or path_id < 0:
            self.error = True
            return error_codes['id']

        # first we want to delete all the data associated with it
        
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "SELECT * from pathways WHERE id = %s;"
        cursor.execute(sql,(path_id,))
        pathway = cursor.fetchall()

        # Get the path name for deletion
        pathway_path = pathway[0][2]

        # deleting the pathway
        sql = "DELETE FROM pathways WHERE id = %s;"
        cursor.execute(sql,(path_id,))

        # Test it has been deleted
        sql = "SELECT * from pathways WHERE id = %s;"
        cursor.execute(sql,(path_id,))
        pathway = cursor.fetchall()

        cursor.close()
        conn.commit()
        conn.close()

        # remove it from the server
        os.remove(pathway_path)

        # Check it was removed
        if pathway:
            self.error = True
            return error_codes['id']

        return "success"


    def parse_pathdata(self):
        if not isinstance(self.name, str) or not isinstance(self.notes, str) or not NAME_REGREX.match(self.name):
            self.error = True
            return "There was an error with uploaded data."

        # Need to set the validated
        if self.validated == 'on':
            self.validated = 't'
        else:
            self.validated = 'f'

        # Determine whether it is a pathway for a group or a user
        if self.owner_type == 'group':
            # From the authmodel
            group = get_group_by_name(self.group_name)
            if group == error_codes['name']:
                self.error = True
                return "Error with the uploaded group name"
            # set the owner_id to be the group id
            self.owner_id = group[ID]

        return "success"      


    def add_pathway(self, uid):
        # Check the metadata doesn't have any obvious errors

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        
        # If the user hasn't selected to validate it we need to set the uid to None
        # and the date to None as well
        if (self.validated == 'f'):
            uid = None
            date = None
        else:
            # Get the current time from the system and store it as a string
            date = datetime.datetime.now().strftime("%Y-%m-%d")

        # Check if the user already has a pathway with that name
        sql = "SELECT * from pathways where name = %s;"
        cursor.execute(sql,(self.name,))
        pathway = cursor.fetchall()
        
        if pathway:
            # Remove the pathway
            os.remove(self.path)
            self.error = True
            return "A pathway already exists with that name please select another name."            

        # Need to add the metadata to the data table
        sql = "INSERT into pathways (name, filepath, owner_id, owner_type, notes, private, validated, validator_id, validate_date ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);"
        cursor.execute(sql,(self.name, self.path, self.owner_id, self.owner_type, self.notes, self.private, self.validated, uid, date))

        sql = "SELECT * from pathways where name = %s and owner_id = %s and owner_type = %s;"
        cursor.execute(sql,(self.name, self.owner_id, self.owner_type,))        
        pathway = cursor.fetchall()
       
        # check it has been added 
        # add the ID to the dataset
        if len(pathway) != 1:
            # remove the path
            os.remove(self.path)
            self.error = True
            return "Wasn't able to add pathway."

        self.path_id = int(pathway[0][ID])
       
        # Make the name of the file path the name and the id
        path_name = self.name + "_" + str(self.path_id )
        path = PATH + "pathways/"
        # Change the pathway file name to be the id of the file
        new_file_path = os.path.join(path, '%s.JSON' % path_name)

        # Need to update the pathway filepath in the database
        sql = "UPDATE pathways SET filepath=%s where id=%s;"
        cursor.execute(sql,(new_file_path, self.path_id,))

        cursor.close()
        conn.commit()
        conn.close()

        os.rename(self.path, new_file_path)


        return pathway[0]
       
 
           
    def get_all_pathways_for_user(self, uid):
        # low level check
        if not isinstance(uid, int) or uid < 0:
            return error_codes['id']

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "select p.id, name, owner_id, owner_type, notes, private, validated, validate_date from group_users as gu left join pathways as p on (gu.group_id = p.owner_id and p.owner_type = 'group' and gu.user_id = %s) order by owner_type;"
        cursor.execute(sql,(uid,))

        # We want all the datasets that are available just to the user
        # and all the datasets which are available to the user within the group

        pathways_groups = cursor.fetchall()

        sql = "select id, name, owner_id, owner_type, notes, private, validated, validate_date from pathways where (owner_id = %s and owner_type ='user') or private = 'f';" 
        cursor.execute(sql,(uid,))

        pathways_users = cursor.fetchall()

        cursor.close()
        conn.commit()
        conn.close()

        # want to parse the datasets to a dictionary so we can pass it easily
        # to the templates
        user_pathway_dict = {}
        for p in pathways_groups:
            if p[0] != None:
                path_dict = {}
                path_dict['name'] = p[1]
                path_dict['owner_id'] = p[2]
                path_dict['owner_type'] = p[3]
                path_dict['notes'] = p[4]
                if (p[5] == 't'):
                    path_dict['private'] = "private"
                else:
                    path_dict['private'] = "public"
                if (p[6] == 't'):
                    path_dict['validated'] = "yes on: " + p[7]
                else:
                    path_dict['validated'] = "no"


                user_pathway_dict[p[0]] = path_dict # 0 contains pathway_id
              # note if the user has been given access to a pathway for both
              # group and user, the user access will override it


        for p in pathways_users:
            if p[0] != None:
                path_dict = {}
                path_dict['name'] = p[1]
                path_dict['owner_id'] = p[2]
                path_dict['owner_type'] = p[3]
                path_dict['notes'] = p[4]
                if (p[5] == 't'):
                    path_dict['private'] = "private"
                else:
                    path_dict['private'] = "public"
                if (p[6] == 't'):
                    path_dict['validated'] = "yes on: " + p[7]
                else:
                    path_dict['validated'] = "no"



            user_pathway_dict[p[0]] = path_dict

        return user_pathway_dict

