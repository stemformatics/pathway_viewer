import os
import uuid
import shutil
import random
import datetime
import time

# Import the config file
from pathway_view.config import *

# Import the base model
from pathway_view.model.base import BaseModel
from pathway_view.model.metabolite_lists import MetaboliteListModel
import csv


PATH = '/var/www/pathway_viewer/pathway_viewer/pathway_view/uploads/lists/'

class MappingModel(BaseModel):

    def __init__(self):
        self.error = None
        self.upload_file = None
        self.id_types = None
        self.selected_id_types = None
        self.uid = None

    def parse_list_save_info(self, data_dict):
        """ 
        Gets the information from the user to save the list from 
        the AJAX call.
        
        Sets up the list model to parse the metabolite list 
        to the DB.
        
        Returns an insatnce of the list model to save the 
        mappings with.

        """
        # First check if they wanted to save or delete
        if str(data_dict.get('save')) == 'f':
            delete_tmp_file(self) # remove the file
            return "Mappings not saved."
    
        list_model = MetaboliteListModel()
        list_model.name = str(data_dict.get('filename'))
        list_model.private = str(data_dict.get('access_options'))
        list_model.owner_type = str(data_dict.get('owner_options'))
        list_model.notes = str(data_dict.get('notes'))
        list_model.group_name = str(data_dict.get('group_name'))        

        return list_model


    def parse_user_input(self, data_dict, id_types):
        """
        Takes the POST from the user and saves it to the model.
        """
        self.upload_file = data_dict.get('id-list').file

        """
        Checks which of the checkboxs have been selected and if so
        adds the id_type to the list we want to choose.

        Always add the BiGG ID mapping.
        """
        selected_ids = ['bigg_id']

        for id_type in id_types:
            if data_dict.get(id_type):
                if id_type not in selected_ids:
                    selected_ids.append(id_type)

        self.selected_id_types = selected_ids

        return "success"


    def save_mapping_as_metabolite_list(self, mappings, list_model):
        """
        Lets the user save the mapping as a metabolite list.
        
        Parameters:
                list_name   -> the name of the metabolite list which
                the user has been prompted for when saving.

                If there exists a list already with that name the
                user will be prompted to enter a new list name.
                
                private     -> boolean as to whether or not the list is
                private or public.
                
                owner_id    -> a group or user id of the owner of the metabolite
                list.
            
                owner_type  -> either 'group' or 'user'.
                
        """

        # Perform a preliminary check on the list metadata.
        list_model.path = self.tmp_path;

        good_metadata = list_model.parse_list_metadata()

        if good_metadata != 'success':
            self.error = True
            return good_metadata
        
        # Parse the metadata to the DB.
        # Saves the list id to the list_model.
        parsed_metadata = list_model.process_metabolite_list_metadata()

        if isinstance(parsed_metadata, str):
            self.error = True
            return parsed_metadata

        list_id = parsed_metadata[0]          
        # Connect to DB as we now want to add it
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Add the metabolite list data (i.e. the chebi mapping to the database)
        for metabolite_id in mappings: 
            chebi_id = ''
            for other_id in mappings[metabolite_id]:
                if other_id == 'chebi_id':
                    chebi_id = mappings[metabolite_id][other_id]
            raw_metabolite_id = metabolite_id
            raw_metabolite_id_type =''

            sql = "INSERT into metabolite_list_entry (list_id, chebi_id, raw_metabolite_id) VALUES (%s, %s, %s);"
            cursor.execute(sql,(list_id, chebi_id, raw_metabolite_id),)        

        # Update the path of the file
        list_path_name = 'metabolite/' + list_model.name + "_" + str(list_id)
        new_file_path = os.path.join(PATH, '%s.tsv' % list_path_name)
        os.rename(self.tmp_path, new_file_path)

        # Update the path in the DB
        sql = "UPDATE metabolite_lists SET raw_filepath=%s where id=%s;"
        cursor.execute(sql,(new_file_path, list_id,))

        cursor.close()
        conn.commit()
        conn.close()

        return 'success'    
        
        


    def delete_tmp_file(self):
        os.remove(self.tmp_path)
        


    def save_temp_id_list_as_file(self):
        """
        Saves the list of IDs to map as a tempory file so that the
        user can opt to save the mappings or not.

        The path is saved to the mapping model instance and should the
        user choose not to save the mappings as a metabolite list.
    
        Parameters: 
            input_file: the file uploaded by the user.

        Returns:
            a success or failure message.

        Sets: 
            self.tmp_path: a tempory file path to the users uploaded file.
                    -> if a user is logged in then it is the user_id + time_stamp
                    -> else it is a random code + time_stamp
        """
        time_stamp = datetime.datetime.fromtimestamp(time.time()).strftime('-%Y_%m_%d-%H:%M:%S')
        if (self.uid):
            file_name = str(self.uid) + time_stamp
        else:
            file_name = str(random.randint(1000, 9999)) + time_stamp
    
        
        input_file = self.upload_file

        try:

            file_path = os.path.join(PATH, '%s.tsv' % file_name)

            temp_file_path = file_path + '~'
            input_file.seek(0)

            with open(temp_file_path, 'wb') as output_file:
                shutil.copyfileobj(input_file, output_file)

            os.rename(temp_file_path, file_path)
            self.tmp_path = file_path

            return "success"

        except:
            self.error = True
            return "There was an error parsing the file."


        


    def get_vis_pathway_mapping(self):
        """
        Returns a dictionary of the mappings from all the bigg
        ids to chebi ids in the pathways available.

        Also has the associated information for the metabolites
        e.g. mass, charge etc.

        Perform a double left join:
            1. join on two chebi_mapping_to_other_id where we join on the 
            chebi id and constrain one table to have bigg IDs thus
            getting the bigg mappings

            2. join again on chebi_info to get the charge, mass and definition
            of the metabolite.
 
        """
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        
        sql = "select distinct cm.chebi_id, ci.other_id, cm.other_id, cm.other_id_type, inf.mass, inf.charge, inf.formulae, inf.definition from chebi_mapping_to_other_id as cm left join chebi_mapping_to_other_id as ci on ci.chebi_id=cm.chebi_id left join chebi_info as inf on inf.chebi_id = cm.chebi_id where ci.other_id_type ='bigg_id';"
        cursor.execute(sql,)
        mapped_ids = cursor.fetchall()

        mapping_dict = {}
            
        # Create the mapping dictionary with chebi ids as keys
        for mapping in mapped_ids:
            chebi_id = mapping[0]
            bigg_id = mapping[1]
            other_id_type = mapping[3]
            other_id = mapping[2].strip()
            mass = float(mapping[4])
            charge = float(mapping[5])
            formulae = mapping[6]
            definition = mapping[7]
            
            # Check if we already have an entry for that chebi_id
            try:
                row = mapping_dict[chebi_id]
            except:
                mapping_dict[chebi_id] = {}
                # For each ID in ID types we want to make an array
                for id_type in SUPPORTED_METABOLITE_ID_TYPES:
                    mapping_dict[chebi_id][id_type] = []
            if other_id not in mapping_dict[chebi_id][other_id_type]: 
                # Add the mapping to the dictionary
                mapping_dict[chebi_id][other_id_type].append(other_id)

                # Store the info associated with the metabolite
                info = {'mass': mass, 'charge': charge, 'formulae': formulae, 'definition': definition}
                mapping_dict[chebi_id]['info'] = info

        return mapping_dict


        


    def file_to_id_list(self, input_file):
        """
        Converts a users file to a list of IDs.

        Saves the file to a temporary file and then deletes it
        after the IDs have been read from the file.

        IDs must be in the first collumn. 

        Parameter:
            input_file: user uploads a TSV file of IDs to map

        Returns:
            parsed_id_list: a list of "cleaned" IDs from the file as a list.

            parsed_to_raw_id_dict: a dictionary of the parsed ids to the
                users raw_ids
        """
        # Saved the IDs as lowered and cleaned
        parsed_id_list = []
        # Map back to the original IDs
        parsed_to_raw_id_dict = {}

        with open(input_file, 'r') as filein:
            filein = csv.reader(filein, delimiter='\t')
            # For each row we just want to save the metabolite ID
            # which is stored in the first column
            for row in filein:
                raw_id = row[0]
                parsed_id = self.clean_name(raw_id)
                if parsed_id not in parsed_id_list:
                    parsed_id_list.append(parsed_id)
                    parsed_to_raw_id_dict[parsed_id] = raw_id

        return {'parsed_id_list': parsed_id_list, 'parsed_to_raw_id_dict': parsed_to_raw_id_dict}





    def get_stats_on_mapping(self, parsed_to_raw_metabolite_dict, mapping_dict, id_types):
        """

        We not only want to return to the user the mappings
        but some stats on which IDs didn't map and how many
        that were mapped had pathways i.e. how many will they
        actually be able to use in Omicxview.
        
        Parameters:
 
            parsed_to_raw_metabolite_dict: a dictionary from parsed metabolite
                IDs to the raw metabolite ID (for example, the id is lowered so 
                user inputs HMDB... converts to hmdb...).
            
            id_types: has a list of ID_TYPES that a users IDs were mapped to

            mapped_metabolite_ids: a dictionary with users parsed metabolite
                IDs an entry containing the other ids that it maps
                to from that identifier through the neutral ChEBI ID.
            
        """
        # Make a dictionary to keep track of the counts of each mapping
        # for the id types
        mapped_id_stats = {}

        # add general mapping stats to the dictionary
        mapped_id_stats['Number of unique ids in file'] = len(parsed_to_raw_metabolite_dict)
        mapped_id_stats['Number of mapped ids'] = len(mapping_dict)
        mapped_id_stats['Number of unmapped ids'] = len(parsed_to_raw_metabolite_dict) - len(mapping_dict) 
       
        
        for id_type in id_types:
            mapped_id_stats[id_type] = 0


        """
         Itterate through the ID mappings.
        
           1. Store IDs which were not able to be mapped.
           2. Keep a count of each type of ID that was mapped.
           3. Save to a new mapping dictionary with the key as the
               users raw_metabolite_id (rather than the parsed one
               used during the mapping process).
        """
        mapping_dict_with_raw_ids = {}
        unmapped_ids = []

        for parsed_id in parsed_to_raw_metabolite_dict:
            try:
                mapped_ids = mapping_dict[parsed_id]
                for other_id in id_types:#mapped_ids:
                    try: 
                        other_ids = mapped_ids[other_id]
                    except:
                        other_ids = ""
                    if len(other_ids) > 0:
                        mapped_id_stats[other_id] += 1
                mapping_dict_with_raw_ids[parsed_to_raw_metabolite_dict[parsed_id]] = mapped_ids
            except:
                mapping_dict_with_raw_ids[parsed_to_raw_metabolite_dict[parsed_id]] = ""
                unmapped_ids.append(parsed_to_raw_metabolite_dict[parsed_id])
  
        return {'mapping_dict_with_raw_ids': mapping_dict_with_raw_ids, 'mapped_id_stats': mapped_id_stats, 'unmapped_ids': unmapped_ids}




    def get_id_mapping(self, raw_metabolite_ids, list_of_ids_to_map_to):

        """
        Performs the mapping from a raw identifier from an uploaded file
        to those used in Omicxview.
        
        
        Parameters:
            raw_metabolie_ids: a list of raw metabolite IDs from a TSV
                    file that a user has uploaded

            list_of_ids_to_map_to: a list of ID_TYPES that we will output in
                    the front end -> initially static and pre defined but later
                    can have the user choose a range.
                    ['chebi_id', 'name', 'hmdb', 'inchi_str', 'bigg_id', 'list_of_pathways']

        
        Returns:
            mapped_metabolite_ids:
                1. raw_metabolite_id
                    and for each ID_TYPE in list_of_ids_to_map_to.

        """
        
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        
        # Convert the lists to tuples
        raw_metabolite_ids = tuple(raw_metabolite_ids)
        id_types = tuple(list_of_ids_to_map_to)        


        # Select the mappings from the database based on the IDs input and
        # the other ID types we are looking to map to.
        sql = "SELECT a.chebi_id, a.other_id, b.other_id_type, b.other_id from chebi_mapping_to_other_id as a LEFT JOIN chebi_mapping_to_other_id as b on (a.chebi_id = b.chebi_id) WHERE a.other_id in %s AND b.other_id_type in %s;"
        cursor.execute(sql,((raw_metabolite_ids),(id_types),),)
        mapped_ids = cursor.fetchall()

        #return mapped_ids
        
        # Make a dictionary which we will return to the user with the 
        # list of ID_Types so we can make a table based on these
        # and an entry with the mappings for each type.
        mapping_dict = {}
        
        for mapping in mapped_ids:
            chebi_id = mapping[0]
            users_id = mapping[1]
            other_id_type = mapping[2]
            other_id = mapping[3]

            # Check if we already have an entry for that chebi_id
            try:
                row = mapping_dict[users_id]
            except:
                mapping_dict[users_id] = {}
                mapping_dict[users_id]['chebi_id'] = []
                # For each ID in ID types we want to make an array
                for id_type in list_of_ids_to_map_to:
                    mapping_dict[users_id][id_type] = []
            
            # Add the mapping to the dictionary
            if other_id not in mapping_dict[users_id][other_id_type]:
                mapping_dict[users_id][other_id_type].append(other_id)

            # Add the chebi mapping to the dictionary
            if chebi_id not in mapping_dict[users_id]['chebi_id']:
                mapping_dict[users_id]['chebi_id'].append(chebi_id)
        
        return mapping_dict




    def assign_neutral_chebi_id(self, ds_id, raw_metabolite_id_type, raw_metabolite_ids, raw_metabolite_values): 
        id_errors = []
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        ids = tuple(list(raw_metabolite_ids.keys()))
        sql = "SELECT chebi_id, other_id from chebi_mapping_to_other_id WHERE other_id in %s;"
        cursor.execute(sql, (ids,))
        neutral_chebi_ids = cursor.fetchall()

        mapped_raw_metabolite_ids = []

        # for each of these assigned chebis we want to add them (along with the raw metabolite ids) to the database
        for value in neutral_chebi_ids:
            # The parsed metabolite id is in value[1] and the chebi_id in value[0]
            raw_metabolite_id = raw_metabolite_ids[value[1]]
            mapped_raw_metabolite_ids.append(value[1])
            chebi_id = value[0]
            samples = raw_metabolite_values[raw_metabolite_id]
            # Insert that mapping into the dataset mapping table
            #sql = "INSERT into dataset_chebi_mapping (ds_id, chebi_id, raw_metabolite_id, raw_metabolite_type) VALUES (%s, %s, %s, %s);"
            #cursor.execute(sql,(ds_id, chebi_id, raw_metabolite_id, raw_metabolite_id_type,))
            # retrieve the id from that mapping
            #sql = "SELECT id FROM dataset_chebi_mapping WHERE ds_id = %s AND chebi_id = %s AND raw_metabolite_id = %s AND raw_metabolite_type = %s;"
            #cursor.execute(sql,(ds_id, chebi_id, raw_metabolite_id, raw_metabolite_id_type,))
            # ---------------------------------------------------------------------
            # Maybe put a check in here for the metabolite mapping?
            # ---------------------------------------------------------------------
            #mapping_id = cursor.fetchall()[0][0]

            for sample in samples:
                value = raw_metabolite_values[raw_metabolite_id][sample]
                sql = "INSERT into data (ds_id, chebi_id, sample, value, raw_metabolite_id) VALUES (%s, %s, %s, %s, %s);"
                cursor.execute(sql,(ds_id, chebi_id, sample, value, None,))

        # Want to return to the user any ids which weren't able to be mapped
        for parsed_metabolite_id in raw_metabolite_ids:
            if not parsed_metabolite_id in mapped_raw_metabolite_ids:
                id_errors.append(raw_metabolite_ids[parsed_metabolite_id])

        cursor.close()
        conn.commit()
        conn.close()
    
        return id_errors


    # Gets the identification information which aids the user in deciding which
    # chebi id they want to assign to a particular metabolite
    def get_mapping_metabolite_list_info(self, metabolite_list_id, uid):
        if not isinstance(metabolite_list_id, int):
            self.error = True
            return "Metbaolite list id wasn't an integer."
    
        list_model = MetaboliteListModel()

        metabolite_list = list_model.get_processed_metabolite_list(metabolite_list_id, uid)
        
        # For each of the data elements we want to get the chebi_id based on the raw_metabolite
        # id that the user has uploaded
        list_data = metabolite_list['data']
        
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        list_with_chebi_and_bigg_assigned = {}
        list_data_matched = {}
        # Keep track of list of biggs
        bigg_id_ref = []
    
        count = 0

        raw_metabolite_id_tag = 'raw metabolite id'

        # Want to match on the other ID that the user has assigned and for these, find 
        # the matching bigg_id for that chebi id
        # 1. Find matching chebi id for the raw metabolite ID, match this up with a bigg ID 
        # corrsponding to that chebi id
        for i in list_data:
            raw_metabolite_id = self.clean_name(list_data[i][raw_metabolite_id_tag])
            # Get a match between chebi and bigg ids for this metabolite
            sql = "select distinct cm.chebi_id, ci.other_id from chebi_mapping_to_other_id as cm left join chebi_mapping_to_other_id as ci on ci.chebi_id=cm.chebi_id where cm.other_id=%s and ci.other_id_type ='bigg_id';"
            cursor.execute(sql, (raw_metabolite_id,))
            bigg_chebi_name_matches = cursor.fetchall()
            list_data_matched[raw_metabolite_id] = []

            # Store a mapping between chebi and biggs so we can easily access that bigg id for 
            # the chebi ID. We want to do this so we can group all the chebis together and
            # perform one check to get the information (rather than performing sql statements
            # separately)
            chebi_bigg_dict = {}
            chebi_list = []
            chebi_info_list = []
            # Keep a list of chebi ids so we don't have to do a try catch
            chebi_id_ref = []        
            # Get the relevant information for the user for each of the chebi ID's so that
            # can make an accurate choice
            for match in bigg_chebi_name_matches:
                chebi_id = match[0]
                bigg_id = match[1]
                chebi_list.append(chebi_id)
                if chebi_id in chebi_id_ref:
                    bigg_list = chebi_bigg_dict[chebi_id]
                else:
                    bigg_list = []
                    chebi_id_ref.append(chebi_id)

                bigg_list.append(bigg_id)
                chebi_bigg_dict[chebi_id] = bigg_list

            if len(chebi_list) > 0:
                count += 1
                # Now we have a list of all the chebi's we are looking for, performa sql to get these
                sql = "select distinct chebi_id, name, mass, charge, formulae, definition, last_updated_date from chebi_info where chebi_id in %s;"
                cursor.execute(sql, (tuple(chebi_list),))
                chebi_info_list = cursor.fetchall()
            else:
                print(list_data[i][raw_metabolite_id_tag])    
            # Need to separate this out now for each of the chebi and biggs
            for chebi_info in chebi_info_list:
                chebi_id = chebi_info[0]
                bigg_ids = chebi_bigg_dict[chebi_id]
                # We want to assign it to a bigg ID as this is how we map it on the front end
                # so we group the chebis to a bigg id
                for bigg_id in bigg_ids:
                    #print(bigg_id,',', chebi_id,',', chebi_info[1], ',', str(chebi_info[2]), ',',str(chebi_info[3]))
                    #try:
                    #    bigg_tmp = list_with_chebi_and_bigg_assigned[bigg_id]
                    #except:
                    #    bigg_tmp = {}
                    if bigg_id in bigg_id_ref:
                        bigg_tmp = list_with_chebi_and_bigg_assigned[bigg_id]
                    else:
                        bigg_tmp = {}
                        bigg_id_ref.append(bigg_id)
                    tmp = {}
                    tmp['chebi_id'] = chebi_id
                    tmp['name'] = chebi_info[1]
                    tmp['mass'] = str(chebi_info[2])
                    tmp['charge'] = str(chebi_info[3])
                    tmp['formulae'] = chebi_info[4]
                    tmp['definition'] = chebi_info[5] 
                    tmp['last_updated'] = str(chebi_info[6])
                    bigg_tmp[chebi_id] = tmp
                    list_with_chebi_and_bigg_assigned[bigg_id] = bigg_tmp

                    # Add the id match to the list data so that we can know where the data
                    # will map to
                    bigg_chebi_match = {'bigg_id': bigg_id, 'chebi_id': chebi_id}
                    list_data_matched[raw_metabolite_id].append(bigg_chebi_match)

        cursor.close()
        conn.close()
        
        return {'metadata': metabolite_list['metadata'], 'data': list_data_matched, 'matched_id': list_with_chebi_and_bigg_assigned}

    # Helper function which cleans names and makes it easier to match on names
    def clean_name(self, name):
        name = re.sub(r'[\W_]+', '', name.lower().replace(" ", ""))
        return name        

    # If the dataset isn't verified (this check is done in the vis controller)
    # then the get mapping data function is called
    def get_mapping_data(self, ds_id, data):
        data_info = {}

#        chebi_id = data[ID]
#        data_info.data = data
#        data_info.info = get_info_for_id(chebi_id)
#        data_info.bigg_id = get_bigg_for_chebi(chebi_id)

#        if data.verified == false:
#            data_info.other_ids = get_other_ids_for_chebi(chebi_id)
#        else:
#            data_info.other_ids = "verified"

#        return data_info
                

    # Gets the stored information for a chebi ID
    # e.g. mass, charge, definition etc
    def get_info_for_id(self, chebi_id):
        return 0
#        info = select * from chebi_info where chebi_id = chebi_id


    # Gets the other ID's that match for the chebi id
    def get_other_ids_for_chebi(self, chebi_id):
        return 0
        #other_ids = select * from chebi_other_id where chebi_id = chebi_id


    # Gets the BiGG ID that matches the chebi id
    # This is for an unverified mapping so the bigg_id may be returned as a list, the
    # user will then need to select the bigg id which matches and this will be stored for
    # the dataset
    def get_bigg_for_unverified_chebi(self, chebi_id):
        return 0
        #bigg_id = select other_id from chebi_other_id where chebi_id = chebi_id and other_id_type = 'bigg'


    # Makes the preliminary chebi to raw match when a user uploads a dataset
    def assign_default_chebi(self, raw_metabolite_id, raw_metabolite_id_type, ds_id):
        # check that the raw_metabolite is a string and ds_id is an int
        # check the raw_metabolite_id_type is allowed
        if not isinstance(raw_metabolite_id, str) or not isinstance(ds_id, int) or raw_metabolite_id_type not in SUPPORTED_METABOLITE_ID_TYPES:
            self.error = True
            return "Error in assigning the default chebi"

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) 

        if raw_metabolite_id_type == 'chebi_id':
            chebi_id = self.parse_users_chebi(raw_metabolite_id)
            if not chebi_id:
                self.error = True
                return "Chebi id bad format."
            # Check if we have an entry for this chebi ID otherwise return an error
            sql = "SELECT * FROM chebi_info where chebi_id=%s;"
            cursor.execute(sql,(chebi_id,))
            chebi_info = cursor.fetchall()
            # This may be beacuse it is an outdated chebi and they will have to update
            # their ID
            if not chebi_info:
                self.error = True
                return "Chebi id not in database couldn't store."
        else:
            sql = "SELECT chebi_id FROM chebi_mapping_to_other_id WHERE other_id=%s and other_id_type=%s;"
            cursor.execute(sql,(raw_metabolite_id,raw_metabolite_id_type,))
            chebi_matches = cursor.fetchall()
            # Means we have no matching ID's in the database and won't be able to match
            if not chebi_matches:
                self.error = True
                return "No match for your ID in the database"
            chebi_list = []
            for chebi_match in chebi_matches:
                print(chebi_match[0])
                chebi_list.append(chebi_match[0])
            sql = "SELECT * FROM chebi_info WHERE chebi_id IN %s ORDER BY last_updated_date;"
            cursor.execute(sql,(tuple(chebi_list),))
            chebis = cursor.fetchall()
            # Pick the most recent
            chebi_id = chebis[0]

        # Means we have the chebi_id for this metabolite stored add it to the dataset_chebi_mapping table
        sql = "INSERT INTO dataset_chebi_mapping (ds_id, chebi_id, raw_metabolite_id, raw_metabolite_id_type) VALUES(%s, %s, %s, %s);"
        cursor.execute(sql,(ds_id, chebi_id, raw_metabolite_id, raw_metabolite_id_type,))
        cursor.close()
        conn.commit()
        conn.close()
        
        # Return the chebi id so it can be stored in the data table
        return chebi_id


    # Assigns the neutral chebi to the users list of raw identifiers
    def assign_neutral_chebis(raw_metabolite_ids, raw_metabolite_id_type):
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        
        sql = "SELECT chebi_id, other_id from chebi_mapping_to_other_id WHERE other_id_type = %s AND other_id in %s;"
        cursor.execute(sql,(raw_metabolite_id_type,(tuple(raw_metabolite_ids)),))
        cursor.close()
        conn.close()


    # Parse the chebi id
    def parse_users_chebi(self, raw_chebi):
        try:
            # Check if they have the CHEBI: token at the front if so strip
            chebi_id = raw_metabolite_id_type.strip()
            if chebi_id[0] == 'C':
                chebi_id = chebi_id.split(':')[1]
                # Need to do a database lookup to see 
            return chebi_id 
        except:
            return None
        
    # Choose the chebi ID which was most recently updated
    def choose_most_recent_chebi(self, chebi_matches):
        
        #chebi_info = select * from chebi_info where chebi_id in chebi_matches order by date
        return chebi_matches['chebi_id']

"""
    # Parse the list of chebi ID's that the user wants to update
    # This will be JSON 
    def update_users_mapping_choices(mapping_data):
        updated_chebis = mapping_data.updated_chebis
        ds_id = mapping_data.ds_id

        for (chebi in updated_chebis):
            update_chebi_id(ds_id, old_chebi_id, new_chebi_id, bigg_id, user_id)

        return success 

    # Save the users's choice for a new chebi id
    def update_chebi_id(ds_id, old_chebi_id, new_chebi_id, bigg_id, user_id):
        update ds_chebi_match set chebi_id = new_chebi_id, bigg_id = bigg_id, verified = True, verified_by_id = user_id where chebi_id = old_chebi_id and ds_id = ds_id

        return success


    # Gets the bigg id for a mapping which has been verified.
    def get_bigg_for_verified_chebi(ds_id, chebi_id):
        bigg_id = select bigg_id from dataset_chebi_mapping where ds_id = ds_id and chebi_id = chebi_id
        return bigg_id
"""
