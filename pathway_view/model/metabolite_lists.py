import os
import uuid
import shutil
import datetime

from pathway_view.model.base import BaseModel
from pathway_view.config import *
from pathway_view.model.auth import get_group_by_name
from pathway_view.model.auth import AuthModel
import csv


class MetaboliteListModel(BaseModel):
    
    def __init__(self):
        self.path_id = None
        self.name = None
        self.error = None
        self.owner_id = None
        self.list_id = None
        self.raw_metabolite_id = None
        self.internal_call = False



    def parse_list_metadata(self):
        # Type:
        #       Matabolite or Pathway
        # Normal checks e.g. for Metabolite: 
        #   Has the user selected a collumn for:
        #       Metabolite Name
        #       ChEBI ID
        # Is there a file uploaded and they've supplied a name

        if self.name != None:
            if not isinstance(self.name, str) or not NAME_REGREX.match(self.name):
                self.error = True
                return "There was an error with uploaded data."

        if self.notes != None:
            if not isinstance(self.notes, str):
                self.error = True
                return "There was an error with uploaded data."

        # Determine whether it is a pathway for a group or a user
        if self.owner_type == 'group':
            # From the authmodel
            group = get_group_by_name(self.group_name)
            if group == error_codes['name']:
                self.error = True
                return "Error with the uploaded group name"
            # set the owner_id to be the group id
            self.owner_id = group[ID]

        # Check that collumns have been selected


        return "success"


    def save_raw_list(self):
        # Saves the raw file that the user has uploaded
        # Saves as a tmp file and will be renamed once the list has been parsed
        # to /uploads/lists/type/name_listID.tsv
        filename = self.name
        input_file = self.upload_file

        path = PATH + "lists/"
        # Check the user has provided a name and a file
        if not isinstance(filename, str):
            self.error = True
            return "There was an issue with the file or filename you uploaded, please try again."

        # File may throw an error so surround with try catch
        try:
            file_path = os.path.join(path, '%s.tsv' % filename)

            temp_file_path = file_path + '~'
            input_file.seek(0)

            with open(temp_file_path, 'wb') as output_file:
                shutil.copyfileobj(input_file, output_file)

            os.rename(temp_file_path, file_path)

            # update the path so that we can have access to the file
            self.path = file_path
            return "success"

        except:
            self.error = True
            return "There was an error parsing the file."


    def process_metabolite_list_metadata(self):
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Test if there already exists a list with the same name
        sql = "SELECT * from metabolite_lists where name = %s;"
        cursor.execute(sql,(self.name,))
        list_metadata = cursor.fetchall()

        if list_metadata:
            self.error = True
            # Need to delete the list that was added 
            return "There already exists a list with that name, please choose a new name."

        sql = "INSERT into metabolite_lists (name, owner_id, owner_type, raw_filepath, notes, private) VALUES (%s, %s, %s, %s, %s, %s);"

        cursor.execute(sql,(self.name, self.owner_id, self.owner_type, self.path, self.notes, self.private,))

        sql = "SELECT * from metabolite_lists where name = %s and owner_id = %s and owner_type = %s;"

        cursor.execute(sql,(self.name, self.owner_id, self.owner_type,))
        list_metadata = cursor.fetchall()

        # check it has been added 
        # add the ID to the list
        if len(list_metadata) != 1:
            self.error = True
            # remove the file
            os.remove(self.path)
            return "Wasn't able to add metadata."

        self.list_id = int(list_metadata[0][ID])

        # Make the name and id the list name
        path = PATH + "lists/"

        # Make the name and id the list name 
        list_path_name = self.name + "_" + str(self.list_id)

        # if the dataet was able to be added completely
        new_file_path = os.path.join(path, '%s.tsv' % list_path_name)
        os.rename(self.path, new_file_path)

        # Need to update the filepath in the database
        sql = "UPDATE metabolite_lists SET raw_filepath=%s where id=%s;"
        cursor.execute(sql,(new_file_path, self.list_id,))

        cursor.close()
        conn.commit()
        conn.close()

        self.path = new_file_path
        return list_metadata[0]

        
    def process_raw_metabolite_list_data(self):
        # Save list in the database
        # 1. In metabolite_list
        # 
        # 2. For each row:
        #           An entry in: metabolite_list_entry
        filepath = self.path
        list_id = int(self.list_id)

        # connect to db
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Contains the indicies of the collumns to store
        col_dict = self.col_dict
        count = 0
        
        # Keep track of any metabolites in the lists which weren't able to be mapped
        id_errors = []
        try:
            with open(filepath, 'r') as filein:
                filein = csv.reader(filein, delimiter='\t')

                # Read each row
                for row in filein:
                    count += 1
                    # If it is the first row we want to store the samples
                    if count == 1:
                        num_cols = len(row) 
                        """ 
                        Have a restriction that a user must have at minimum one
                        column of list data i.e. at minimum pvalue, 
                        so they need to columns min (one for the 
                        metabolite name and one for the data.

                        Return that error to the user and delete the list.
                        """
                        if (num_cols < 2):
                            self.error = True
                            self.internal_call = True
                            self.delete_metabolite_list(list_id, self.owner_id)
                            return "Please check that you're file is in .TSV format. Also files must have at minimum 2 columns, one for the metabolite ID and one for a statistical measurement."
                
                    else:
                        # Stores the values after they have been converted to real numbers (or none)
                        row_dict = self.get_empty_row_dict() 
                        # Try to parse each of the collumns in the collumn dict
                        for header in col_dict:
                            if col_dict[header] != None:
                                # We need to add this to the data table
                                value = row[col_dict[header]]
                                # comments and name don't need to be converted to numeric values
                                if (METABOLITE_LIST_HEADER_TYPES[header] == VARCHAR):
                                    val_processed = value
                                else:
                                    # Do the first check to see if the value is numeric
                                    try:
                                        val_processed = float(value)
                                    except:
                                        # Store that they had a value which wasn't able to be converted to a float
                                        val_processed = None
                                # update the list row dict
                                row_dict[header] = val_processed

                        # Assigning the neutral chebi id to the list
                        # -----------------------------------------------------------------
                        # To do, some form of confirmation that this is correct
                        # i.e. validation by user
                        # ----------------------------------------------------------------- 
                        sql = "SELECT chebi_id from chebi_mapping_to_other_id WHERE other_id = %s;"
                        cursor.execute(sql, (row_dict['raw_metabolite_id'].lower(),))
                        chebi_id = cursor.fetchall()
                        if len(chebi_id) < 1:
                            id_errors.append(row_dict['raw_metabolite_id'])
                        else:
                            chebi_id = chebi_id[0][0]
                            sql = "INSERT into metabolite_list_entry (list_id, chebi_id, raw_metabolite_id, name, raw_metabolite_id_type, p_value, t_statistic, z_score, bh_adjusted_p_value, fold_change, standard_error, comments) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"

                            cursor.execute(sql,(list_id, chebi_id, row_dict['raw_metabolite_id'], row_dict['name'], self.raw_metabolite_id_type, row_dict['p_value'], row_dict['t_statistic'], row_dict['z_score'], row_dict['bh_adjusted_p_value'], row_dict['fold_change'], row_dict['standard_error'], row_dict['comments'],))
        except Exception as e:
            self.error = True
            self.internal_call = True
            self.delete_metabolite_list(list_id, self.owner_id)
            return "File wasn't able to be opened, files must be of .TSV format."


        # Maybe do a check to test the length of the data added is the same
        # as the size of the list


        # Make the name and id the list name
        path = PATH + "lists/" 

        # Make the name and id the list name 
        list_path_name = self.name + "_" + str(list_id)

        # if the dataet was able to be added completely
        new_file_path = os.path.join(path, '%s.tsv' % list_path_name)
        os.rename(self.path, new_file_path)

        # Need to update the filepath in the database
        sql = "UPDATE metabolite_lists SET raw_filepath=%s where id=%s;"
        cursor.execute(sql,(new_file_path, list_id,))

        cursor.close()
        conn.commit()
        conn.close()

        self.path = new_file_path
        return id_errors



    # Helper function used above: makes an empty row with None values to get filled
    # as we process each row of the list file
    def get_empty_row_dict(self):
        empty_row_dict = {}
        # METABOLITE_LIST_HEADER_MAP is stored in config.py
        for header in METABOLITE_LIST_HEADER_MAP:
            empty_row_dict[header] = None
        return empty_row_dict;



    def get_processed_metabolite_list(self, list_id, uid):
        # Returns a list the user has chosen to display on a pathway
        # Check ds_id an int
        if not isinstance(list_id, int) or list_id < 0:
            self.error = True
            return error_codes['id']
    
        # Check the user has access to the list, uid is also checked here for validity
        if not list_id in self.get_all_processed_metabolite_lists_for_user(uid):
            self.error = True
            return "User doesn't have access to the list."

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "SELECT * from metabolite_lists WHERE id = %s;"

        cursor.execute(sql,(list_id,))
        list_metadata = cursor.fetchall()

        # Now we need the data
        sql = "SELECT * from metabolite_list_entry WHERE list_id = %s;"

        cursor.execute(sql,(list_id,))
        list_data = cursor.fetchall()

        cursor.close()
        conn.close()

        if (len(list_metadata) != 1):
            self.error = True
            return "Couldn't retrieve list."

        # Need to process it to keep it modular (only have the one place where it is processed)
        # Metabolite list storage values need to be gotton from the config file (where they are stored)
        data_count = 0
        processed_list_data = {}
        for data in list_data:
            chebi_id = data[METABOLITE_LIST_HEADER_MAP['chebi_id']] # Get position where chebi id is stored
            info = {} 
      
            for header in METABOLITE_LIST_HEADER_MAP:
                # Make the header nicer to read so we replace underscores with spaces
                # faster to do in python than JS
                header_space = header.replace("_", " ")

                # We need to determine whether or not we need to parse it to a float, it will come
                # out of the DB with a Decimal('') tag and won't be able to be passed to JSON if 
                # we don't.
                if (METABOLITE_LIST_HEADER_TYPES[header] == VARCHAR):
                    info[header_space] = data[METABOLITE_LIST_HEADER_MAP[header]]

                # Only parse it to a float if it is not a none type
                elif data[METABOLITE_LIST_HEADER_MAP[header]]:
                    info[header_space] = float(data[METABOLITE_LIST_HEADER_MAP[header]])

                # Otherwise assign it a none value
                else:
                    info[header_space] = "null"
            # Add it to that chebi id in the processed list        
            processed_list_data[chebi_id] = info
            data_count += 1

    
        # Process the metadata
        processed_metadata = {}
    
        for header in METABOLITE_LIST_METADATA_MAP:
            processed_metadata[header] = list_metadata[0][METABOLITE_LIST_METADATA_MAP[header]]

        
        return {'metadata': processed_metadata, 'data': processed_list_data}
    

    def get_all_processed_metabolite_lists_for_user(self, uid):
        # Gets all the lists for a user
        # low level check
        if not isinstance(uid, int) or uid < 0:
            return error_codes['id']

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "select l.id, name, owner_id, owner_type, notes, private from group_users as gu left join metabolite_lists as l on (gu.group_id = l.owner_id and l.owner_type = 'group' and gu.user_id = %s) order by owner_type;"
        cursor.execute(sql,(uid,))

        # We want all the lists that are available just to the user
        # and all the lists which are available to the user within the group

        list_groups = cursor.fetchall()

        sql = "select id, name, owner_id, owner_type, notes, private from metabolite_lists where (owner_id = %s and owner_type ='user') or private = 'f';"
        cursor.execute(sql,(uid,))

        list_users = cursor.fetchall()

        cursor.close()
        conn.commit()
        conn.close()
    
        # Parse the lists group and user information and return a combined dict
        return self.parse_db_user_group_lists(list_groups, list_users)


    def parse_db_user_group_lists(self, list_groups, list_users):
        # want to parse the lists to a dictionary so we can pass it easily
        # to the templates

        user_list_dict = {}
        for list_info in list_groups:
            if list_info[0] != None: # means we have an ID
                # Make a new list dict
                list_id = list_info[0]
                user_list_dict[list_id] = {}
                # The headers and relative database positions are stored in the 
                # METABOLITE_LIST_METADATA_MAP so that if these change only have to 
                # update the config.py file
                for header in METABOLITE_LIST_METADATA_MAP:
                    list_value = list_info[METABOLITE_LIST_METADATA_MAP[header]]
                    if header == PRIVATE:
                        if (list_value == 't'):
                            # PRIVATE (or PUBLIC) are just the string represenation we want to pass
                            # the user when a list is private (in config.py)
                            list_value = PRIVATE
                        else:
                            list_value = PUBLIC

                    user_list_dict[list_id][header] = list_value

        for list_info in list_users:
            if list_info[0] != None:
                list_id = list_info[0]
                user_list_dict[list_id] = {}
                for header in METABOLITE_LIST_METADATA_MAP:
                    list_value = list_info[METABOLITE_LIST_METADATA_MAP[header]]
                    if header == PRIVATE:
                        if (list_value == 't'):
                            list_value = PRIVATE
                        else:
                            list_value = PUBLIC

                    user_list_dict[list_id][header] = list_value


        
        return user_list_dict

    

    def delete_metabolite_list(self, list_id, uid):
        # Low level check
        if not isinstance(list_id, int) or list_id < 0:
            self.error = True
            return error_codes['id']

        # First check the user has access
        if not list_id in self.get_all_processed_metabolite_lists_for_user(uid):
            self.error = True
            return "User doesn't have access to the list."

        auth = AuthModel()
        # first we want to delete all the data associated with the list
        if auth.get_user_role_by_uid(uid) != 'admin' and self.internal_call != True:
            self.error = True
            self.internal_call = False
            return "Only admins can delete datasets"

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Get the list so we can delete it
        sql = "SELECT raw_filepath from metabolite_lists WHERE id = %s;"

        cursor.execute(sql,(list_id,))
        list_metadata = cursor.fetchall()

        # Get the raw file path
        list_path_name = list_metadata[0][0]

        # Set any datasets with that default list ID to be NULL
        sql = "UPDATE DATASETS SET default_list_id=NULL WHERE default_list_id=%s;"
        cursor.execute(sql,(list_id,))

        # deleting all the columns from the data table which contain the values
        sql = "DELETE FROM metabolite_list_entry WHERE list_id = %s;"
        cursor.execute(sql,(list_id,))

        # deleting the metadata
        sql = "DELETE FROM metabolite_lists WHERE id = %s;"
        cursor.execute(sql,(list_id,))

        # Test it has been deleted
        sql = "SELECT * from metabolite_lists WHERE id = %s;"
        cursor.execute(sql,(list_id,))
        list_metadata = cursor.fetchall()

        cursor.close()
        conn.commit()
        conn.close()

        # Delete the list from the server
        os.remove(list_path_name)

        # Check it was removed
        if list_metadata:
            self.error = True
            return error_codes['id']

        return "success"

