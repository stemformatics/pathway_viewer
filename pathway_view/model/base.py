from pathway_view.config import *

class BaseModel:


    def to_dict(self):
        return self.__dict__


    def perform_db_query(self, sql, data, commit, fetch_data):
        """
        A generic function for performing SQL statements.

        Takes a SQL string with named parameters and a dictionary
        with values associated with each of these parameters.

        commit is a bool, which is whether the database needs to
        be committed after the SQL is performed (e.g. if it is an
        insert or update)

        Example: 
            INSERT INTO some_table (an_int, a_date, another_date, a_string)
            VALUES (%(int)s, %(date)s, %(date)s, %(str)s);,
            {'int': 10, 'str': "O'Reilly", 'date': datetime.date(2005, 11, 18)})
    
        http://initd.org/psycopg/docs/usage.html
        """
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cursor.execute(sql,(data))
        
        fetched_data = None

        # True for selects.
        if fetch_data:
            fetched_data = cursor.fetchall()

        cursor.close()

        # True for inserts, deletes, updates.
        if commit:
            conn.commit()
            
        conn.close()

        
        return fetched_data
