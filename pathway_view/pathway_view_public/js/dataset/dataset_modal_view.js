

/**
 * When the user clicks the view dataset button 
 * a modal div pops up with the dataset information
 */ 
view_dataset = function(ds_id) {
    // get the datasets
    var json_data = document.getElementById("datadiv").innerHTML;
    var data = JSON.parse(json_data)[ds_id];
    // Set the dataset properties of the modal div
    document.getElementById("view_ds_id").innerHTML = ds_id;
    document.getElementById("view_ds_title").innerHTML = data['name'];
    document.getElementById("view_ds_private").innerHTML = data['private'];    
    document.getElementById("view_ds_access").innerHTML = data['owner_type'];
    document.getElementById("view_ds_notes").innerHTML = data['notes'];
}
