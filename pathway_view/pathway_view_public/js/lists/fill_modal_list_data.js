
view_element_info = function(element_id) {
    var json_data = document.getElementById("datadiv").innerHTML;
    var data = JSON.parse(json_data)[element_id];
    document.getElementById("view_id").innerHTML = element_id;
    document.getElementById("view_title").innerHTML = data['name'];
    document.getElementById("view_private").innerHTML = data['private'];
    document.getElementById("view_access").innerHTML = data['owner_type'];
    document.getElementById("view_validation").innerHTML = data['validated'];
    document.getElementById("view_notes").innerHTML = data['notes'];
}

