/**
 * The scroll for the main image on the landing page
 */
$(window).scroll(function(){
    $(".imagescroll").css("opacity", 1 - $(window).scrollTop() / 500);
  });

/**
 * Sticks the side bar.
 
$(function() {

    var $sidebar   = $("#sidebar"), 
        $window    = $(window),
        offset     = $sidebar.offset(),
        topPadding = 15;

    $window.scroll(function() {
        if ($window.scrollTop() > offset.top) {
            $sidebar.stop().animate({
                marginTop: $window.scrollTop() - offset.top + topPadding
            });
        } else {
            $sidebar.stop().animate({
                marginTop: 0
            });
        }
    });
    
});
*/
/**
 * Scroll for the navbar
 */
$(window).scroll(function() { 
    if ($(this).scrollTop()> 100) { 
        $('.navscroll').fadeIn(); 
    } else { 
        $('.navscroll').fadeOut(); 
    } 
});
