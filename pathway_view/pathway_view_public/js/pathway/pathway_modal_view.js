/**
 * When the user clicks the view dataset button 
 * a modal div pops up with the dataset information
 */
view_pathway = function(path_id) {
    // get the datasets
    var json_data = document.getElementById("datadiv").innerHTML;
    var data = JSON.parse(json_data)[path_id];
    // Set the dataset properties of the modal div
    document.getElementById("view_path_id").innerHTML = path_id;
    document.getElementById("view_path_title").innerHTML = data['name'];
    document.getElementById("view_path_private").innerHTML = data['private'];
    document.getElementById("view_path_access").innerHTML = data['owner_type'];
    document.getElementById("view_path_validation").innerHTML = data['validated'];
    document.getElementById("view_path_notes").innerHTML = data['notes'];
} 
