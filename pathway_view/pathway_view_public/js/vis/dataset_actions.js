var pathway_action_toggle_pathway_text = function() {
    toggle_class_opacity(".nodetext");
}


/**
 * Creates a datatable out of the stats.
 * 
 * Updates when a pathway is updated or when a dataset is updated.
 * 
 * TODO update when a metabolite list is updated.
 */
var update_stats_table = function () {

    // Empty old stats
    document.getElementById('stats-table').innerHTML = "";
    var stats = pathway.data.stats; 
    var node_count = pathway.data.pathway.node_count || 0;
    var data_count = pathway.data.dataset.metabolite_count || 0;

    var html = "<table class='display table table-striped' id='table-stats'> <thead> " +   
                "<tr> <th> Name </th> <th> Value </th> <th> % </th> </tr> </thead>";

    var stats_front_end = {
             "mappable_node_count": "Number of usable metabolites in this pathway", // + options.svg_info.pathway_title_padding, 
            "nodes_with_data": "Number of metabolites for this dataset in this pathway", // + options.svg_info.pathway_title_padding, 
            "no_mapping_for_any_pathway_count": "Number of usable metabolites in this dataset"
    }

    html += "<tbody>"

    for (var stat in stats.pathway) {
        var stat_name = stats_front_end[stat];
        if (stat_name != undefined) {
            html += "<tr> <td>" + stat_name +  "</td><td>" + 
                stats.pathway[stat] + "" + " </td>" +
                "<td> " + (stats.pathway[stat] * 100 / node_count).toFixed(2) + " </td> </tr>"
        }
    }

    for (var stat in stats.dataset) {
        var stat_name = stats_front_end[stat];
        if (stat_name != undefined) {

            html += "<tr> <td> " + stat_name +  "</td><td>" +
                stats.dataset[stat] + "" + " </td>" +
                "<td> " + (stats.dataset[stat] * 100 / data_count).toFixed(2) + " </td> </tr>"
        }
    }

    html += "</tbody> </table>"
    
    // Set the inner HTML to be that of the table.
    document.getElementById('stats-table').innerHTML = html;

    // Turn it into a datatable.
    //$('#table-stats').DataTable();
}


/**
 * Toggles the opacity of a specific class
 */
var toggle_class_opacity = function(class_name) {
    var opacity = options.svg.selectAll(class_name).attr('opacity');
    if (opacity == 0) {
        options.svg.selectAll(class_name).attr('opacity', 1);
    } else {
        options.svg.selectAll(class_name).attr('opacity', 0);
    }
}


/**
 * Removes the graph data from the stored object (pathway)
 * and the front end SVG.
 */
var remove_data = function() {
    // remove the datasets and graphs
    try {
        options.svg.selectAll('g.graph-group').remove();
        options.svg_info.title_group.selectAll('text.dataset-title').remove();
        pathway.data.dataset = {};
    } catch (err) {
        // quietly ignore
    }
}

/**
 * Toggles the graph text on and off for a nicer visual
 */
var dataset_action_toggle_graph_text = function() {
    toggle_class_opacity(".toggle-text");
    
}

var toggle_bar = function() {
    var opacity = 0;
    var visible = document.getElementById("toggle-graph-vis-button").value;
    if (visible == "true") {
        opacity = 1;
        document.getElementById('toggle-graph-vis-button').value = "false";
        document.getElementById('toggle-graph-vis-button').textContent = "hide graphs";
    } else {
        document.getElementById('toggle-graph-vis-button').value = "true";
        document.getElementById('toggle-graph-vis-button').textContent = "show graphs";
    }
    options.svg.selectAll('g.graph-group').attr('opacity', opacity);
}


var toggle_pie = function() {


}



var highlight_reactions_from_metabolites_with_data = function(sig_diff) {
    for (var from_node_bigg_id in options.stored.nodes_with_data_info) {
        var from_node = options.stored.nodes_with_data_info[from_node_bigg_id].node_id;
        var reactions = options.stored.node_info[from_node].reactions;
        for (var r in reactions) {
            var to_node = reactions[r].node_to;
            var class_name = "path.r" + reactions[r].reaction_id; //+ "f" +  from_node + "t" + to_node;
            //document.getElementById(class_name).attributes.stroke.nodeValue = "green";
            options.svg.selectAll(class_name).attr('stroke', options.actions.reaction_between_data_colour);
            options.svg.selectAll(class_name).attr('stroke-width', options.actions.reaction_between_data_stroke_width);
            //document.getElementById(class_name).attributes.stroke.nodeValue = "red";

            if (reactions[r].data.reversibility == true) {
                var class_name ="path.r"+ reactions[r].reaction_id;// + "f" +  to_node + "t" + from_node;
                options.svg.selectAll(class_name).attr('stroke', options.actions.reaction_between_data_reversible_colour);
                options.svg.selectAll(class_name).attr('stroke-width', options.actions.reaction_between_data_stroke_width);
                //document.getElementById(class_name).attributes.stroke.nodeValue = "green";
            }
        }
    }
}

var highlight_reactions_between_metabolites_with_data = function() {
    

}

var show_metabolites_with_sig_diff_samples = function(diff_amount) {
    for (var from_node in options.stored.nodes_with_data_info) {
        var node = options.stored.nodes_with_data_info[from_node];
        if (node.graph.max_sample_diff > 20) {
            //d3.select("#n" + from_node).attr("fill", options.actions.significant_difference_colour);
            //d3.select("#n" + from_node).attr("r", options.actions.significant_difference_radius);
            d3.select("#outside" + from_node).attr("fill", options.actions.significant_difference_colour);
        }
    }
}


var highlight_reactions_between_sig_diff_samples = function(diff_amount) {

}

var highlight_reactions_from_sig_diff_samples = function(diff_amount) {

}

var show_metabolites_with_sample_val = function(sample, value, comparator) {

}


var undo = function() {


}
