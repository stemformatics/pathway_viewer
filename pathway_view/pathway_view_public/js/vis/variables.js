// User needs to put in any transforms that are done on the SVG so that the
//Graphs can be placed appropriately
var transforms = [{margin_width: 50, margin_height: 336}];
//An array of colours which are used for the different probes
var colours = ["DarkOrchid", "#736AFF", "Tomato", "GreenYellow",
    "ForestGreen", "Orange", "DodgerBlue", "Blue", "Green", "Brown",
    "Deeppink", "BurlyWood", "CadetBlue",
    "Chartreuse", "Chocolate", "Coral", "CornflowerBlue", "Crimson",
    "Cyan", "Red", "DarkBlue",
    "DarkGoldenRod", "DarkGray", "Violet", "DarkGreen", "DarkKhaki",
    "DarkMagenta", "DarkOliveGreen",
    "DarkOrange", "DarkOrchid", "DarkRed", "DarkSalmon",
    "DarkSlateBlue", "DarkTurquoise",
    "DarkViolet", "DeepPink", "DeepSkyBlue", "DodgerBlue",
    "FireBrick", "Fuchsia",
    "Gold", "GoldenRod", "HotPink", "IndianRed", "Indigo"];



/**
 * The stored data for the pathway_viewer
 */
var pathway_viewer_data = {
    pathway: {},            // key: node_id,    value: node or reaction info + chebi_id
    dataset: {},            // key: chebi_id,   value: graph of expression vals
    node_ids_with_metabolite_list: [],
    metabolite_list: {
            data:{}
    },    // key: chebi_id,   value: list_values (p_value etc) 
    metabolite_info: {},    // key: chebi_id,   value: mass, synonyms, etc. + list_of_node ids
    search_list: {},        // key: synonym,    value: chebi_id
    u_bigg_id_to_chebi: {}, // key: universal_bigg_id: value: list[chebi_id]
    stats: {
        pathway: {
            mappable_node_count: 0, // keeps track of how many ids in a pathway 
            // are actually mappable i.e. have an entry in metabolite_info.
            nodes_with_data: 0,     // How many nodes in the pathway have data, i.e. raw
            // counts, some metaboilites may appear multiple times.
            unmappable_node_count: 0, // Number of metabolites (no dups)
        },
        dataset: {
            no_mapping_for_chosen_pathway_count: 0,
            no_mapping_for_any_pathway_count: 0,
            mapped_count: 0,
        },
        metabolite_list: {
            no_mapping_for_chosen_pathway_count: 0,
            no_mapping_for_any_pathway_count: 0,
            mapped_count: 0,
        },

        // which had data associated.
    }
}   

/**
 * Start visualisation and pull the data from the HTML.
 * 
 * Set the options and pathway to be global variables
 * accesssible by other functions.
 *
 */
var start_visualisation = function() {
    options = get_options();
    options.data = pathway_viewer_data;
    options.raw_svg_id = 'visdiv';
    options.svg_id = '#visdiv';
    
    var vis_json = JSON.parse(document.getElementById("visdiv").innerHTML);
    pathway = parse_mapping_data(options, document.getElementById("dictdiv").innerHTML);
    pathway = parse_pathway_data(pathway, vis_json.pathway.data);                
    pathway = update_dataset(pathway, vis_json.dataset);

    document.getElementById("dictdiv").innerHTML = "";
    document.getElementById("visdiv").innerHTML = "";

    // 1 is where the json data is stored in the passed file
    pathway = parse_map(pathway);
    pathway = overlay_graphs(pathway);
    // Add the metabolite list
    pathway = update_metabolite_list(pathway, vis_json.default_metabolite_list);

    document.getElementById("visdiv").style.display = "block";
    
    // Add the users selectable datasets and pathways to a table
    fill_data_table(vis_json.pathway_list, 'pathway name', 'pathways-sidebar', 'get_pathway_data');
    fill_data_table(vis_json.dataset_list, 'dataset name', 'datasets-sidebar', 'get_dataset_data');

    // Make the datasets visable first
    make_sidebar_section_visible("datasets-pathways");
}

/**
 *
 * Fills the data tables for datasets and pathways, fills it
 * with the list and creates a table in the set div.
 *
 */
var fill_data_table = function(list, label, div_id, onclick_function) {
    var html = "<table class='table' id='" + div_id +"-table' > <thead> <tr> " +
        "<th> Name </th> " +
        "<th> Access </th> " +
        "<th> Select </th>" +
        //"<th> Charge </th> " +
        //"<th> Mass </th> " +
        //"<th> Name </th> " 
        "</tr> </thead> <tbody>";

    for (var element in list) {
        html += "<tr> <td> " + list[element]['name'] + "</td>" + 
                    "<td>" + list[element]['private'] + "</td>" +
                    "<td><button onClick=" + onclick_function + "(" + element + ")>" + 
                    "view</button></td></tr>";
    }
    document.getElementById(div_id).innerHTML = html;
}


/**
 * Parses the metabolite information recieved as 
 * a JSON str into:
 *      1. stored as: metabolite_info
 *      2. search list: each synonym is turned into a key
 *      and the chebi id becomes the value.
 *
 * While itterating through, keeps a dictionary of:
 *      universal_bigg_ids -> list of chebis.
 *
 * This is used to associate the node IDs to a chebi_id.
 *
 * The metabolite_info doesn't get updated unless the user
 * refreshes the page and then this function is run again.
 */
var parse_mapping_data = function (pathway, raw_metabolite_info) {
    var u_bigg_id_key = 'bigg_id';
    var u_bigg_id_to_chebi = {};
    var search_list = {};
    var search_id_types = ['bigg_id', 'bigg_name', 'hmdb_synonyms', 'inchi_str', 'metanetx_id', 'mass', 'charge', 'formulae', 'hmdb_generic_name'];
    // Parse the JSON string.
    raw_metabolite_info = JSON.parse(raw_metabolite_info);

    for (var chebi_id in raw_metabolite_info) {
        var other_ids = raw_metabolite_info[chebi_id];

        for (var oid_index in search_id_types) {//other_ids) {
            var other_id_type = search_id_types[oid_index];
            // Other id type is the key and this will be associated
            // with a list of the ids of that type associated with 
            // the chebi id. For each of these we want to store it as
            // an entry in the search list for fast access.
            for (var o_id in other_ids[other_id_type]) {
                var other_id = other_ids[other_id_type][o_id];
                //if (search_list[other_id] == undefined) {
                //    search_list[other_id] = [];
                //}
                if (! (other_id in search_list)) {
                    search_list[other_id] = [];
                }
                search_list[other_id].push(chebi_id);
            }
        

            if (other_id_type == u_bigg_id_key) {

                // For each universal bigg id we want to
                // store a list of chebi ids associated.
                for (var o in other_ids[other_id_type]) {
                    other_id =  other_ids[other_id_type][0];
                    // If it becomes non one to one mapping
                    //if (u_bigg_id_to_chebi[other_id] == undefined) {
                    //    u_bigg_id_to_chebi[other_id] = [];
                    //}
                    u_bigg_id_to_chebi[other_id] = chebi_id; //.push(chebi_id);
                }
            }
        }
    }

    pathway.data.metabolite_info = raw_metabolite_info; 
    pathway.data.u_bigg_id_to_chebi = u_bigg_id_to_chebi;   
    pathway.search_list = search_list;
    return pathway;
}



/**
 * Parses the pathway data that the user has selected to display.
 * 
 * Each node_id gets a chebi_id tag added (from u_bigg_id_to_chebi)
 * based on the universal bigg_id.
 *
 * A list of node_ids for each chebi_id is saved to the metabolite_info
 * data structure in a dictionary with the pathway name as the key:
 *      metabolite_info[chebi_id][pathway_name] = list of ndoe ids
 */
var parse_pathway_data = function (pathway, raw_pathway_data) { 
    raw_pathway_data = JSON.parse(raw_pathway_data);
    var data_num = pathway.json_data_pos;
    var nodes = raw_pathway_data[data_num].nodes;
    var u_bigg_id_to_chebi = pathway.data.u_bigg_id_to_chebi;
    var metabolite_info = pathway.data.metabolite_info;
    var node_count = 0;
    var pathway_name = raw_pathway_data[0].map_name;
    // Omicxview name: raw_pathway_data[0].pathway.header.name;

    // Remove all the drawn pathway objects and reset the data
    // REMOVE >>>>>>>>>>>>>>>>>>>>>>>>>>>>
    // Does automatically as the SVG is re drawn.
    for (var node_id in nodes) {
        var node = nodes[node_id];
        node_count += 1;
        // Only want metabolites
        if (node.node_type == 'metabolite') {
            var u_bigg_id = node.bigg_id.slice(0, -2);

            // Get chebi ids from the u_bigg_id_to_chebi dict
            var chebi_id = u_bigg_id_to_chebi[u_bigg_id];
            node.chebi_id = chebi_id;

            // Add to the count that this is a mappable ID.
            if (chebi_id == undefined) {
                node.mappable = false;
                pathway.data.stats.pathway.unmappable_node_count += 1;
            } else {
                node.mappable = true;
                pathway.data.stats.pathway.mappable_node_count += 1;

                // Now for each chebi ID associated, we want to
                // update the list of node IDs that it points to.
                // First check if we have that pathway stored or not
                if (metabolite_info[chebi_id][pathway_name] == undefined) {
                    metabolite_info[chebi_id][pathway_name] = [];
                }
                metabolite_info[chebi_id][pathway_name].push(node_id);
            }
        }
    }

    pathway.data.pathway = raw_pathway_data;
    pathway.data.pathway.nodes = raw_pathway_data[data_num].nodes;
    pathway.data.pathway.reactions = raw_pathway_data[data_num].reactions;
    pathway.data.pathway.name = pathway_name;
    pathway.data.pathway.node_count = node_count;

    return pathway;
}



/**
 * Updates the dataset.
 *
 * Removes the SVG elements associated with the old
 * dataset and adds the new data to the data object.
 *
 */
var update_dataset = function (pathway, dataset) {
    remove_data();
    if (dataset != "na") {
        pathway.data.dataset = dataset;
        pathway.data.dataset.name = dataset.metadata[0];
    }
    return pathway;
}

var remove_metabolite_data = function(pathway) {
    // For each of the metabolite nodes with a list value
    // highlight green it if it is > 0 otherwise red for < 0
    pathway.data.node_ids_with_metabolite_list = new Array();
    /*
    for (var node_id in pathway.data.node_ids_with_metabolite_list) {
        var node_id = pathway.data.node_ids_with_metabolite_list[node_id];
        if (pathway.data.pathway.nodes[node_id] != undefined) {
            pathway.data.pathway.nodes[node_id].metabolite_list_info = null;
        }
    }
    */
    return pathway;
}


/**
 * Updates the metabolite list.
 *
 * Removes the SVG elements associated with the old
 * metabolite list and adds the new data to the data
 * object.
 */
var update_metabolite_list = function (pathway, metabolite_list) {
    // Remove all associated SVG elements
    remove_metabolite_data(pathway);
    pathway.data.metabolite_list = metabolite_list;

    // Set the new data
    var pathway_name = pathway.data.pathway.name;
    var data_overlay = new Array();
    pathway.node_ids_with_metabolite_list = []; // Reset the list storing nodes with
    // metabolite IDs
    // Highlight an outer doughnut to indictate there is metabolite info
    // associated with that metabolite.
    // Also update the id of the node so we can access it during updates

    /**
     * Keep track of the misses and hits (so we know which chebi IDs 
     * don't exist on the pathway and which ones were able to map.
     */
    var unmapped_chebis = {
            chosen_pathway: [],
            no_pathway: []
    };

    for (var chebi_id in pathway.data.metabolite_list.data) {
        var m_info = pathway.data.metabolite_list.data[chebi_id];
        // Get the nodes associated
        // Check if there is a match on any pathways or not.
        if (pathway.data.metabolite_info[chebi_id] == undefined) {
            pathway.data.stats.metabolite_list.no_mapping_for_any_pathway_count += 1;
            unmapped_chebis.no_pathway.push(chebi_id);
        } else {
            
            var nodes = pathway.data.metabolite_info[chebi_id][pathway_name];
            // Check if there was a match on the current pathway
            if (nodes == undefined) {
                pathway.data.stats.metabolite_list.no_mapping_for_chosen_pathway_count += 1;
                unmapped_chebis.chosen_pathway.push(chebi_id);
            } else {
                pathway.data.stats.metabolite_list.mapped_count += 1;
                // Actually add it now.
                for (var n_index in nodes) {
                    var node = pathway.data.pathway.nodes[nodes[n_index]];
                    data_overlay = parse_metabolite_list_to_node_id(node, m_info, data_overlay);
                }
            }
        }
    }
    highlight_metabolites_from_lists();
    return pathway;
}


var highlight_metabolites_from_lists = function() {
    // Draw the feature to show that there is list info associated with
    // the metabolite.
    //draw_metabolite_list_feature(data_overlay);
    // Highlight based on bh-adjusted and p-value
    // reddy = #FCAFCC
    // green = #ABF7E4
    // yellow = #FFFD7A
    // purple = #D7AFFC
    // blue = #BFFFFB
    // pink = #F8BFFF

    highlight_metabolite_on_list_value('bh adjusted p value', "#outside-", "#F8BFFF", "#AFFCBB");
    highlight_metabolite_on_list_value('fold change', "#inside-", "#BFFFFB", "white");
}

var get_options = function() {


var options = {
    data: pathway_viewer_data,
    data_info: {
        // Let the user perform a pre processing step on the data:
        pre_processing_action: "log2", // See perform_preprocessing for details
    },
    // Keep track of the class names so we can easily filter based on this
    // The data names are layered and stored as the class names so these 
    // can be easily grabbed without always having to precompute things
    // It is a layered approach and the ordering has a dict for each one
    // hyphens (-) make a separation between each layer
    // underscores (_) are a separator between each separte entity 
    class_names: {
                node_type: 1, /*'p' = primary 's' = secondary n = none */    // 0    metabolite type
                diff: 2, /*diff value between data*/                  // 1    difference between samples
                samples: 3, /*samples in a dataset*/                          // 2    samples
                name: 4, /*names for that metabolite*/                        // 3    names
                'p value': 5,                                                  // 4    pvalue
                't statistic': 6,                                              // 5    t statistic
                'z score': 7,                                                   // 6    z score
                'bh adjusted p value': 8,                                      // 7    bh adjusted value
                'fold change': 9,                                              // 8    fold change
                'standard error': 10,                                           // 9    standard error          
                type: 0 // node reation or graph
    },
    node_class_name_default: 'n-n-n-n-n-n-n-n-n-n', // assigns none to all
    node_class_name_list: ['n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n'],
    bigg_node_map: {},
    // options for the modal graphs
    modal: {
        graph_font_size: "15px",
        text_opacity: 1,
        is_modal: true,
        graph_div_id: "modal-graph",
        hover_on: true,
        svg: undefined,
        size: 150,
        width: 100,
        y: undefined, 
        max_expression_value: undefined, // Gets calculated
        assigned_colours: colours,
        colours: colours,
        yAxis: undefined,
        offset_graph_height: -50,
        offset_graph_width: -55,
        graph_outer_circle_radius: 180,
        graph_outer_circle_colour: "blue",
        graph_outer_circle_opacity: 0.3,
        toggle: {
            number_of_ticks_y_axis: 7
        },
    },
    toggle: {
        number_of_ticks_y_axis: 2
    },
    molecule_display_info: ['bigg_name', 'hmdb_generic_name', 'bigg_id', 'bigg_paths', 'metanetx_id', 'inchi_hmdb', 'inchi_str', 'hmdb_synonyms', 'info'],
    json_data_pos: 1,
    pathway_data_pos: 0,
    dataset_data_pos: 0,
    metabolite_lists: {
        p_value: {
            colour: "#6AFB92",
            size: 50
        }
    },
    pie: {
        label_position: 22,
        radius: 50,
        stroke_width: 3,
        stroke: "grey",
        text_size: "12px",
        font_family: "Gill Sans, sans-serif"
    },
    // Action variables which are changed by the user
    actions: {
        significant_difference_colour: "#A74AC7",
        significant_difference_radius: 60,
        reaction_between_data_colour: "#736AFF",
        reaction_between_data_stroke_width: 12,
        reaction_between_data_reversible_colour: "#6AFB92",
        node_radius: 100,
        opacity: 0.5,
        current_class: undefined,
        position: 2, // TEST
        action: "less",
        value: 100 // This is the value the user has selected
    },
    metabolite_list: {
        stroke: "#736AFF",
        fill: "white",
        stroke_width: 50,
        fill: "none",
        opacity: 0.2,
        /* If this is updated need to update the class names as well so that we get the
         * flow through with actions. */
        metabolite_list_header: ['name', 'p_value', 't_statistic', 'z_score', 'bh_adjusted_p_value', 'fold_change', 'standard_error', 'comments', 'raw_metabolite_id', 'raw_metabolite_id_type']
    },
    graph: {
        number_of_groups: 0, // Gets updated when a dataset is read in
        height: 60,
        width: 60,
        font_size: "12px",
        margin: {left: 24, top: 15, right: 0, bottom: 0},
        outer_circle_colour: "blue",
        outer_circle_opacity: 0.5,
        outer_circle_radius: 70,

//        offset_graph_width: -24,
//        offset_graph_height: -15,
    },
    /**
     * SVG styles i.e. for axis colours and stroke
     */
    svg_info: {
        pathway_title_padding: 50,
        dataset_title_padding: 100,
        metabolite_list_title_padding: 150,
        stats_btn: 200,
        btn_height: 50,
        btn_width: 200,
        stroke: "#AEB6BF",
        btn_colour: "#55545c",
        stroke_width: "1px",
    },
    // Visual componenets
    text_opacity: 0,
    colours: colours,
    /*
    graph_outer_circle_colour: "blue",
    graph_outer_circle_opacity: 0.5,
    graph_outer_circle_radius: 70,*/
    node_radius_primary: 15,
    node_radius_secondary: 10,
    node_other_stroke: "white",
    node_metabolite_stroke: "white",
    node_metabolite_colour: "orange",
    node_other_colour: "pink",
    node_opacity: 1,
    node_stroke_width: 3,
    reaction_count: 0,
    text_colour: "white",//"black",
    text_outline: "white",
    text_stroke: 0.5,
    text_size: "20px",
    label_text_stroke: 0.5,
    label_text_size: "42px",
    reaction_stroke: "#D1D0CE",
    reaction_stroke_width: 8,
    reaction_stroke_opactity: 1,
    search_colour: "#25eab8",
    //* Size of the graphs
    size: 60,
    graph_font_size: "12px",
    colours: colours,
    offset_graph_width: -24,
    offset_graph_height: -15,
    //Modes which can be true or false
    hover_on: true,
    delete_mode: true, //Escher specific
    // Data
    //svg_id: svg_id,
    //transforms: transforms,
    svg_overlay: null,
    collumn_array: new Array(),
    x_label_array: new Array(),
    elements_to_remove: new Array(),
    graphs: new Array(),
    // The width is used to scale
    width: 1800,
    margin_width: 300,
    margin_height: 200,
    //Tracking variables
    metabolite_count: 0,
    bar_count: 0, /* Tells us how many bars we are going to have to create */
    div_factor: 1 //Used in calculating rotations to determine graph placement
};
    // Clean the metabolite list headers (want to remove the underscores)
    var clean_headers = new Array();

    for (var header in options.metabolite_list.metabolite_list_header) {
        clean_headers.push(options.metabolite_list.metabolite_list_header[header].replace(/_/g, ""));
    }
    options.metabolite_list.metabolite_list_header = clean_headers;
return options;
}

