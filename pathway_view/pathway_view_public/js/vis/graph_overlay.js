/**
 * Add the dataset title
 */
var add_dataset_title = function (options) {
    var text_colour = options.text_colour;
    var text_stroke = options.text_stroke;
    var text_size = options.text_size;

    var text = options.svg_info.title_group.append("text")
            .attr("class", "dataset-title")
            .attr("id", "dataset-title")
            .attr("x", options.svg_info.width/2)
            .attr("y", options.svg_info.dataset_title_padding)
            .attr("fill", text_colour)
            .attr("text-anchor", "middle")
            .attr("stroke-width", text_stroke)
            .style("font-size", text_size)
            .style("font-family", "sans-serif")
            .text(options.data.dataset.name)
};



/**
 * Data is stored as: gene, sample, value
 * So we need to group the samples together for two genes that way we can
 * do a logs ratio test.
 */
var group_data_by_gene = function(options) {
    var dataset_data = options.graph_data.data;
    // Store the data associated with each gene
    var gene_data = {};
    // Keep track of the length of the longest value 
    // if there are genes with two samples it means we will perform a 
    // log odds ratio
    var max_samples_for_gene = 0;
    for (var d in dataset_data) {
        var gene = dataset_data[d][0];
        var data = gene_data[gene.bigg_id];
        if (data == undefined) {
            data = [];
        }
        data.push(gene);
        // Store the max number of gene samples
        if (data.size() > max_samples_for_gene) {
            max_samples_for_gene = data.size();
        }
        gene_data[gene.bigg_id] = data;
    }
    // Add the max_number of samples to the options
    options.max_samples_for_gene = max_samples_for_gene;
    // return the gene data grouped
    return gene_data;
}



/**
 * 
 */
var overlay_gene_expression_data = function(options) {
    var dataset_data = options.graph_data.data; // data user has uploaded
    var gene_reaction_map = options.gene_reaction_map; // Stores map between genes and associated reactions
    var reaction_gene_map = options.reaction_gene_map; // Stores genes part of reactions and reaction data
    // Store colour list so colours can be associated to genes
    var colour_map = {};
    // Store reactions which have graphs associaetd (e.g. gene expr data)
    var nodes_with_graphs = new Array();
    // Group the gene data together so we can perform the log odds ratio
}


/**
 * Adds the graph to the modal info tooltip so they can see it better
 */
var update_modal_svg = function (div_id, node, node_id) {
    // Clear the old graph
    $('#' + divid).empty();
    // Add the new graph
    create_new_graph(node, modal_options, node_id);
}

/**
 * The main overlay function which gets called when there is data to overlay
 */
overlay_graphs = function(options) {
    var dataset_data = options.data.dataset.data; // The data the user has uploaded
    var chebi_to_u_bigg_id = options.data.metabolite_info;
    var u_bigg_id_to_chebi = options.data.u_bigg_id_to_chebi;
    var pathway_nodes = options.data.pathway.nodes;

    var min_value = graph_options.data.default_min_value;
    var max_value = graph_options.data.default_max_value;

    // Group the data by metabolite and sample type
    var metabolite_graph_map = {};
    var metabolite_count = 0;

    add_dataset_title(pathway);

    for (var d in dataset_data) {

        var row = dataset_data[d];
        var metabolite_id = row[0];
        /**
         * Make a new data element 
         */
        var tmp = {};
        /**
         * Test is the value is the new min or max
         */
        tmp.value = row[2];

        if (tmp.value < min_value) {
            min_value = tmp.value;
        }
        if (tmp.value > max_value) {
            max_value = tmp.value;
        }
        // For now we don't have standard deviation       
        tmp.standard_dev = 0;

        /**
         * If the data can be split on the split token this
         * is indicative that there is a sample type followed by
         * the sample ID.
         *
         * For example data can be in format sample_type|sample_id
         *
         * The ID is always the last element
         */ 
        var id = row[1].split(graph_options.data.split_token);

        if (id.length > 1) {
            var group_count = 0;
            for (var sub_group in graph_options.data.groups) {
                tmp[sub_group] = id[group_count];
                group_count += 1;
            }
        };
    
        tmp.id = id[id.length - 1];        

        /**
         * Keep track of the samples for that group
         *
         * If we don't already have an array for this metabolite
         * create one.
         */
        var values = metabolite_graph_map[metabolite_id];
        if (values == undefined) {
            var values = [];
        }
        values.push(tmp); 

        metabolite_graph_map[metabolite_id] = values;//bars;
    }

    graph_options.data.default_min_value = min_value;
    graph_options.data.default_max_value = max_value;

    // Keep a list of the data which weren't in the pathway.
    var unmapped_chebis = {
            chosen_pathway: [],
            no_pathway: []
    };
    
    for (var chebi_id in metabolite_graph_map) {
        var bars = metabolite_graph_map[chebi_id];
        metabolite_count += 1;
        /**
         * Check if we have a bigg id for this metabolite first
         * as there may be no pathways associated thus it won't exist
         * in the mapping table.
         */ 
        if (chebi_to_u_bigg_id[chebi_id] == undefined) {
            pathway.data.stats.dataset.no_mapping_for_any_pathway_count += 1;
            unmapped_chebis.no_pathway.push(chebi_id);
        } else {
            /**
             * Node ids are stored as an extra value in the metabolite info
             * and are stored to be associated with a pathway
             */
            var nodes = chebi_to_u_bigg_id[chebi_id][pathway.data.pathway.name];
            /**
             * If nodes == undefined this means that the users' metabolite isn't
             * in this pathway. Keep track of these and have on hand to return to
             * the user if necessary.
             *
             * Add to the stats count for ds_metabolites_not_in_pathway
             */
            if (nodes == undefined) {
                pathway.data.stats.dataset.no_mapping_for_chosen_pathway_count += 1;
                unmapped_chebis.chosen_pathway.push(chebi_id);
            } else {
                pathway.data.stats.dataset.mapped_count += 1;
                for (var n in nodes) {
                    var node_id = nodes[n];
                    pathway.data.stats.pathway.nodes_with_data += 1;
                    // want to assign the raw metabolite id to this node
                    var node = pathway_nodes[node_id];
                    // Get the max difference between the bars we we can quickly check if it is
                    // diferenttially expressed or significantly differnet
      //              graph['max_sample_diff'] = get_difference_between_samples(bars);

                    pathway_nodes[node_id].data = bars;
                    // False indicates it isn't the modal
                    // Since we have already done the stat test we want to clear it
                    graph_options.data.statistical_test = 'log2';
                    graph_options.y_axis_label = "log2";
                    draw_graph(pathway_nodes[node_id], options.svg, false);

                        /*
                         * ------------------------------------------------------------------
                         *      Update the class name to include: max sample diff and
                         *      the names of the samples, we do this for the graph and 
                         *      the node
                         *  -----------------------------------------------------------------
                         */
                        // Update the diff and samples
                        var types = ['n', 'g']; // update for both the graph and the node
          //              update_node_class_name(node.node_id, options.class_names.diff, graph['max_sample_diff'], types);
            //            update_node_class_name(node.node_id, options.class_names.samples, Object.keys(bars).join('_'), types);
                //    }
                    }
                }
            }
       }
    pathway.data.unmapped_dataset_ids = unmapped_chebis;
    pathway.data.dataset.metabolite_count = metabolite_count;
    return pathway;
}


/**
 * Re overlays an already parsed dataset.
 */
var overlay_parsed_dataset = function (pathway) {

    // TO DO to make more efficient currently itterates
    // through again.
}

/**
 * Perform an action on the data, a preprocessing
 * step.
 *
 * This can be used to transform the data to another
 * scale.
 *
 * Example, log2.
 *
 * Parameter:   options = has the preprocessing action
 *      stored in options.data.pre_processing_action
 *
 *              value = expression value to be changed
 *
 *  Return: value changed by some action (or unchanged)
 */
var perform_preprocessing = function(value, options) {
    if (options.data_info.pre_processing_action == 'log2') {
        return Math.log2(value);
    }
    return value;
}


/**
 * Updates the node ID to reflect characteristics associated 
 * with that node.
 *
 * For example: when a metabolite list is added, any statistical 
 * information associated with that node is added
 * i.e. p-value then beomes a part of the ID.
 */
var update_node_class_name = function(node_id, position, value, types) {
    // update the position of the class name  for the position with
    // the given value
    // get the old class name
    for (var node_type in types) {
        var node = d3.select("*[id^="+ types[node_type] + node_id + "]");
        if (node[0][0] != null) {
            var class_name = node.attr("id");
            class_name = class_name.split('-');
            class_name[position] = value;
            node.attr("id", class_name.join('-'));
        }
    }
    
}

var get_difference_between_samples = function(bars) {
    var max_diff = 0;
    var min = 100;
    var max = 0;
    var diffs = [];
    for (var b in bars) {
        var val = bars[b].value;
        if (val < min) {
            min = val;
        } 
        if (val > max) {
            max = val;
        }
    }
    return Math.abs(max - min);
}



