/**
 * Contains all the actions for the vis page.
 *
 * and the search functionality.
 */




/**
 * Initialising the search div
 */
$(document).ready( function() {
    /**
     * Create the table based on the IDs that have been passed to 
     * the front end.
     */ 
    // Start the visualisation
    start_visualisation();

    $("#action-container" ).draggable();
    $("#stats-container").draggable();

    // Set the search list so users can search for metabolites
    set_search_list();

    $('#datasets-sidebar-table').DataTable(
    );

    $('#pathways-sidebar-table').DataTable(
    )
});

/**
 * Toggles the sidebar minimising and displaying
 */
var toggle_minimise_sidebar = function() {
    var toggle = document.getElementById("minimise-btn").innerHTML.trim();
    if (toggle == "&lt;&lt;") {
        document.getElementById("minimise").style.display = "none";
        document.getElementById("minimise-btn").innerHTML = ">>";
        document.getElementById("sidebar").style.width = "3%";
    } else if (toggle == "&gt;&gt;") { // &gt; == >
        document.getElementById("minimise").style.display = "block";
        document.getElementById("minimise-btn").innerHTML = "<<";
        document.getElementById("sidebar").style.width = "25%";
    }
}

/**
 * Makes the selected tab visible on the new sidebar
 */
var make_sidebar_section_visible = function(id) {
    var side_bar_ids = ["metabolite-info", "datasets-pathways"];//, "pathways-sidebar"];
    for (var i in side_bar_ids) {
        document.getElementById(side_bar_ids[i]).style.display = "none";
        document.getElementById(side_bar_ids[i] + "-btn").style.backgroundColor = "white";
    }
    document.getElementById(id).style.display = "block";
    document.getElementById(id + "-btn").style.backgroundColor = "rgba(242, 105, 64, 0.37)";
}

/**
 * Updates the size of the radius of the selected value
 */
function update_diff_val(val) {
    options.actions.value = val;
    document.getElementById('diff-val').innerHTML=val;
}

function update_prob_diff_val(val) {
    options.actions.value = val/100;
    document.getElementById('prob-diff-val').innerHTML=val/100;
}


function update_node_val(val) {
    options.actions.node_radius = val;
    document.getElementById('node-val').innerHTML=val; 
}

function update_opacity_val(val) {
    options.actions.opacity = val/100;
    document.getElementById('opacity-val').innerHTML=val;
}


function show_metabolites_diff_action_bar() {
    document.getElementById('action-container').style.display = "block";
}

function update_action_type() {
    options.actions.action = $('input[name="action-option"]:checked').val();
}

/**
 * Sets the class we currently want to be changing
 * Either sig diff or metabolite list etc
 */
function set_class_to_update_with_value() {
    var type = $('input[name="action-type"]:checked').val(); // type e.g. secondary, primary, sig diff value
    var action = $('input[name="action"]:checked').val(); // the action to update
    var position = options.class_name[type]; //position of the class name to update
    var value = int($('#input-value').val()); // the value the user has selected
    if (action == 'equals') {
        // Something
    }
}

/**
 * Colour picker from: https://github.com/DavidDurman/FlexiColorPicker
 * Under MIT licence
 */

ColorPicker.fixIndicators(
        document.getElementById('slider-indicator'),
        document.getElementById('picker-indicator'));

ColorPicker(
        document.getElementById('slider'),
        document.getElementById('picker'),

        function(hex, hsv, rgb, pickerCoordinate, sliderCoordinate) {
            // Check if the user has updated the selection
            options.actions.action = $('input[name="action-option"]:checked').val();
            var position = $('input[name="position-option"]:checked').val().replace(/_/g, ' ');
            options.actions.position = options.class_names[position];
            ColorPicker.positionIndicators(
                document.getElementById('slider-indicator'),
                document.getElementById('picker-indicator'),
                sliderCoordinate, pickerCoordinate
            );
            d3.selectAll("circle.metabolite-graph").filter( function(d) {
                var value = d3.select(this).attr('id').split('-')[options.actions.position]
                if (options.actions.position > 3) {
                    if (value[0] == 'n') {
                        value = value.substr(1);
                        value = -1 * value;
                    } else {
                        value = value.substr(1);
                    }
                }
                if (options.actions.action == 'equal') {
                    return value == options.actions.value;
                } else if (options.actions.action == 'less') {
                    return value < options.actions.value;
                } else {
                    return value > options.actions.value;
                }
            })
            .style("fill", hex)
            .attr("r", options.actions.node_radius)
            .attr("opacity", options.actions.opacity);

            document.getElementById('colour-example').style.backgroundColor = hex;
        }
)

/**
 * -------------------------------------------------------------------------------------------
 *                          Search and Zoom
 *  https://bl.ocks.org/iamkevinv/0a24e9126cd2fa6b283c6f2d774b69a2
 * -------------------------------------------------------------------------------------------
 */

/**
 * Reset the transform after a user has zoomed to a metabolite.
 */
var reset_transform = function() {
    var svg = options.svg;
    var node_ids = pathway.curr_search_node_ids;

    svg.transition()
        .duration(200)
        .attr("transform", "translate(" + options.translate + ")scale(" + options.scale + ")");

}


/**
 * Resets the outer node colours after the search 
 * is finished.
 */
var reset_node_colours = function() {
    for (var n_index in pathway.curr_search_node_ids) {
        d3.select("#oc" + pathway.curr_search_node_ids[n_index]).attr("opacity", 0);
    }
}

/**
 * Auto complete search bar to help the user in their metabolite search.
 *
 * Runs on page load.
 */
var set_search_list = function() {
    var search_list = Object.keys(pathway.search_list);
    // Select the search input and set the search items as tags
    $("#search-input").autocomplete({
        source: search_list
    });
}

/**
 * Called when the user clicks the search button.
 *
 * Gets the text input from the user and looks up to find nodes that
 * are attached to this synonym.
 */
var search_for_metabolite = function() {
    var search_reset = document.getElementById('search-btn').innerHTML;

    if (search_reset == "search") {
        document.getElementById('search-btn').innerHTML = "reset";
        document.getElementById('search-next-btn').innerHTML = "zoom in";
        search_for_nodes();

    } else {
        document.getElementById('search-btn').innerHTML = "search";
        document.getElementById('search-next-btn').innerHTML = "";
        reset_transform();
        reset_node_colours();
    }
}

var search_next_metabolite = function () {
    zoom_to_node(pathway.curr_search_node_ids[pathway.curr_search_node_id_index]);
    pathway.curr_search_node_id_index += 1;
    if (pathway.curr_search_node_id_index > pathway.curr_search_node_id_index.length) {
        pathway.curr_search_node_id_index = 0;
    }
}
    
var search_for_nodes = function () {
    var metabolite = document.getElementById('search-input').value;
    // Get the matching chebi id
    var chebi_ids = pathway.search_list[metabolite];
    // If undefined we want to return
    if (chebi_ids == undefined) {
        document.getElementById('search-input').value = "Metabolite: " + metabolite + " not found."; 
        return;
    }
    /* Otherwise get a list of node ids and search to the first one.
     * From the chebi_id we can get the list of nodes for that 
     * pathway which we set up when parsing the pathway.
     */ 
    var node_ids = [];
    for (var c_index in chebi_ids) {
        var chebi_id = chebi_ids[c_index];
        var tmp_node_ids = pathway.data.metabolite_info[chebi_id][pathway.data.pathway.name];
        node_ids = node_ids.concat(tmp_node_ids)
    }
    // Set the current nodes that are being searched in pathway (so 
    // we can access them and go to the next ones.
    pathway.curr_search_node_ids = node_ids;
    pathway.curr_search_node_id_index = 0;

    // Also want to set the node colour of each of the metabolites so we can 
    // differentiate it.
    for (var n_index in node_ids) {
        d3.select("#oc" + node_ids[n_index]).attr("opacity", 1); 
    }

}


/**
 * Zooms to a particular node id.
 *
 */
var zoom_to_node = function(node_id) {
    // Reset the SVG zoom just incase the user has already tried 
    // to zoom in.
    reset_transform();
    var node = d3.select("#t" + node_id);
    var offset = 0;
    var zoom_factor = 4;
    var x = node.attr("x") - offset;
    var y = node.attr("y")  - offset;
    var translate = x + "," + y;
    var scale = options.scale * zoom_factor ;

    var translate = ((options.svg_info.width/2) - (scale * x)) + "," + ((options.svg_info.height/2) - (scale * y));

    var svg = options.svg;
    svg.transition()
        .duration(200)
        .attr("transform", "translate(" + translate + ")scale(" + scale + ")");
}



/**
 * ---------------------------------------------------------------------------------------------
 *                           Save as PNG
 * http://bl.ocks.org/Rokotyan/0556f8facbaf344507cdc45dc3622177
 * ---------------------------------------------------------------------------------------------
 */

// Set-up the export button
var save_svg_to_png = function(){
	var svgString = getSVGString(options.root_svg.node());
	var width = options.svg_info.width;
	var height = options.svg_info.height;
	svgString2Image( svgString, 2*width, 2*height, 'png', save ); // passes Blob and filesize String to the callback

	function save( dataBlob, filesize ){
		saveAs( dataBlob, options.data.pathway.name + '.png' ); // FileSaver.js function
	}
};


// Below are the functions that handle actual exporting:
// getSVGString ( svgNode ) and svgString2Image( svgString, width, height, format, callback )
function getSVGString( svgNode ) {
	svgNode.setAttribute('xlink', 'http://www.w3.org/1999/xlink');
	var cssStyleText = getCSSStyles( svgNode );
	appendCSS( cssStyleText, svgNode );

	var serializer = new XMLSerializer();
	var svgString = serializer.serializeToString(svgNode);
	svgString = svgString.replace(/(\w+)?:?xlink=/g, 'xmlns:xlink='); // Fix root xlink without namespace
	svgString = svgString.replace(/NS\d+:href/g, 'xlink:href'); // Safari NS namespace fix

	return svgString;

	function getCSSStyles( parentElement ) {
		var selectorTextArr = [];

		// Add Parent element Id and Classes to the list
		selectorTextArr.push( '#'+parentElement.id );
		for (var c = 0; c < parentElement.classList.length; c++)
				if ( !contains('.'+parentElement.classList[c], selectorTextArr) )
					selectorTextArr.push( '.'+parentElement.classList[c] );

		// Add Children element Ids and Classes to the list
		var nodes = parentElement.getElementsByTagName("*");
		for (var i = 0; i < nodes.length; i++) {
			var id = nodes[i].id;
			if ( !contains('#'+id, selectorTextArr) )
				selectorTextArr.push( '#'+id );

			var classes = nodes[i].classList;
			for (var c = 0; c < classes.length; c++)
				if ( !contains('.'+classes[c], selectorTextArr) )
					selectorTextArr.push( '.'+classes[c] );
		}

		// Extract CSS Rules
		var extractedCSSText = "";
		for (var i = 0; i < document.styleSheets.length; i++) {
			var s = document.styleSheets[i];

			try {
			    if(!s.cssRules) continue;
			} catch( e ) {
		    		if(e.name !== 'SecurityError') throw e; // for Firefox
		    		continue;
		    	}

			var cssRules = s.cssRules;
			for (var r = 0; r < cssRules.length; r++) {
				if ( contains( cssRules[r].selectorText, selectorTextArr ) )
					extractedCSSText += cssRules[r].cssText;
			}
		}


		return extractedCSSText;

		function contains(str,arr) {
			return arr.indexOf( str ) === -1 ? false : true;
		}

	}

	function appendCSS( cssText, element ) {
		var styleElement = document.createElement("style");
		styleElement.setAttribute("type","text/css");
		styleElement.innerHTML = cssText;
		var refNode = element.hasChildNodes() ? element.children[0] : null;
		element.insertBefore( styleElement, refNode );
	}
}


function svgString2Image( svgString, width, height, format, callback ) {
	var format = format ? format : 'png';

	var imgsrc = 'data:image/svg+xml;base64,'+ btoa( unescape( encodeURIComponent( svgString ) ) ); // Convert SVG string to data URL

	var canvas = document.createElement("canvas");
	var context = canvas.getContext("2d");

	canvas.width = width;
	canvas.height = height;

	var image = new Image();
	image.onload = function() {
		context.clearRect ( 0, 0, width, height );
		context.drawImage(image, 0, 0, width, height);

		canvas.toBlob( function(blob) {
			var filesize = Math.round( blob.length/1024 ) + ' KB';
			if ( callback ) callback( blob, filesize );
		});


	};

	image.src = imgsrc;
}
