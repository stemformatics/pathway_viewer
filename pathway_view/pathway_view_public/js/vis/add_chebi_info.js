var parse_chebi_bigg_data = function(options) {
    var data = options.data[3];
    var data_overlay = new Array();
    for (var bigg_id in data) {
        var nodes = stored_data.bigg_node_map[bigg_id]
        for (var n in nodes) {
            var node = nodes[n];
            var x = node.x;
            var y = node.y;
            var tmp = {};
            tmp.x = x;
            tmp.y = y;
            tmp.node_id = node.node_id;
            tmp.info = data[bigg_id];
            data_overlay.push(tmp);
        }
    }
 
    options['bigg_chebi_list'] = data_overlay;
    return options;
}

var tooltip_mapping_info = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("color", "black")
    .style("background-color", "white")
    .style("border-radius", "7px")
    .style("opacity", 0);


var display_chebi_info = function(all) {
    var svg = options.svg
    
    svg.selectAll("dot")
    .data(options['bigg_chebi_list'])
    .enter().append("circle")
            .attr("r", 30)
            .attr("cx", function(d) {
                        return d.x;
            })
            .attr("cy", function(d) {
                        return d.y;
            })
            .attr("fill", "#33FFB5")
            .attr("opacity", 0.2)
            .on("mouseover", function(d) {
                var html = "<table class='table'> <thead> <tr> " +
                    "<th> ChEBI ID </th> " +
                    "<th> Charge </th> " +
                    "<th> Mass </th> " +
                    "<th> Name </th> " +
                    "</tr> </thead> <tbody>";
                for (var chebi in d.info.other_chebis) {
                    if (chebi.chebi_id == d.info.chosen_chebi.chebi_id) {
                        html += "<tr style='background-color:green'>" + "<td> " + chebi.chebi_id + " </td> " +
                            "<td> " + chebi.charge + " </td>" +
                            "<td> " + chebi.mass + "</td>" +
                            "<td> " + chebi.name + "<td/> </tr>"

                    } else if (all) {
                        html += "<tr>" + "<td> " + chebi.chebi_id + " </td> " +
                            "<td> " + chebi.charge + " </td>" +
                            "<td> " + chebi.mass + "</td>" +
                            "<td> " + chebi.name + "<td/> </tr>"
                    }
                }
                html += "</tbody> </table>"
                tooltip_mapping_info.transition()
                    .duration(50)
                    .style("opacity", .9);
                tooltip_mapping_info.html( html)
                    .style("left", (d3.event.pageX) + "px")
                    .style("top", (d3.event.pageY - 28) + "px");
            })
            .on("mouseout", function(d) {
                tooltip_mapping_info.transition()
                    .duration(50)
                    .style("opacity", 0);
            });
}

