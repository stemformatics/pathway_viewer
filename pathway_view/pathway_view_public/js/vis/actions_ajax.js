/**
 * Queries for the pathway data based on a pathway ID
 */
var get_pathway_data = function(path_id){
    // Send the ajax query to the application
    jQuery.ajax({
        url     : '/vis/get_pathway_data',
        type    : 'POST',
        data    : {'pathway_id': path_id},
        dataType: 'json',
        success : function(response) {
                document.getElementById(options.raw_svg_id).innerHTML = ""
                 
                // Keep the same metabolite list as previously
                var metabolite_list = pathway.data.metabolite_list;
                // Set the pathway to contain the new data in the JS options
                pathway = parse_pathway_data(pathway, JSON.parse(response).pathway.data);
                // parse the map, options are created for the first pathway
                pathway = parse_map(pathway);
                // Overlay graphs if the user has a dataset selected
                pathway = overlay_graphs(pathway); 

                // Set the colours of the pathway by re uploading the metaboilite
                // list to this pathway node.  
                update_metabolite_list(pathway, metabolite_list);
        }
    })
}


/**
 * Queries for the dataset data based on dataset ID
 */
var get_dataset_data = function(ds_id){
    jQuery.ajax({
        url     : '/vis/get_dataset_data',
        type    : 'POST',
        data    : {'dataset_id': ds_id},
        dataType: 'json',
        success : function(response) {
            var dataset = response['dataset'];
            var metabolite_list = response['metabolite_list'];
            // Make the JS call to overlay the graphs
            pathway = update_dataset(pathway, dataset);
            
            pathway = overlay_graphs(pathway);

            pathway = update_metabolite_list(pathway, metabolite_list); 
        }
    });
}


function get_metabolite_list_data(list_id) {
    var list = list_id;
    jQuery.ajax({
        url     : '/vis/get_metabolite_list_data',
        type    : 'POST',
        data    : {'metabolite_list_id': list_id},
        dataType: 'json',
        success : function(response) {
            // Don't actually need to update anything we just want to store the data
            // and process it with regards to the pathway so we can let the
            // user perform actions
            pathway = update_metabolite_list(pathway, response);
        }

    });
}


/**
 * Gets the mapping information, chebi_map between each bigg identifier and chebi
 * id.
 */
function get_metabolite_list_mapping_data(list_id) {
    var list = list_id;
    jQuery.ajax({
        url     : '/vis/get_metabolite_list_mapping_data',
        type    : 'POST',
        data    : {'metabolite_list_id': list_id},
        dataType: 'json',
        success : function(response) {
            var options = get_options();
            options = set_options_svg_id('#visdiv')
            name = set_metabolite_list(response);
            process_metabolite_name_map(name);
        }

    });
}
