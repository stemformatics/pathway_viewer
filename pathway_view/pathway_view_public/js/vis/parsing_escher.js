/**
 * Parses an Escher map. 
 * https://github.com/patorjk/d3-context-menu for the context menu.
 */

/* Load in the map as a json file */
var parse_map = function (options) {
    var data_pos = options.json_data_pos;
    var pathway = options.data.pathway[data_pos];
    //define_menu(options);
    create_svg(pathway.canvas, options);
    create_nodes(pathway.nodes, options);
    create_reactions(pathway.reactions, options);
    create_labels(pathway.text_labels, options);
    add_pathway_title(options);
    return options;
};


/**
 * Creates an svg.
 */
var create_svg = function (svg_data, options) {
    // Set the scale amount when zoomed to be 10 % of the page
    var zoom_box_size = 0.1;

    var delete_mode = options.delete_mode;
    var svg = options.svg;
    // Width is based on the container that the svg is placed in
    var width = options.width;
    var delete_mode = false;
    // Since we want to heep the ratio only allow the user to scale the 
    // width (otherwise the graph would get distorted)
    var scale_w = width / svg_data.width;
    var width = svg_data.width * scale_w;               
    var height = svg_data.height * scale_w;
    var margin_width = -svg_data.x + options.margin_width;
    var margin_height = -svg_data.y + options.margin_height;
    options.translate_x = margin_width;
    options.trasnlate_y = margin_height;

    svg = d3.select(options.svg_id).append("svg")
            .attr("id", "graphSVG")
            .attr("width", options.width)
            .attr("height", scale_w * svg_data.height);

    var node_group = svg.append("g")
            .attr("transform", "translate(" + (margin_width * scale_w) +
                "," + (margin_height * scale_w) + ")" + " scale(" + scale_w + ")");

    svg.call(d3.behavior.zoom()
                .on("zoom", function () {
                    var translate_vals = d3.event.translate;
                    var scale_value = d3.event.scale / 4;
                    node_group.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + scale_value + ")")
                })
            );

    var title_group =  svg.append("g")
            .attr("transform", "translate(0, 0)")

    svg.append("svg:defs").append("svg:marker")
            .attr("id", "triangle-end")
            .attr("refX", 5)
            .attr("refY", 2)
            .attr("markerWidth", 5)
            .attr("markerHeight", 5)
            .attr("orient", "auto")
            .attr("fill", options.reaction_stroke)
            .append("path")
            .attr('d', "M 0 0 4 2 0 4 0 2");

    // Keep track of the root_svg so that we can export it to PNG
    options.root_svg = svg;
    options.svg = node_group;
    options.svg_info.title_group = title_group;
    // Store the width and height
    options.svg_info.width = width;
    options.svg_info.height = height;
    // Store the origional transform so we can easily change
    // it back when a user has zoomed to a node.
    options.translate = margin_width * scale_w + "," + margin_height * scale_w;
    options.scale = scale_w;
    options.zoom_scale = scale_w * 0.1;
};



/**
 * Add the pathway title in
 */
var add_pathway_title = function (options) {
    var text_colour = options.text_colour;
    var text_stroke = options.text_stroke;
    var text_size = options.text_size;

    var text = options.svg_info.title_group.append("text")
            .attr("class", "pathtext")
            .attr("id", "title")
            .attr("x", options.svg_info.width/2)
            .attr("y", options.svg_info.pathway_title_padding)
            .attr("fill", text_colour)
            .attr("stroke-width", text_stroke)
            .style("font-size", text_size)
            .attr("text-anchor", "middle")
            .style("font-family", "sans-serif")
            .text(options.data.pathway.name)

/*
    var rect = options.svg_info.title_group.append("rect")
            .attr("class", "pathrect")
            .attr("id", "statsbtn")
            .attr("x", options.svg_info.width/2 - (options.svg_info.btn_width/2))
            .attr("y", options.svg_info.stats_btn - options.svg_info.btn_height * 0.66)
            .attr("height", options.svg_info.btn_height)
            .attr("width", options.svg_info.btn_width)
            .attr("fill", options.svg_info.btn_colour)
            .attr("stroke-width",  options.svg_info.stroke_width)
            .attr("stroke", options.svg_info.stroke)
            .on('click', function() {
                toggle_modal_display("stats-container");
            })


    var text = options.svg_info.title_group.append("text")
            .attr("class", "pathtext")
            .attr("id", "stats")
            .attr("x", options.svg_info.width/2)
            .attr("y", options.svg_info.stats_btn)
            .attr("fill", text_colour)
            .attr("stroke-width", text_stroke)
            .style("font-size", text_size)
            .style("font-family", "sans-serif")
            .attr("text-anchor", "middle")
            .text("Stats")
            .on('click', function() {
                toggle_modal_display("stats-container");
            })

*/
};


/**
 * Appends the marking circle to the node.
 */
var create_node_circle = function (node, group, node_data, options) {

    var node_radius_primary = options.node_radius_primary;
    var node_metabolite_stroke = options.node_metabolite_stroke;
    var node_metabolite_colour = options.node_metabolite_colour;
    var node_radius_secondary = options.node_radius_secondary;
    var node_other_colour = options.node_other_colour;
    var map_node_data = options.data.pathway.nodes;
    var node_other_stroke = options.node_other_stroke;
    var node_opacity = options.node_opacity;
    var this_node = node_data[node];

    // We need to store the important information for this node in our node
    // info so we can do calculations on it later
    /*var temp = {};
    temp['node_is_primary'] = this_node.node_is_primary;
    temp['node_type'] = this_node.node_type;
    temp['name'] = this_node.name;
    temp['bigg_id'] = this_node.bigg_id;

    options.stored.node_info[node] = temp;*/
    
    if (this_node.node_type == "metabolite" ) {

            var circle_outer = group.append("circle")
                .attr("id", "oc" + node)
                .attr("class", function () {
                    return "metabolite-node";
                })
                .attr("cx", this_node.x)
                .attr("cy", this_node.y)
                .attr("r", function () {
                    if (this_node.node_is_primary) {
                        return 2 * node_radius_primary;
                    } else {
                        return 2 * node_radius_secondary;
                    }
                })
                .attr("stroke-width", options.node_stroke_width)
                .attr("stroke", function () {
                    if (this_node.node_type == "metabolite") {
                        return node_metabolite_stroke;
                    } else {
                        return node_other_stroke;
                    }
                })
                .attr("opacity", 0)
                .attr("fill", "#33E0FF");

        var circle = group.append("circle")
                .attr("id", function() {
                        // update the class name
                        var class_name = options.node_class_name_list;
                        var class_position = options.class_names;
                        if (this_node.node_is_primary) {
                            class_name[class_position.node_type] = 'p';
                        } else if (this_node.node_type == "metabolite" ){
                            class_name[class_position.node_type] = 's';
                        } else {
                            class_name[class_position.node_type] = 'n';
                        }
                        return "n" + node + "-" + class_name.join('-');
                    })
                .attr("class", function () {
                    return "metabolite-node"; //"n" + node;
                })
                .attr("cx", this_node.x)
                .attr("cy", this_node.y)
                .attr("r", function () {
                    if (this_node.node_is_primary) {
                        return node_radius_primary;
                    } else {
                        return node_radius_secondary;
                    }
                })
                .attr("stroke-width", options.node_stroke_width)
                .attr("stroke", function () {
                    if (this_node.node_type == "metabolite") {
                        return node_metabolite_stroke;
                    } else {
                        return node_other_stroke;
                    }
                })
                .attr("opacity", node_opacity)
                .attr("fill", function () {
                    if (this_node.node_type == "metabolite") {
                        return node_metabolite_colour;
                    } else {
                        return node_other_colour;
                    }
                })
                .on("mouseover", function () {
                    /* Change radius back to normal if it has been altered*/
                    var radius = d3.select(this).attr("r");
                    var nodeID = d3.select(this).attr("id");
                    var node = nodeID.substr(1); // Remove the n from the start
                    if (radius == 3 * node_radius_primary ||
                            radius == 3 * node_radius_secondary) {
                        d3.select(this).attr("r", radius / 3);
                        if (map_node_data[node].node_type == "metabolite") {
                            d3.select(this).attr("fill", node_metabolite_colour);
                        } else {
                            d3.select(this).attr("fill", node_other_colour);
                        }
                    }
                })
                .on("click", function() {
                    var node_id = d3.select(this).attr("id");
                    var node_id = node_id.substr(1).split("-")[0];
                    update_modal(node_id, false);
                });
       }
};



/**
 * Helper function for getting the position of the div
 */
function getOffset( el ) {
    var _x = 0;
    var _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return { top: _y, left: _x };
}

var x = getOffset( document.getElementById('yourElId') ).left;


/**
 * Update the inner html of the modal.
 */
var update_modal = function(node_id, add_graph) {
    // Want to make the info tab visible on the side bar
    make_sidebar_section_visible('metabolite-info');
    // Need to strip the bigg to the universal bigg id
    var map_node_data = pathway.data.pathway.nodes;
    var node = map_node_data[node_id];
    var bigg_id = node.bigg_id.slice(0, -2);
    var chebi_id = node.chebi_id;
    var name = bigg_id; // Set default incase the info doesn't contain a name
    var metabolite_info = pathway.data.metabolite_info[chebi_id];

    // Need to update the inner HTML of the respective componenets
    for (var m = 0; m < options.molecule_display_info.length; m ++) {
        var info_name = options.molecule_display_info[m];
        var mol_info = metabolite_info[info_name];
        if (info_name == 'info') {
            for (var i in mol_info) {           
                document.getElementById(i).innerHTML = mol_info[i];
            }
        } else {
            document.getElementById(info_name).innerHTML = mol_info;
        }
    }
    // Update the title of the modal
    document.getElementById('molecule-modal-title').innerHTML = name;
    // Add the metabolite list info if there is info available
    add_metabolite_list_info_to_modal(node_id);
    // We want to see if there is a graph associated with this node
    var specific_bigg_id = node.bigg_id;
    // Clear the old graph out of the div
    var div_id = options.modal.graph_div_id;
    $('#' + div_id).empty();
    if (node != undefined && add_graph == true) {
        // Need to save the old x and y and create new x and y for the graph
        var old_x = node.x;
        var old_y = node.y;
        var new_coords = getOffset( document.getElementById(div_id));
        node.x = (new_coords.left + options.modal.graph_outer_circle_radius)/2 + 50;
        node.y = (new_coords.top + options.modal.graph_outer_circle_radius)/2;

        var modal_svg = d3.select("#" + div_id).append("svg")
            .attr("id", "modal-graphSVG")
            .attr("class", "svg-graph")
            .attr("width", options.modal.graph_outer_circle_radius * 2.5 + 10)
            .attr("height", options.modal.graph_outer_circle_radius * 2 + 10);
        
        // Update the statistical test to none so we don't re perform the log 
        // opperation done at the start.
        graph_options.data.statistical_test = "log2";
        draw_graph(node, modal_svg, true);
        node.x = old_x;
        node.y = old_y;
    } else {
        // otherwise we want to display the chemical structure of the metabolite
        var img_link = "http://escher.github.io/escher-demo/structures/structure_imgs/" + bigg_id + ".png"
        var elem = document.createElement("img");
        elem.setAttribute("src", img_link);
        elem.setAttribute("class", "chem-img")
        elem.setAttribute("alt", "No Chemical structure available");
        document.getElementById(div_id).appendChild(elem); //= "<image href='" + img_link + "' alt='no structure' style='width:200px;'></image>" 
    }
}


/**
 * Creates the text for the node
 */
var create_node_text = function (node, group, node_data, options) {
    var text_colour = options.text_colour;
    var text_stroke = options.text_stroke;
    var text_size = options.text_size;

    var text = group.append("text")
            .attr("class", "nodetext")
            .attr("id", function () {
                return "t" + node;
            })
            .attr("x", node_data[node].label_x)
            .attr("y", node_data[node].label_y)
            .attr("fill", text_colour)
            .attr("stroke-width", text_stroke)
            .style("font-size", text_size)
            .style("font-family", "sans-serif")
            .text(node_data[node].bigg_id)
            .attr("opacity", 1) // Set default opacity to be 0
            .on("mouseover", function () {
                var id = d3.select(this).attr("id");
                var text = d3.select(this).text();
                d3.select(this).text(id);
                d3.select(this).attr("id", text);
            })
            .on("mouseout", function () {
                var id = d3.select(this).attr("id");
                var text = d3.select(this).text();
                d3.select(this).text(id);
                d3.select(this).attr("id", text);
            });
};



/**
 * Creates the node group which gets the circle and text added to it
 */
var create_group = function (node, node_data, options) {
    var svg = options.svg;
    var group = svg.append("g")
            .attr("class", "node")
            .attr("id", "group"+ node);

    create_node_circle(node, group, node_data, options);
    create_node_text(node, group, node_data, options);

};



/**
 * Creates the nodes.
 */
var create_nodes = function (node_data, options) {
    // Sets up the graph options based on the map data
    for (var node in node_data) {
        create_group(node, node_data, options);
        node_data[node].node_id = node;
    }
    return options;
};


/* Creates the for the paths */
var line_function = d3.svg.line()
        .x(function (d) {
            return d.x;
        })
        .y(function (d) {
            return d.y;
        })
        .interpolate("bundle");

d3.selection.prototype.moveToBack = function () {
    return this.each(function () {
        var firstChild = this.parentNode.firstChild;
        if (firstChild) {
            this.parentNode.insertBefore(this, firstChild);
        }
    });
};



/**
 * Creates the reaction dict. Pulls info out of the reaction which 
 * is relevant
 */
var create_reaction_info = function(reaction) {
    var reaction_info = {};
    reaction_info['bigg_id'] = reaction.bigg_id;
    reaction_info['label_x'] = reaction.label_x;
    reaction_info['label_y'] = reaction.label_y;
    reaction_info['x'] = reaction.label_x;
    reaction_info['y'] = reaction.label_y
    reaction_info['name'] = reaction.name;
    reaction_info['gene_reaction_rule'] = reaction.gene_reaction_rule;
    reaction_info['is_node_primary'] = true;
    reaction_info['node_type'] = 'gene';

    return reaction_info;
}



/**
 * Creates the reactions
 */
var create_reactions = function (reaction_data, options) {
    var reaction_stroke_width = options.reaction_stroke_width;
    var reaction_count = options.reaction_count;
    var reaction_stroke_opactity = options.reaction_stroke_opactity;
    var reaction_stroke = options.reaction_stroke;
    var map_node_data = options.data.pathway.nodes;
    var node_to;
    var node_from;
    var svg = options.svg;
    // Data storage maps
    var gene_reaction_map = {}
    var reaction_gene_map = {}

    // Want to store the information for each reaction, to IDs from a 
    // particular node so we can hide, display or change it
    //options.stored.reaction_info = reaction_data; // Want to just store everything

    for (var reaction in reaction_data) {
        create_group(reaction, reaction_data, options);
        // We want to store the position of the reaction and some data
        // so that we can easily create a graph of the gene expression data
        reaction_gene_map[reaction] = create_reaction_info(reaction_data[reaction]);
        for (var g in reaction_data[reaction].genes) {
            var gene = reaction_data[reaction].genes[g];
            // We want to make a mapping from genes to reactions so that
            // when reaction data is overlayed we can easily map back to 
            // a reaction that it effects.
            var gene_info = gene_reaction_map[gene.bigg_id];
            // If we don't have an array of genes make one
            if (gene_info == undefined) {
                gene_info = {};
                gene_info['name'] = gene.name;
                var reactions = [];
            } else {
                var reactions =  gene_reaction_map[gene.bigg_id]['reactions'];
            }
            reactions.push(reaction);
            gene_info['reactions'] = reactions;
            gene_reaction_map[gene.bigg_id] = gene_info;
        }

        for (var metabolite in reaction_data[reaction].metabolites) {

        }
        for (var section in reaction_data[reaction].segments) {
            var segment = parseInt(section);
            var line_points = new Array();
            var connect_node_points = new Array();
            for (var line_segment in reaction_data[reaction].segments[segment]) {
                var this_node = reaction_data[reaction].segments[segment][line_segment];
                if (line_segment == "from_node_id" || line_segment == "to_node_id") {
                    var tmp = {};
                    tmp.x = map_node_data[this_node].x; 
                    tmp.y = map_node_data[this_node].y
                    connect_node_points.push(tmp);
                    if (line_segment == "from_node_id") {
                        line_points.push(tmp);
                        node_from = this_node;
                    }
                    if (line_segment == "to_node_id") {
                        node_to = this_node;
                    }
                } else {
                    var tmp = {};
                    var b1 = this_node;
                    if (b1 !== null) {
                        tmp.x = this_node.x;
                        tmp.y = this_node.y;
                        line_points.push(tmp);
                    }
                }
            }
            reaction_count++;
            line_points.push(connect_node_points[1]);
            var line_segment_1 = svg.append("path")
                    .attr("d", line_function(line_points))
                    .attr("class", function () {
                        var id_var = "r" +reaction;// + "f" + node_from + 't' + node_to;
                        return id_var;
                    })
                    .attr("stroke-width", reaction_stroke_width)
                    .attr("stroke", reaction_stroke)
                    .attr("opacity", reaction_stroke_opactity)
                    .attr("fill", "none")
                    //.on("click", remove_reaction)
                    .attr("marker-end", function () {
                        if (map_node_data[node_to].node_type == "metabolite") {
                            return "url(#triangle-end)";
                        }   
                        // Want to also store the (to's) for each metabolite 
                        // this way when we upload data we can quickly see which 
                        // reactions are affected
                        if (map_node_data[node_from].node_type == "metabolite") {
                            // There may already be a reaction there so try to get the array
                            var reaction_array = map_node_data[node_from]['reactions'];
                            if (reaction_array == undefined) {
                                reaction_array = new Array();
                            }
                            var temp_to_reaction = {};
                            // Store the reaction (so we can access all places where it occurs 
                            // but as the 'node_to' node)
                            temp_to_reaction['node_to'] = node_to;
                            temp_to_reaction['reaction_id'] = reaction;
                            temp_to_reaction['data'] = reaction_data[reaction];
                            reaction_array.push(temp_to_reaction);
                            map_node_data[node_from]['reactions'] = reaction_array;
                        }
                    });


            if (reaction_data[reaction].reversibility == true) {
                line_points.reverse();
                var line_segment_2 = svg.append("path")
                        .attr("d", line_function(line_points))
                        .attr("class", function () {
                            var id_var =  "r" + reaction;//+ "f" + node_to + 't' + node_from;
                            return id_var;
                        })
                        .attr("stroke-width", reaction_stroke_width)
                        .attr("stroke", reaction_stroke)
                        .attr("opacity", reaction_stroke_opactity)
                        .attr("fill", "none")
                        ///.on("click", remove_reaction)
                        .attr("marker-end", function () {
                            if (map_node_data[node_from].node_type == "metabolite") {
                                return "url(#triangle-end)";
                            }
                        });

                line_segment_2.moveToBack();
            }

            line_segment_1.moveToBack();
        }

    }
    ;
    // Add the maps to the options datastructure
    options['gene_reaction_map'] = gene_reaction_map;
    options['reaction_gene_map'] = reaction_gene_map;
};



var create_labels = function (label_data, options) {
    var label_text_stroke = options.label_text_stroke;
    var label_text_size = options.label_text_size;
    var svg = options.svg;
    for (var label in label_data) {
        var group = svg.append("g")
                .attr("class", "label")
                .attr("id", label);

        var text = group.append("text")
                .attr("class", "labeltext")
                .attr("id", function () {
                    return label_data[label].text;
                })
                .attr("fill", options.text_colour)
                .attr("x", label_data[label].x)
                .attr("y", label_data[label].y)
                .attr("stroke-width", label_text_stroke)
                .style("font-size", label_text_size)
                .style("font-family", "sans-serif")
                //.on("click", remove_element)
                .text(label_data[label].text);
    }
};


