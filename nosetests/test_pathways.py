from pathway_view.config import *

from pathway_view.model.auth import AuthModel
from pathway_view.model.pathways import PathwayModel
from shutil import copyfile

# test pathway path
test_path = "pathway_view/test_data/pathways/test.JSON"

def test_pathways():
    # Test the non uploading part

    # Add some user and groupu to be the owners/user performing this
    auth = AuthModel()
    full_name = "New user"
    email = "newuser@gmail.com"
    auth.full_name = full_name
    auth.email = email
    user = auth.add_new_user()
    uid = int(user[ID])
    auth.role = 'admin' # set the user to be admin
    auth.uid = uid
 
    # Create the group
    name = 'some groups'
    group = auth.create_group(name)
    gid = group[ID]

    print('Testing bad metadata')

    # Test with bad metadata

    path = PathwayModel()
    path.name = "some name"
    path.u_file = None
    path.u_filename = "test"
    path.private = 't' #True
    path.owner_type = 'user'
    path.owner_id = 'notnum'
    path.notes = None
    path.path = "pathway_view/uploads/pathways/test.JSON" 

    #assert path.parse_pathdata() == "There was an error with uploaded data."
        
    path = PathwayModel()
    path.name = "A test file"
    path.u_filename = "test"
    path.private = 't'
    path.owner_type = 'user'
    path.owner_id = uid
    path.notes = 'some notes'
    path.validated = 'f'
    path.path = "pathway_view/uploads/pathways/test.JSON"
    path.error = False
    # Copy the file to the destination
    copyfile(test_path, path.path)

    # Open a file and close it to create a test file
    open(path.path, 'a').close()

    print('----------------------')

    print('Test: saving metadata.')

    pathway = path.add_pathway(uid)
    path_id = path.path_id
    assert isinstance(path_id, int) == True
    #print("path_id", path_id)

    print('--------------------------')

    print('Test: retrieving a dataset')
    pathway = path.get_pathway(path_id)

    # Spreadsheets should have a length of 2 (data and metadata)
    assert len(pathway) == 2
    #print("length pathway = 2", len(pathway))

    print("--------------------------")

    print('Test: getting datasets belonging to a user')
    # Add a new spreadsheet to a group the user is part of
    # Set up environment
    ret = auth.add_user_to_group(email, name)

    # Add a "new" spreadsheet for that group
    group_path = PathwayModel()
    group_path.name = "group sheet"
    group_path.u_filename = "group test"
    group_path.private = 'f'
    group_path.owner_type = 'group'
    group_path.owner_id = gid
    group_path.notes = 'group notes'
    group_path.validated = 'f'
    # Need to add a file there
    group_path.path = 'pathway_view/uploads/pathways/somethings.JSON'
    
    # Open a file and close it to make a file to test on
    open(group_path.path, 'a').close()

    group_pathway = group_path.add_pathway(uid)
    group_path_id = group_path.path_id

    all_paths = group_path.get_all_pathways_for_user(uid)    

    #print("all paths", all_paths)
    assert all_paths != None

    print("------------------------")

    print('Test: deleting pathway.')
    
    success = path.delete_pathway(group_path_id)
    success = path.delete_pathway(path_id)

    #print("success=", success)
    assert success == "success"
    
    auth.error = None

    # Clean up the environment
    group = auth.delete_group(int(gid))
    assert auth.error == None
    #print("group deleted", group)

    success = auth.delete_user(int(uid))
    assert auth.error == None    
    #print("auth error", auth.error)

    
test_pathways()

   
