from pathway_view.config import *
import csv
import json


"""

Script to determine the accuracy of mapping between the internal 
identifiers and manually chosen identifiers.

"""

def get_ids_from_file(file_path):
    """
    Given a selected file, adds the ids to a list to get
    neutral chebi ids from later on.
    Parameter:
        file_path: path to the file with the ids
                    id1 in col0 -> chebi_id in test case
                    id2 in col1 -> hmdb in test case

    Returns:
        ids: a list of all ids (from both columns)
        col0_id_dict: the ids from col0 as a dictionary mapping to the
            ids from col1
        col1_id_dict: the ids from col1 as a dictionary mapping to the
            ids from col0 (i.e. from hmdb to chebi)
    """
    ids = []
    col1_id_dict = {}
    col0_id_dict = {}
    # Keep track of the lines which had duplicates so we can print them one after eachother
    line_dict = {}
    # keep track of the duplicates and write these to files
    dup_file_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/pera/dups.tsv"
    output = open(dup_file_path, 'w')
    output = csv.writer(output, delimiter='\t')
    
    # keep reference between ids
    with open(file_path, 'r') as filein:
        filein = csv.reader(filein, delimiter='\t')
        for line in filein:
            # check if we already have that id
            clean_id0 = line[0].strip().lower()
            clean_id1 = line[1].strip().lower()
            if clean_id0 in ids or clean_id1 in ids:
                output.writerow(line_dict[clean_id1])
                output.writerow(line)

            line_dict[clean_id1] = line

            ids.append(clean_id0)
            ids.append(clean_id1)
            col0_id_dict[clean_id0] = clean_id1
            col1_id_dict[clean_id1] = clean_id0

    return [ids, col0_id_dict, col1_id_dict, line_dict]



def parse_bigg_dict():
    """
    Parses the BiGG library which is used to map on the front end between
    chebi ids and bigg ids.

    The BiGG library uses the universal BiGG identifier as the key
    so we want to translate this to having the neutral ChEBI as the key.

    """
    path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/public/data/bigg_mapping_dict_full.json"
    bigg_as_str = open(path, 'r').read()
    bigg_dict = json.loads(bigg_as_str)
    chebi_to_bigg = {}
    for bigg in bigg_dict:
        chebi_id = bigg_dict[bigg]['Neutral ChEBI ID'].split(":")[1];
        chebi_to_bigg[chebi_id] = bigg_dict[bigg]

    return chebi_to_bigg



def check_neutral_chebis_are_same_for_two_files(get_from_json, file_path):
    """
    With two files as inputs this script checks to see how many of the identifiers can be
    mapped to a bigg id in a pathway.
    
    Parameters:
        get_from_json: a boolean so either true or false, if we have already performed 
                the join this has been dumped to a file so we can quickly access it. 
                Otherwise we perform a SQL statement to merge the IDs on the neutral
                chebi identifier and the mappings in the table chebi_mapping_to_other_id.

        file_path: the path to the file containing the different identifiers, it is
                a file of type TSV with the chebi_id in col0 and the hmdb in col1

    Returns:
        Nothing rather outputs to a TSV file the results from the test.
    """

    path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/from_merged_inchi/inchi_merged.json"
    save_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/pera/pera_hmdb_chebi_results_v_join_new_mappings.tsv"
    json_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/public/data/ouput_from_join_updated.json"
    output_of_unmapped = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/pera/unmapped_hmdb_ids.tsv"
    output_unmapped = open(output_of_unmapped, 'w')
    output_unmapped = csv.writer(output_unmapped, delimiter='\t')

    ids_info = get_ids_from_file(file_path)
    id_list = tuple(ids_info[0])
    chebi_id_dict = ids_info[1]
    hmdb_id_dict = ids_info[2]
    line_dict = ids_info[3]

    if get_from_json == True:
        neutral_ids_str = open(json_path, 'r').read()
        neutral_chebi_ids = json.loads(neutral_ids_str) 
    else:
        # now we have ids want to get the neutral chebi for both of these
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = "SELECT a.chebi_id, a.other_id, a.other_id_type, b.other_id from chebi_mapping_to_other_id as a LEFT JOIN chebi_mapping_to_other_id as b on (a.chebi_id = b.chebi_id) WHERE a.other_id in %s AND b.other_id_type = 'bigg_id';"
        cursor.execute(sql,((id_list),),)
        neutral_chebi_ids = cursor.fetchall()
        """
        save to json so we don't have to do this again
        """
        with open(json_path, 'w') as fp:
            json.dump(neutral_chebi_ids, fp)
    
    mapping_dict_hmdb = {}
    mapping_dict_chebi = {}
    mapping_dict_to_bigg = {}

    # Go through and check the other ids match
    # Store the reference to the BiGG ID in a separate dictionary
    for val in neutral_chebi_ids:
        chebi_id = val[0]
        other_id = val[1]
        other_id_type = val[2]
        mapping_dict_to_bigg[other_id] = val[3]
        if other_id_type == 'inchi_hmdb': # or other_id_type == 'hmdb':
            mapping_dict_hmdb[other_id] = chebi_id
        elif other_id_type == 'chebi_id':
            mapping_dict_chebi[other_id] = chebi_id

    print(len(id_list), len(chebi_id_dict), len(hmdb_id_dict), len(neutral_chebi_ids), len(mapping_dict_hmdb), len(mapping_dict_chebi))

    output = open(save_path, 'w')
    output = csv.writer(output, delimiter='\t')
    output.writerow(['hmdb', 'chebi_id_from_ma', 'bigg_id_chebi_from_ma', 'neutral_chebi_id_hmdb', 'bigg_id_hmdb', 'neutral_chebi_id_from_ma_chebi_id', 'same_neutral'])
    # Merge on the id_dict created when we made the file and 
    # output to tsv file
    # Keep track of the found chebi_ids so we can add the ones which weren't able to be mapped as well.
    added_chebis = []

    """
    Keep track of the counts of how many ids were found with pathways:
        counts[0] = how many of the MA chebi ids were found in pathways
        counts[1] = how many of the MA chebi ids once mapped to a neutral id were
                    found in the pathways
        counts[2] = how many of the MA hmdb ids once mapped to a neutral chebi id
                    were found in the pathways

    """
    counts = {0: 0, 1: 0}
    not_found_hmdb = []

    for hmdb in mapping_dict_hmdb:
        chebi_id = mapping_dict_hmdb[hmdb]
        ma_chebi_id = hmdb_id_dict[hmdb]
        same_neutral = "false"
        bigg_id_chebi_from_ma = ""
        bigg_id_hmdb = mapping_dict_to_bigg[hmdb]
        counts[1] += 1
        try:
            neutral_from_ma_chebi = mapping_dict_chebi[ma_chebi_id]
            bigg_id_chebi_from_ma = mapping_dict_to_bigg[ma_chebi_id]
            if bigg_id_chebi_from_ma:
                counts[0] += 1
            # Add this so we don't get a duplicate row
            added_chebis.append(ma_chebi_id)
        except:
            neutral_from_ma_chebi = "chebi_not_mapped"
            same_neutral = "false"

        if neutral_from_ma_chebi == chebi_id:
            same_neutral = "true"

        # Find the bigg pathways and write a row
        #ret = find_bigg_pathways(output, hmdb, ma_chebi_id, neutral_from_ma_chebi, chebi_id, same_neutral, counts, not_found_hmdb)
        output.writerow([hmdb, ma_chebi_id, bigg_id_chebi_from_ma, chebi_id, bigg_id_hmdb, neutral_from_ma_chebi, same_neutral])

        #counts = ret[0]
        #not_found_hmdb = ret[1]

    
    for ma_chebi_id in mapping_dict_chebi:
        neutral_chebi_from_ma = mapping_dict_chebi[ma_chebi_id]
        hmdb = chebi_id_dict[ma_chebi_id]
        # if this hasn't been added already then we know that 
        # it wasn't able to be mapped on hmdb
        if ma_chebi_id not in added_chebis:
            counts[0] += 1
            bigg_id_hmdb = ""
            bigg_id_chebi_from_ma = mapping_dict_to_bigg[ma_chebi_id]
            same_neutral = "false"
            output.writerow([hmdb, ma_chebi_id, bigg_id_chebi_from_ma, "hmdb_not_mapped", bigg_id_hmdb, neutral_chebi_from_ma, same_neutral])

            #find_bigg_pathways(output, hmdb, ma_chebi_id, neutral_from_ma_chebi, "hmdb_not_mapped", "false", counts)

    
    """
    Add a row for each metric
    """
    
    print(len(added_chebis))
    print(counts)

    output.writerow(["Input data metrics:"])
    output.writerow(["Total number ids (not unique, hmdb and chebi)", len(id_list)])
    output.writerow(["Number of unique  chebi ids", len(chebi_id_dict)])
    output.writerow(["Number of unique  hmdb ids", len(hmdb_id_dict)])

    output.writerow(["Output data metrics:"])
    output.writerow(["Number of MA chebi ids which were able to be mapped", len(mapping_dict_chebi)])
    output.writerow(["Number of MA hmdb ids which were able to be mapped", len(mapping_dict_hmdb)])
    output.writerow(["Number of MA chebi ids once mapped to a neutral id were found in the pathways", counts[0]])
    output.writerow(["Number of MA hmdb ids once mapped to a neutral chebi id were found in the pathways", counts[1]])




    # 
    # Any which weren't found we want to add a row for
    
    for hmdb in hmdb_id_dict:
        chebi = hmdb_id_dict[hmdb]
        try:
            found = mapping_dict_hmdb[hmdb]
        except:
            output_unmapped.writerow(line_dict[hmdb])
            
    """
    for chebi in chebi_id_dict:
        hmdb = chebi_id_dict[chebi]
        try:
            found = mapping_dict_chebi[chebi]
        except:
            output.writerow([hmdb, chebi, "", "not_mapped_on_chebi", ""])
            print(hmdb, chebi)
    """

def find_bigg_pathways(output, hmdb, ma_chebi_id, neutral_from_ma_chebi, chebi_id, same_neutral, counts):
        """
        Need to try find if there are pathways.
        Surround with try catch statement as otherwise will throw an error.
        Want the pathways for three chebi ids:
            1. chebi id found manually by ma (bigg_id_chebi_from_ma)
            2. neutral chebi id mapped with omicxview from (1) neutral_chebi_id_from_ma_chebi_id
            3. neutral chebi id mapped with omicxview from MA hmdb id (bigg_id_hmdb)
    
        Keep track of how many of each of the chebi id types were able to be
        mapped to pathways from Escher.
        Return:
            counts:
                counts[0] = how many of the MA chebi ids were found in pathways
                counts[1] = how many of the MA chebi ids once mapped to a neutral id were
                            found in the pathways
                counts[2] = how many of the MA hmdb ids once mapped to a neutral chebi id
                            were found in the pathways

        SELECT a.chebi_id, a.other_id, a.other_id_type from chebi_mapping_to_other_id as a LEFT JOIN chebi_mapping_to_other_id as b on (a.chebi_id = b.chebi_id) WHERE a.other_id = 'hmdb01401' AND b.other_id_type = 'bigg_id';

        """
        bigg_id_key = 'Universal BiGG ID'
        bigg_pathway_key = 'BiGG Pathways'

        try:
            bigg_data_chebi_from_ma = chebi_bigg_dict[ma_chebi_id]
            bigg_id_chebi_from_ma = ', '.join(bigg_data_chebi_from_ma)
            pathways_chebi_from_ma = "" #', '.join(bigg_data_chebi_from_ma[bigg_pathway_key])
            counts[0] += 1
        except:
            bigg_id_chebi_from_ma = ""
            pathways_chebi_from_ma = ""

        try:
            bigg_data_chebi_from_ma_neutral = chebi_bigg_dict[neutral_from_ma_chebi]
            bigg_id_chebi_from_ma_neutral = ', '.join(bigg_data_chebi_from_ma)
            pathways_chebi_from_ma_neutral = "" #', '.join(bigg_data_chebi_from_ma[bigg_pathway_key])
            counts[1] += 1

        except:
            bigg_id_chebi_from_ma_neutral = ""
            pathways_chebi_from_ma_neutral = ""

        try:
            bigg_data_hmdb = chebi_bigg_dict[chebi_id]
            bigg_id_hmdb = ', '.join(bigg_data_chebi_from_ma) #[bigg_id_key]
            pathways_hmdb = "" #''.join(bigg_data_chebi_from_ma[bigg_pathway_key])
            counts[2] += 1
        
        except:
            not_found_hmdb.append(hmdb)
            bigg_id_hmdb = ""
            pathways_hmdb = ""


        output.writerow([hmdb, ma_chebi_id, bigg_id_chebi_from_ma, pathways_chebi_from_ma, chebi_id, bigg_id_hmdb, pathways_hmdb, neutral_from_ma_chebi, bigg_id_chebi_from_ma_neutral, pathways_chebi_from_ma_neutral, same_neutral])

        return [counts, not_found_hmdb]

file_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/pera/pera_data_standards.tsv"
chebi_bigg_dict = parse_bigg_dict()
check_neutral_chebis_are_same_for_two_files(False, file_path)
