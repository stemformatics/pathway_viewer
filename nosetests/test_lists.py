from pathway_view.config import *

from pathway_view.model.metabolite_lists import MetaboliteListModel
from pathway_view.model.auth import AuthModel
from pathway_view.model.mapping import MappingModel
from shutil import copyfile

test_list_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/test_data/lists/ma_names.tsv"


def test_lists():
    # set up the environment
    auth = AuthModel()
    full_name = "New user"
    email = "newuser@gmail.com"
    auth.full_name = full_name
    auth.email = email
    user = auth.add_new_user()
    uid = int(user[ID])
    auth.role = 'admin' 
    auth.uid = uid

    # Create a group
    name = 'some groups'
    group = auth.create_group(name)
    gid = group[ID]

    list_m = MetaboliteListModel()
    
    list_m.path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/uploads/lists/test.tsv"
    # copy the test file to the destination
    copyfile(test_list_path, list_m.path)
    
    # Fill in the things we would normally get from the users upload
    list_m.name = "alist"
    list_m.private = 't'
    list_m.owner_type = 'user'
    list_m.owner_id = uid
    list_m.raw_metabolite_id_type = 'name'
    list_m.notes = 'some notes'
    list_m.list_type = 'metabolite'
    list_m.col_dict = {'t_statistic': 4, 'p_value': 2, 'z_score':  3, 'name': 0, 'raw_metabolite_id': 0, 'bh_adjusted_p_value': None, 'fold_change': None, 'standard_error': None, 'comments': None}

    
    # For test.tsv
    #{'t_statistic': 5, 'p_value': 6, 'z_score': None, 'name': 1, 'uniq_id': 2, 'bh_adjusted_p_value': 7, 'fold_change': 8, 'standard_error': 9, 'comments': None}
    
    print("Test parsing metabolite list metadata:")

    success = list_m.parse_list_metadata()

    print("success", success)
    #assert "success" == success

    # Normally would save the list data to the server at this point
    # as we are using a test file go straight to adding the raw metadata to the db

    print('-------------------------------------')

    print("Test process metadata and add to db:")

    m_list = list_m.process_metabolite_list_metadata()
    list_id = m_list[ID]
    print('list id', m_list[ID])
    #assert isinstance(m_list[ID], int) == True

    print("--------------------------------------")
    
    print('Test process the raw data and add to db:')

    d_list = list_m.process_raw_metabolite_list_data()
    
    print('success', d_list)
    
    #assert d_list == 'success'

    print('---------------------------------------')
    
    print('Test get processed metabolite list:')
    
    m_list = list_m.get_processed_metabolite_list(list_id, uid)

    #print('processed list', m_list)

    # Want to get the mapping between the identifiers
    map_model = MappingModel()
    metabolite_list_info = map_model.get_mapping_metabolite_list_info(list_id, uid)

    print(metabolite_list_info)
    #assert m_list != None

    print('--------------------------------------')
    
    print('Test get all metabolite lists for a user:')


    list_g = MetaboliteListModel()

    list_g.path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/uploads/lists/test_group.tsv"

    # copy to that path
    copyfile(test_list_path, list_g.path)
    # Create a new list an add the list to the group created before 
    list_g.name = "list2"
    list_g.private = 't'
    list_g.owner_type = 'group'
    list_g.group_name = group[NAME]
    list_g.notes = 'some notes more'
    list_g.list_type = 'metabolite'
    list_g.col_dict =  {'t_statistic': 4, 'p_value': 2, 'z_score':  3, 'name': 0, 'raw_metabolite_id': 0, 'bh_adjusted_p_value': None, 'fold_change': None, 'standard_error': None, 'comments': None}

    # {'t_statistic': 5, 'p_value': 6, 'z_score': None, 'name': 1, 'uniq_id': 2, 'bh_adjusted_p_value': 7, 'fold_change': 8, 'standard_error': 9, 'comments': None}

    # Have to re add as a group list
    success = list_g.parse_list_metadata()

    print("success:=", success)
    #assert "success" == success
    print('-------------------------------------')

    print("Test group process metadata and add to db:")

    g_list = list_g.process_metabolite_list_metadata()
    list_id_grp = g_list[ID]

    print('list id', g_list[ID])
    #assert isinstance(g_list[ID], int) == True

    print("--------------------------------------")

    print('Test group process the raw data and add to db:')

    d_list = list_m.process_raw_metabolite_list_data()

    # print('success', d_list)

    #assert d_list == 'success'

    lists = list_m.get_all_processed_metabolite_lists_for_user(uid)

    print('all users lists', lists, isinstance(lists, str))

    #assert isinstance(lists, str) != True

    print('---------------------------------------')

    print('Test deleting metabolite list:')

    success = list_m.delete_metabolite_list(list_id, uid)

    print('success', success)
    
    #assert 'success' == success

    success = list_m.delete_metabolite_list(list_id_grp, uid)

    print('success', success)

    #assert 'success' == success

    group = auth.delete_group(int(gid))
    assert auth.error == None


    success = auth.delete_user(int(uid))
    assert auth.error == None
    

test_lists()    
