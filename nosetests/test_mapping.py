from pathway_view.config import *
from pathway_view.model.mapping import MappingModel
from pathway_view.model.metabolite_lists import MetaboliteListModel

from shutil import copyfile

test_ds_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/bpa_data/HML_MA_20120707.csv"
#pera_data/pera_data_hmdb.tsv



def test_mapping():
    #id_type_list = ['bigg_name', 'inchi_hmdb', 'bigg_id', 'bigg_paths', 'metanetx_id', 'inchi_str', 'hmdb_generic_name', 'hmdb_synonyms']
    id_type_list = ['hmdb_generic_name', 'hmdb_synonyms']

    mp = MappingModel()
    mp.uid = 292
    mp.upload_file = test_ds_path

    success = mp.save_temp_id_list_as_file()

    print(success)

    parsed_file = mp.file_to_id_list(test_ds_path)

    id_mappings = mp.get_id_mapping(parsed_file['parsed_id_list'], id_type_list)

   
    mapping_stats = mp.get_stats_on_mapping(parsed_file['parsed_to_raw_id_dict'], id_mappings, id_type_list)

#    assert mapping_stats['mapped_id_stats'] == {'inchi_hmdb': 62, 'bigg_name': 42, 'Number of unique ids in file': 67, 'metanetx_id': 42, 'Number of ids that were able to be mapped': 62, 'Number of unmapped ids': 5, 'bigg_paths': 42, 'bigg_id': 42, 'inchi_str': 62}
 
    unmapped_ids = mapping_stats['unmapped_ids']
    print(mapping_stats['mapped_id_stats'])
    print(unmapped_ids)
    # IDs which should be in the unmapped IDs
    known_unmapped_ids = ['HMDB00589', 'HMDB00571', 'HMDB00171', 'HMDB01138', 'HMDB04984']

    """
    HMDB00571 has been revoked
    [HMDB00589, HMDB00171, HMDB04984] are secondary accession numbers and not in the SDF download from HMDB
    HMDB01138 can't map on inchi string to chebi 
    """
    
    # Save the mappings, set list model attr 
    # Save to a user and then to a group.
    list_model = MetaboliteListModel()    
    list_model.name = "name11111"
    list_model.private = "true"
    list_model.owner_type = "user"
    list_model.notes = "noeeee"
    list_model.group_name = ""
    list_model.owner_id = 292

    #print(mapping_stats['mapping_dict_with_raw_ids'])
    success = mp.save_mapping_as_metabolite_list(mapping_stats['mapping_dict_with_raw_ids'], list_model)

    print(success)
    for unmapped_id in known_unmapped_ids:
        assert unmapped_id in unmapped_ids
 
test_mapping() 
