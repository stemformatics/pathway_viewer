from pathway_view.config import *

from pathway_view.model.auth import AuthModel


def test_token_gen():
    auth = AuthModel()
    full_name = "New user"
    email = "newuser@gmail.com"
    auth.full_name = full_name
    auth.email = email
    user = auth.add_new_user()
    uid = int(user[ID])
    auth.uid = uid
    auth.role = 'admin'

    tokens = []

    # Generate a token 10 times
    for i in range(10):
        token = auth.generate_login_token(uid)
            
        # Login with token   
        uid2 = auth.get_user_by_token(token)
    
        assert uid == uid2
        
        # Delete the token
        success = auth.delete_token_for_user(uid)
            
        assert success == "success"

        # Try login again with theh token
        uid2 = auth.get_user_by_token(token)

        assert uid2 == "No user attached to token."

        assert (token in tokens) == False

        tokens.append(token)

    # Remove user
    user = auth.delete_user(uid)
    


def test_bad_token():
    auth = AuthModel()
    full_name = "New user"
    email = "newuser@gmail.com"
    auth.full_name = full_name
    auth.email = email

    user = auth.add_new_user()

    uid = int(user[ID])
    auth.uid = uid
    auth.role = 'admin'

    # Try login with None when user has no
    # token.
    uid2 = auth.get_user_by_token(None)
        
    assert uid2 == 'Token was incorrect length.'

    # Try login with '' when user has no token
    uid2 = auth.get_user_by_token('')

    assert uid2 == 'Token was incorrect length.'
    
    # Generate a token for the user
    token = auth.generate_login_token(uid)

    # Login with random numbers
    uid2 = auth.get_user_by_token(1234)

    assert uid2 == 'Token was incorrect length.'

    # Login with string of correct length but not token (32)
    uid2 = auth.get_user_by_token('12345678901234567890123456789012')

    assert uid2 == "No user attached to token."

    # Login with shorter string
    uid2 = auth.get_user_by_token('123456789')

    assert uid2 == 'Token was incorrect length.'

    # actually login
    uid2 = auth.get_user_by_token(token)

    assert uid == uid2

    # Delete the user
    user = auth.delete_user(uid)




def test_add_new_user():
    auth = AuthModel()
    full_name = "Ariane Mora"
    email = "ariane2014m@gmail.com"
    auth.full_name = "Ariane Mora"
    auth.email = "ariane2014m@gmail.com"
    user = auth.add_new_user(full_name, email)
    assert user[NAME] == "Ariane Mora"
    assert user[EMAIL] == "ariane2014m@gmail.com"
    assert user[ROLE] == 'general'
    
    # Now the user has been added we need to change their password
    code = str(user[PASSWORD])
    password = "cakesapiples"
    auth.password = password
    user = auth.register(code)
    assert user[NAME] == full_name
    assert user[EMAIL] == email
    assert user[ROLE] == 'general'

    # Check login success
    user = auth.login()
    assert user[NAME] == full_name
    assert user[EMAIL] == email
    assert user[ROLE] == 'general'


    # Delete the user
    user = delete_user(user[ID])
    assert auth.error == None

def test_add_new_user_failure():
    full_name = "Apple Pie"
    email = "applesandcakes.com"
    auth = AuthModel()
    auth.full_name = full_name
    auth.email = email
    # Not a proper email address (doesn't contain @ symbol)
    bad_email = auth.add_new_user()
    assert auth.error == error_codes['email']

    # Email not a string
    email = 1234
    bad_email = auth.add_new_user()
    assert auth.error == error_codes['email']

    # username exists
    email = "test@test.com"
    dup_email = auth.add_new_user()
    assert auth.error == "Username is already taken"    
'''
    # Password too short
    password = "too"
    short_password = add_new_user(full_name, email, password)
    assert short_password == error_codes['password']

    # Password not a string
    password = 1224282
    int_password = add_new_user(full_name, email, password)
    assert int_password == error_codes['password']
'''


def check_login_failure():
    email = "test@test.com"
    password = "passwordnotright"

    auth = AuthModel()
    auth.email = email
    auth.password = password

    # Invalid password for the user
    uid = auth.login()
    assert auth.error == error_codes['email_or_password']

    # Invalid email
    email = "appleastest.com"
    auth.email = email
    uid = auth.login()
    assert auth.error == error_codes['email']

    # Email not a string
    email = 12348382
    auth.email = email
    uid = auth.login()
    assert auth.error == error_codes['email']

    # User doesn't exist
    email = "apples@eggs.com"
    auth.email = email
    uid = auth.login()
    assert auth.error == error_codes['email_or_password']
    
    # Password not a string    
    auth.password = 12348382
    uid = auth.login()
    assert auth.error == error_codes['password']

    
def check_forgot_user_password_success():
    auth = AuthModel()
    email = "test@test.com"
    auth.email = email
    ret = auth.forgot_user_password()
    assert auth.error == None

def check_forgot_user_password_failure():
    auth = AuthModel()
    # Invalid email
    email = "appleastest.com"
    auth.email = email
    uid = auth.forgot_user_password()
    print(auth.error)
    assert auth.error == error_codes['email']

    # Email not a string
    email = 12348382
    auth.email = email
    uid = auth.forgot_user_password()
    assert auth.error == error_codes['email']

    # No user with email but valid email
    email = "appleas@test.com"
    auth.email = email
    uid = auth.forgot_user_password()
    assert auth.error == error_codes['email']


def check_reset_user_password_success():
    auth = AuthModel()
    email = "test@test.com"
    password = "newpassword"
    auth.email = email
    auth.password = password
    success =  auth.reset_user_password()
    assert auth.error == None


def check_reset_user_password_failure(): 
    email = "test@test.com"
    password = "pass"
    auth = AuthModel()
    auth.email = email
    auth.password = password

    # Too short password
    ret =  auth.reset_user_password()
    assert auth.error == error_codes['password']

    # Password not a string
    password = 1234
    auth.password = password
    ret = auth.reset_user_password()
    assert auth.error == error_codes['password']

    # Invalid email
    email = "applesandcakes"
    auth.email = email
    ret = auth.reset_user_password()
    assert auth.error == error_codes['email']

    # Email not a string
    email = 1234
    auth.email = email
    ret = auth.reset_user_password()
    assert auth.error == error_codes['email']   
    
    # User doesn't exist for email
    email = "fake@fake.com"
    password = "passwrd5"
    auth.password = password
    auth.email = email
    ret = auth.reset_user_password()
    assert auth.error == "Unable to change password for the user"

def test_check_user_by_uid_success():
    uid = 21
    auth = AuthModel()
    user = auth.get_user_by_uid(uid)[0]
    assert user[ID] == 21 #[[21, 'Test User', 'test@test.com', 'password5', 'general']]
    assert user[NAME] == 'Test User'
    assert user[EMAIL] == 'test@test.com'
    assert user[ROLE] == 'general'

def test_check_user_by_uid_faliure():
    # Uid doesn't exist
    auth = AuthModel()
    uid = 22
    user = auth.get_user_by_uid(uid)
    assert user == None

    # Uid not int
    uid = "eggs"
    user = auth.get_user_by_uid(uid)
    assert user == None

    # Uid < 0
    uid = -10
    user = auth.get_user_by_uid(uid)
    assert user == None


def check_create_group_success():
    uid = 61
    role = 'admin'
    name = 'a name'
    auth = AuthModel()
    auth.error = None
    auth.role = role
    auth.uid = uid
    group = auth.create_group(name)
    print(group, auth.error) 
    assert auth.error == None
    # check deleting a group
    group = auth.delete_group(group[ID])
    assert auth.error == None

 
def check_create_group_failure():
    # Duplicate group name
    uid = 22
    name = 'test group'
    role = 'admin'
    auth = AuthModel()
    auth.role = role
    auth.uid = uid
    group = auth.create_group(name)
    print(auth.error)
    assert auth.error == "A group already exists with that name, please enter a new group name"

    # Name with bad characters
    name = "(kasjh1"
    group = auth.create_group(name)
    assert auth.error ==  error_codes['name']

    # Name not a string
    name = 1299
    group = auth.create_group(name)
    assert auth.error ==  error_codes['name']

    # User with not admin privileges
    role = 'general'
    auth.role = role
    group = auth.create_group(name)
    assert auth.error == error_codes['privileges']

    # Uid error
    uid = '123'
    auth.uid = uid
    group = auth.create_group(name)
    assert auth.error == error_codes['id']

    # Not a positive int
    uid = -129
    auth.uid = uid
    group = auth.create_group(name)
    assert auth.error == error_codes['id']


def check_get_group_by_name_success():
    auth = AuthModel()
    name = 'test group'
    group = auth.get_group_by_name(name)
    assert group == [6, 'test group']


def check_get_group_by_name_failure():
    auth = AuthModel()
    # Name with bad characters
    name = "(kasjh1"
    group = auth.get_group_by_name(name)
    assert group == None
    assert auth.error == error_codes['name']

    # Name not a string
    name = 1299
    group = auth.get_group_by_name(name)
    assert group == None
   
    # Getting a group which has a valid name but doesn't exist
    name = "valid name"
    group = auth.get_group_by_name(name)
    assert group == None 


def check_add_user_to_group_success():
    auth = AuthModel()
    # User to be added
    email = 'admmin@admin.com'
    # uid of adder
    auth.uid = 61
    # Role of adder
    auth.role = 'admin'
    # Group name to add user to
    name = 'test group'

    group = auth.remove_user_from_group(email, name)
    print("eggs", group)
    print(auth.error) 
    group = auth.add_user_to_group(email, name)

    print(group)
    print(auth.error)
    assert auth.error == None

def check_add_user_to_group_failure():
    # Bad user id
    auth = AuthModel()
    auth.uid = -10

    email = 'testy'

    auth.role = 'egg'

    name = 123

    group = auth.add_user_to_group(email, name)
    print(auth.error)
    assert auth.error == error_codes['id']

    # non int uid
    auth.uid = 'eggs'
    group = auth.add_user_to_group(email, name)
    assert auth.error == error_codes['id']

    # Role not valid
    auth.uid = 61
    group = auth.add_user_to_group(email, name)
    assert auth.error == error_codes['privileges']

    
    # Role not string
    auth.role = 123
    group = auth.add_user_to_group(email, name)
    print(group, auth.error)
    assert auth.error == error_codes['privileges']
    
    # name not a string
    auth.role = 'admin'
    email = 'admmin@admin.com'   
    group = auth.add_user_to_group(email, name) 
    assert auth.error ==  "The group " + str(name) + " doesn't exist. Please check the name and try again"

    # bad email
    name = 'test group'
    email = 'eggs'
    group = auth.add_user_to_group(email, name)
    assert auth.error == "Error in users' email: " + str(email)

    # email not a string
    email = 1234
    group = auth.add_user_to_group(email, name)
    assert auth.error == "Error in users' email: " + str(email)

    # Group doesn't exist
    email = 'test@test.com'
    name = 'eggs' 
    group = auth.add_user_to_group(email, name)
 
    assert auth.error == "The group " + str(name) + " doesn't exist. Please check the name and try again"

    # User already part of the group
    auth.uid = 21
    name = 'test group'
    group = auth.add_user_to_group(email, name)

    # Add a second time
    group = auth.add_user_to_group(email, name)
    assert auth.error == "User " + email + " was already part of group " + name

def check_remove_user_from_group_success():
    auth = AuthModel()
    # User to be added
    email = 'test@test.com'
    # uid of adder
    auth.uid = 61
    # Role of adder
    auth.role = 'admin'
    # Group name to add user to
    name = 'test group'
    group = auth.remove_user_from_group(email, name)
    assert auth.error == None
    

def check_remove_user_from_group_failure():
    # Already have done the same checks (from checking the creation of
    # the group so just have a limited test suite
    # Just need to check when the user wasn't part of the group
    auth = AuthModel()
    email = 'admmin@admin.com'
    auth.uid = 61
    auth.role = 'admin'
    name = 'test group'
    group  = auth.remove_user_from_group(email, name)
    # remove a second time (first time is actually removing)
    group  = auth.remove_user_from_group(email, name)
    assert auth.error == "User " + str(email) + " wasn't part of the group " + str(name) + " no changes made."


def check_get_users_groups():
    # Test a user which has groups

    auth = AuthModel()
    auth.uid = 21
    uid = 21
    email = 'test@test.com'
    name = 'test group 3'
    auth.role = 'admin'
    # Add the user to the group asnd create the group
    group = auth.create_group(name)
    auth.add_user_to_group(email, name)
    groups = auth.get_users_groups(uid)

    print(groups)
    #assert groups == [['test group'], ['test group 3']]

    # remove the user from the group
    group2 = auth.remove_user_from_group(email, name)

    group = auth.delete_group(group2[ID])

    # Test a user which has no groups
    uid = 61
    groups = auth.get_users_groups(uid)
    #assert groups == []

    # Test uid doesn't exist
    uid = 1
    groups = auth.get_users_groups(uid)
    assert groups == []

    # Test uid not a number
    uid = "eggs"
    groups = auth.get_users_groups(uid)

    assert auth.error == error_codes['id']

    # Test uid < 0
    uid = -10
    groups = auth.get_users_groups(uid)
    assert auth.error == error_codes['id']    


def check_get_user_from_group():
    # Test when a user is part of a group
    auth = AuthModel()
    auth.uid = 21
    uid = 21
    name = 'test group'
    email = 'test@test.com'
    auth.role = 'admin'

    # Add the user to the group
    auth.add_user_to_group(email, name)
    user = auth.get_user_from_group(uid, name)
    assert user == [[21]]

    # remove the user from the group
    auth.remove_user_from_group(email, name)

    # Test when user is not part of the group
    auth.uid = 61
    name = 'test group'
    user = auth.get_user_from_group(uid, name)
    assert user == None

    # Test bad group name
    name = '.as.##'
    user = auth.get_user_from_group(uid, name)
    assert auth.error == error_codes['name']    

    # Test group name not a string
    name = 123
    user = auth.get_user_from_group(uid, name)
    assert auth.error == error_codes['name'] 

    # Test uid < 0
    uid = -10
    name = "agreatname"
    user = auth.get_user_from_group(uid, name)
    print(auth.error)
    assert auth.error == error_codes['id']

    # Test uid not an int
    uid = 'eggs'
    user = auth.get_user_from_group(uid, name)
    assert auth.error == error_codes['id']


print("No tests selected to run.")

#test_token_gen()
#test_bad_token()



#test_add_new_user()
#test_add_new_user_failure()
#check_login_success()
"""
check_login_failure()
check_forgot_user_password_success()
check_forgot_user_password_failure()
check_reset_user_password_success()
check_reset_user_password_failure()
test_check_user_by_uid_success()
test_check_user_by_uid_faliure()
check_create_group_success()
check_create_group_failure()
check_get_group_by_name_success()
check_get_group_by_name_failure()
check_add_user_to_group_success()
check_add_user_to_group_failure()
check_remove_user_from_group_success()
check_remove_user_from_group_failure()
check_get_users_groups()
check_get_user_from_group()
"""
