from pathway_view.config import *

from pathway_view.model.datasets import *
from pathway_view.model.auth import AuthModel
from pathway_view.model.base import BaseModel
from pathway_view.model.metabolite_lists import MetaboliteListModel
from shutil import copyfile

# Test dataset
test_ds_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/test_data/datasets/test_metanetx.csv"
# Test list
test_list_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/test_data/lists/ma_names.tsv"

def test_dataset():
    # Test the non uploading part

    # Add some user and groupu to be the owners/user performing this
    auth = AuthModel()
    full_name = "New user"
    email = "newuser@gmail.com"
    auth.full_name = full_name
    auth.email = email
    user = auth.add_new_user()
    uid = int(user[ID])
    auth.role = 'admin' # set the user to be admin
    auth.uid = uid
 
    # Create the group
    name = 'some groups'
    group = auth.create_group(name)
    #print(group)
    gid = group[ID]


    # Test with bad metadata

    ds = DatasetModel()
    ds.name = "some name"
    ds.u_file = None
    ds.u_filename = "test"
    ds.private = 't' #True
    ds.owner_type = 'user'
    ds.owner_id = 'notnum'
    ds.notes = None
    ds.path = 'pathway_view/uploads/datasets/doneexist.tsv'
    ds.raw_metabolite_id_type = 'metanetx_id'
    ds.data_type = 'metabolic'

    #print(ds.parse_metadata())
    assert ds.parse_metadata() == "There was an error with uploaded data."
        
    ds = DatasetModel()
    ds.name = "some name"
    ds.u_filename = "test"
    ds.private = 't'
    ds.owner_type = 'user'
    ds.owner_id = uid
    ds.notes = 'some notes'
    ds.raw_metabolite_id_type = 'hmdb'
    ds.data_type = 'metabolic'
    # Need to have a properly formatted test.tsv at the location
    ds.path = '/var/www/pathway_viewer/pathway_viewer/pathway_view/uploads/datasets/test.tsv'
    # Copy the test file to this path
    copyfile(test_ds_path, ds.path)

    print('Test: saving metadata.')

    datas = ds.add_metadata()
    ds_id = ds.ds_id
    assert isinstance(ds_id, int) == True
    print("id", ds_id)    

    print("-------------------------")
    print('Test: parsing datasets')

    success = ds.parse_dataset()
    print(success)
    #assert success == {'value_errors': {}, 'id_errors': ['MNXM726', 'MNXM940', 'MNXM90', 'MNXM1354', 'MNXM114070', 'MNXM81221', 'MNXM2456', 'MNXM2416', 'MNXM90428', 'MNXM1715', 'MNXM145560', 'MNXM6076', 'MNXM369', 'MNXM147072', 'MNXM976', 'MNXM7658', 'MNXM504', 'MNXM146487', 'MNXM33', 'MNXM461', 'MNXM53799', 'MNXM5281', 'MNXM340', 'MNXM8831', 'MNXM927', 'MNXM89602', 'MNXM46301', 'MNXM2621', 'MNXM12747', 'MNXM59473', 'MNXM1114', 'MNXM92401', 'MNXM1251', 'MNXM90514', 'MNXM146468', 'MNXM1', 'MNXM114230', 'MNXM89724', 'MNXM1486', 'MNXM4594', 'MNXM36534', 'MNXM2139', 'MNXM4997', 'MNXM634', 'MNXM12672', 'MNXM9850', 'MNXM97271', 'MNXM5567', 'MNXM146651', 'MNXM6353', 'MNXM91105', 'MNXM5661', 'MNXM491', 'MNXM89629', 'MNXM2265', 'MNXM458', 'MNXM32863']}

    print("success")

    print("-------------------------")
    print('Test: retrieving a dataset')
    dataset = ds.get_dataset(ds_id, uid)

    # Spreadsheets should have a length of 2 (data and metadata)
    #assert len(dataset) == 2
    print("ds len", len(dataset))

    print('Test: getting datasets belonging to a user')
    # Add a new dataset to a group the user is part of
    # Set up environment
    ret = auth.add_user_to_group(email, name)

    # Add a "new" datset for that group
    group_ds = DatasetModel()
    group_ds.name = "group Dataset"
    group_ds.u_filename = "group test"
    group_ds.private = 't'
    group_ds.owner_type = 'group'
    group_ds.owner_id = gid
    group_ds.notes = 'group notes'
    group_ds.path = '/var/www/pathway_viewer/pathway_viewer/pathway_view/uploads/datasets/test.tsv'
    group_ds.raw_metabolite_id_type = 'bigg_id'
    group_ds.data_type = 'metabolic'

    # copy the file to this destination
    copyfile(test_ds_path, group_ds.path)
    
    group_dataset = group_ds.add_metadata()
    group_dataset_id = group_ds.ds_id
    success = group_ds.parse_dataset()

    all_ds = group_ds.get_all_datasets_for_user(uid)    

    assert all_ds != None
    print(all_ds)

    print('Test: deleting dataset.')
    
    success = ds.delete_dataset(group_dataset_id, uid)
    success = ds.delete_dataset(ds_id, uid)
    assert success == "success"
    print("success", success)

    auth.error = None
    # Clean up the environment
    group = auth.delete_group(int(gid))
    assert auth.error == None
    print("err", auth.error)
   
    success = auth.delete_user(int(uid))
    assert auth.error == None    
    print("err", auth.error)



def test_set_list_id():
    uid = create_user()
    ds_id = create_dataset(uid)
    list_id = create_list(uid)
    
    # Test setting the list id to be the default list ID for
    # a dataset
    dataset_model = DatasetModel()

    success = dataset_model.set_default_list_id(uid, ds_id, list_id)

    print(success)
    
    # Get the list ID 
    list_id = dataset_model.get_default_list_id(ds_id)

    print(list_id)
    # Clean up environment
    list_m = MetaboliteListModel()
    success = list_m.delete_metabolite_list(list_id, uid)
    
    ds_m = DatasetModel()
    success = ds_m.delete_dataset(ds_id, uid)

    auth = AuthModel()
    success = auth.delete_user(int(uid))

    print(success)



def create_list(uid):
    list_m = MetaboliteListModel()

    list_m.path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/uploads/lists/test.tsv"
    # copy the test file to the destination
    copyfile(test_list_path, list_m.path)

    # Fill in the things we would normally get from the users upload
    list_m.name = "alist"
    list_m.private = 't'
    list_m.owner_type = 'user'
    list_m.owner_id = uid
    list_m.raw_metabolite_id_type = 'name'
    list_m.notes = 'some notes'
    list_m.list_type = 'metabolite'
    list_m.col_dict = {'t_statistic': 4, 'p_value': 2, 'z_score':  3, 'name': 0, 'raw_metabolite_id': 0, 'bh_adjusted_p_value': None, 'fold_change': None, 'standard_error': None, 'comments': None}


    # For test.tsv
    #{'t_statistic': 5, 'p_value': 6, 'z_score': None, 'name': 1, 'uniq_id': 2, 'bh_adjusted_p_value': 7, 'fold_change': 8, 'standard_error': 9, 'comments': None}

    print("Test parsing metabolite list metadata:")

    success = list_m.parse_list_metadata()

    print("success", success)
    #assert "success" == success

    # Normally would save the list data to the server at this point
    # as we are using a test file go straight to adding the raw metadata to the db

    print('-------------------------------------')

    print("Test process metadata and add to db:")

    m_list = list_m.process_metabolite_list_metadata()
    list_id = m_list[ID]

    print('list id', m_list)
    return list_id

def create_dataset(uid):
    ds = DatasetModel()
    ds.name = "some name"
    ds.u_filename = "test"
    ds.private = 't'
    ds.owner_type = 'user'
    ds.owner_id = uid
    ds.notes = 'some notes'
    ds.raw_metabolite_id_type = 'hmdb'
    ds.data_type = 'metabolic'
    # Need to have a properly formatted test.tsv at the location
    ds.path = '/var/www/pathway_viewer/pathway_viewer/pathway_view/uploads/datasets/test.tsv'
    # Copy the test file to this path
    copyfile(test_ds_path, ds.path)

    print('Test: saving metadata.')

    datas = ds.add_metadata()
    ds_id = ds.ds_id
    #assert isinstance(ds_id, int) == True
    print("id", ds_id)

    print("-------------------------")
    print('Test: parsing datasets')

    success = ds.parse_dataset()
    print(success)
    #assert success == {'value_errors': {}, 'id_errors': ['MNXM726', 'MNXM940', 'MNXM90', 'MNXM1354',$

    print("success")

    print("-------------------------")
    print('Test: retrieving a dataset')
    dataset = ds.get_dataset(ds_id, uid)

    # Spreadsheets should have a length of 2 (data and metadata)
    #assert len(dataset) == 2
    print("ds len", len(dataset))

    return ds_id

def create_user():
    # Add some user and groupu to be the owners/user performing this
    auth = AuthModel()
    full_name = "New user"
    email = "newuser@gmail.com"
    auth.full_name = full_name
    auth.email = email
    user = auth.add_new_user()
    uid = int(user[ID])
    auth.role = 'admin' # set the user to be admin
    auth.uid = uid

    return uid


def test_base():
    ds_name = 'noname'
    uid = 292

    data = {'name': ds_name, 'owner_id':uid, 'owner_type':'user', 'notes':'na', 'private':'t', 'verified':'f', 'data_type':'hmdb'}
    sql = "INSERT into datasets (name, owner_id, owner_type, notes, private, verified, data_type) VALUES (%(name)s, %(owner_id)s, %(owner_type)s, %(notes)s, %(private)s, %(verified)s, %(data_type)s);"
    base = BaseModel()
    return_val = base.perform_db_query(sql, data, True, False)

    assert return_val == None

    print("Inserted into db")

    # Try getting the same dataset
    sql_select = "SELECT * from datasets where name = %(name)s and owner_id = %(u_id)s;"
    data_select = {'name': ds_name, 'u_id': uid}
    
    dataset = base.perform_db_query(sql_select, data_select, False, True)

    ds_id = dataset[0][0]

    assert dataset[0][1] == ds_name

    print("selected ds from db")

    new_name = 'new_name'
    # Try updating the name of the dataset
    sql_update = "UPDATE datasets set name = %(new_name)s where name = %(old_name)s and owner_id = %(u_id)s;"
    data_update = {'old_name': ds_name, 'new_name': new_name, 'u_id': uid, 'ds_id': ds_id}

    return_val = base.perform_db_query(sql_update, data_update, True, False)

    # Try getting the new dataset        
    sql_select = "SELECT * from datasets where name = %(name)s and owner_id = %(u_id)s;"
    data_select = {'name': new_name, 'u_id': uid}

    updated_ds = base.perform_db_query(sql_select, data_select, False, True)

    assert updated_ds[0][1] == new_name

    print("Updated ds in db")

    # Try deleting the dataset
    sql_delete = "DELETE from datasets where name = %(name)s and owner_id = %(u_id)s;"
    data_delete = {'name': new_name, 'u_id': uid}

    return_val = base.perform_db_query(sql_delete, data_delete, True, False)

    print("Deleted ds from db")

    # Try getting the dataset should return None
    sql_select = "SELECT * from datasets where name = %(name)s and owner_id = %(u_id)s;"
    data_select = {'name': new_name, 'u_id': uid}

    updated_ds = base.perform_db_query(sql_select, data_select, False, True)

    assert len(updated_ds) == 0 

    print("Finished testing base db")

#test_base()
#test_set_list_id()
test_dataset()
