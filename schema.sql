create table chebi_info(
    id SERIAL UNIQUE PRIMARY KEY,
    chebi_id VARCHAR(50),
    name TEXT,
    mass NUMERIC,
    charge NUMERIC,
    formulae TEXT,
    definition TEXT,
    last_updated_date date
);

create table chebi_mapping_to_other_id(
    id SERIAL UNIQUE PRIMARY KEY,
    chebi_id VARCHAR(50),
    other_id TEXT,
    other_id_type VARCHAR(50)
);

create table dataset_chebi_mapping(
    id SERIAL UNIQUE PRIMARY KEY,
    ds_id INT,
    chebi_id VARCHAR(50),
    bigg_id VARCHAR(50),
    raw_metabolite_id TEXT,
    raw_metabolite_type VARCHAR(50),
    verified boolean,
    verified_by_id INT
);

create table data(
    id SERIAL UNIQUE PRIMARY KEY,
    ds_id INT,
    chebi_id VARCHAR(50),
    sample VARCHAR(255),
    value NUMERIC  
);

create table datasets(
    id SERIAL UNIQUE PRIMARY KEY,
    name VARCHAR(255),
    owner_id INT,
    owner_type VARCHAR(50),
    notes TEXT,
    private VARCHAR(50),
    verified boolean,
    veridied_by_id INT    
);

create table metabolite_lists(
    id SERIAL UNIQUE PRIMARY KEY,
    name VARCHAR(255),
    owner_id INT,
    owner_type VARCHAR(50),
    notes TEXT,
    private VARCHAR(50),
    raw_filepath VARCHAR(255)
);

create table metabolite_list_entry(
    id SERIAL UNIQUE PRIMARY KEY,
    list_id INT,
    chebi_id VARCHAR(50),
    raw_metabolite_id VARCHAR(255),
    raw_metabolite_id_type VARCHAR(50),
    name VARCHAR(255),
    p_value REAL,
    t_statistic REAL,
    z_score REAL,
    bh_adjusted_p_value REAL,
    fold_change REAL,
    standard_error REAL,
    comments TEXT
);


create table pathway_lists(
    id SERIAL UNIQUE PRIMARY KEY,
    name VARCHAR(255),
    owner_id INT,
    owner_type VARCHAR(50),
    notes TEXT,
    private VARCHAR(50),
    raw_filepath VARCHAR(255)
);

create table pathway_list_entry(
    id SERIAL UNIQUE PRIMARY KEY,
    list_id INT,
    pathway_id INT,
    name VARCHAR(255),
    p_value REAL,
    z_score REAL,
    comments TEXT
);


create table users(
    id INT PRIMARY KEY, 
    name VARCHAR(255), 
    email VARCHAR(255), 
    password VARCHAR(255)
);

create table groups(
    id INT PRIMARY KEY, 
    name VARCHAR(255)
);

create table group_users(
    id INT PRIMARY KEY, 
    user_id INT, 
    group_id INT, 
    FOREIGN KEY (user_id) REFERENCES users(id), 
    FOREIGN KEY (group_id) REFERENCES groups(id)
);

