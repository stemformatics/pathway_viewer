from pathway_view.config import *

from pathway_view.model.auth import AuthModel


def get_user_input():
    uid = int(input("Please enter the user ID for the token: "))

    # Get the user ID from the input
    print("You entered: ", uid)
    selection = input("(y/n) to continue: ")

    # Return the user ID for clarity
    if selection.lower() == "y":
        return uid

    # Check the admin wants to continue
    else:
        print("Wrong selection. Please try again.")
        return None


def generate_token():
    """

    Create a token for a user.

    Token is automatcally saved and also returned to the admin so they 
    can store or add it to a URL.

    """
    uid = get_user_input()

    # Get the user ID from the input
    if uid:
        auth = AuthModel() 
        token = auth.generate_login_token(uid)

        print("Token has been saved to user: ", uid, " it was: ", token)



def get_token_for_user():
    uid = get_user_input()

    if uid:
        auth = AuthModel()
        token = auth.get_token_for_user(uid)

        print("Token for user: ", uid, " was: ", token)




def delete_token():

    uid = get_user_input()

    if uid:
        auth = AuthModel()
        token = auth.delete_token_for_user(uid)

        print("Token for user: ", uid, " has been deleted.")


def choose_action():
    action = int(input("Select: \n 1 for generating user token \n 2 for getting user token \n 3 for removing user token \n Please enter your choice: "))

    if action == 1:
        return generate_token()

    if action == 2:
        return get_token_for_user()

    if action == 3:
        return delete_token()

    print("Incorrect response, terminating.")
    return None


choose_action()
