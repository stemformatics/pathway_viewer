from pathway_view.config import *

from pathway_view.model.datasets import *
from pathway_view.model.auth import AuthModel
from pathway_view.model.base import BaseModel

def get_user_input():
    uid = int(input("Please enter your user ID (must have access to the dataset and be an admin): "))

    ds_id = int(input("Please enter the dataset ID: "))

    print("You entered, user_id: ", uid, " dataset_id: ", ds_id)
    selection = input("(y/n) to continue: ")


    if selection.lower() != "y":
        print("Wrong selection. Please try again.")
        return None

    else:
        return {'uid': uid, 'ds_id': ds_id}


def set_dataset_owner():
    """
    Update the owner ID of a dataset.

    When the dataset is uploaded online the default owner ID is that
    of the user who uplaoded it. It can be chosen to be a group.

    Otherwise may want to update the owner ID useing this function.
    """
    base = BaseModel()
   # old_owner_id = int(input("Please enter the old owner id (must have access to the dataset and be an admin): "))

    new_owner_id = int(input("Please enter the new owner id: "))

    ds_id = int(input("Please enter the dataset ID: "))
    
    print("You entered, new_owner_id: ", new_owner_id, " and dataset_id: ", ds_id)
    selection = input("(y/n) to continue: ")


    if selection.lower() != "y":
        print("Wrong selection. Please try again.")
        return None
 
    sql_update = "UPDATE datasets set owner_id = %(new_owner_id)s where id = %(ds_id)s;"
    data_update = {'new_owner_id': new_owner_id, 'ds_id': ds_id}

    return_val = base.perform_db_query(sql_update, data_update, True, False)

    print("Owner ID updated to: ", new_owner_id, " where ds_id: ", ds_id)
    


def delete_dataset():
    """
    Deletes a dataset.

    Need to delete using the owner_id of that dataset.
    Owner/user must also have admin privliges
    """
    user_input = get_user_input() 
    
    if not user_input:
        return
    
    ds_id = user_input['ds_id']
    uid = user_input['uid']

    ds_model = DatasetModel()

    print("Dataset deleted: ", ds_model.delete_dataset(ds_id, uid))


def get_dataset(metadata_only):
    """
    Gets the dataset given the ID and the user ID.

    If metadata only is selected it will just return 
    the metadata.

    """
    user_input = get_user_input()

    if not user_input:
        return

    ds_id = user_input['ds_id']
    uid = user_input['uid']

    ds_model = DatasetModel()
    
    dataset = ds_model.get_dataset_all_metadata(ds_id, uid)
    
    if isinstance(dataset, str):
        # Means an error occured, print the error and
        # return.
        print(dataset)
        return

    if metadata_only:
        print(dataset['metadata'])
    
    else:
        print(dataset)



def choose_action():
    action = int(input("Select: \n 1 for get dataset metadata \n 2 for get dataset (metadata and data) \n 3 for set dataset owner \n 4 for delete dataset\n Please enter your choice: "))

    if action == 1:
        return get_dataset(True)

    if action == 2:
        return get_dataset(False)

    if action == 3:
        return set_dataset_owner()

    if action == 4:
        return delete_dataset()

    print("Incorrect response, terminating.")
    return None


choose_action()
