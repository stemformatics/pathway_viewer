from pathway_view.config import *

from pathway_view.model.metabolite_lists import MetaboliteListModel
from pathway_view.model.auth import AuthModel
from pathway_view.model.base import BaseModel

def get_user_input():
    uid = int(input("Please enter your user ID (must have access to the dataset and be an admin): "))

    list_id = int(input("Please enter the list ID: "))

    print("You entered, user_id: ", uid, " list_id: ", list_id)
    selection = input("(y/n) to continue: ")


    if selection.lower() != "y":
        print("Wrong selection. Please try again.")
        return None

    else:
        return {'uid': uid, 'list_id': list_id}


def set_list_owner():
    """
    Update the owner ID of a list.

    When the list is uploaded online the default owner ID is that
    of the user who uplaoded it. It can be chosen to be a group.

    Otherwise may want to update the owner ID using this function.
    """
    base = BaseModel()
   # old_owner_id = int(input("Please enter the old owner id (must have access to the dataset and be an admin): "))

    new_owner_id = int(input("Please enter the new owner id: "))

    list_id = int(input("Please enter the list ID: "))
    
    print("You entered, new_owner_id: ", new_owner_id, " and metabolite_list_id: ", list_id)
    selection = input("(y/n) to continue: ")


    if selection.lower() != "y":
        print("Wrong selection. Please try again.")
        return None
 
    sql_update = "UPDATE metabolite_lists set owner_id = %(new_owner_id)s where id = %(list_id)s;"
    data_update = {'new_owner_id': new_owner_id, 'list_id': list_id}

    return_val = base.perform_db_query(sql_update, data_update, True, False)

    print("Owner ID updated to: ", new_owner_id, " where metabolite_list_id: ", list_id)
    


def delete_list():
    """
    Deletes a list.

    Need to delete using the owner_id of that dataset.
    Owner/user must also have admin privliges
    """
    user_input = get_user_input() 
    
    if not user_input:
        return
    
    list_id = user_input['list_id']
    uid = user_input['uid']

    list_model = MetaboliteListModel()

    print("Metabolite List deleted: ", list_model.delete_metabolite_list(list_id, uid))


def get_list(metadata_only):
    """
    Gets a list given the ID and the user ID.

    If metadata only is selected it will just return 
    the metadata.

    """
    user_input = get_user_input()

    if not user_input:
        return

    list_id = user_input['list_id']
    uid = user_input['uid']

    list_model = MetaboliteListModel()
    
    metabolite_list = list_model.get_processed_metabolite_list(list_id, uid)
    
    if isinstance(metabolite_list, str):
        # Means an error occured, print the error and
        # return.
        print(metabolite_list)
        return

    if metadata_only:
        print(metabolite_list['metadata'])
    
    else:
        print(metabolite_list)



def choose_action():
    action = int(input("Select: \n 1 for get list metadata \n 2 for get list (metadata and data) \n 3 for set list owner \n 4 for delete list\n Please enter your choice: "))

    if action == 1:
        return get_list(True)

    if action == 2:
        return get_list(False)

    if action == 3:
        return set_list_owner()

    if action == 4:
        return delete_list()

    print("Incorrect response, terminating.")
    return None


choose_action()
