from pathway_view.config import *

from pathway_view.model.datasets import DatasetModel


def get_user_input():
    uid = int(input("Please enter your user ID (must have access to both the dataset and the list): "))

    ds_id = int(input("Please enter the dataset ID: "))
  
    list_id = int(input("Please enter the list ID: "))
    
    print("You entered, user_id: ", uid, " dataset_id: ", ds_id, " and list_id: ", list_id)
    selection = input("(y/n) to continue: ")


    if selection.lower() != "y":
        print("Wrong selection. Please try again.")
        return None

    else:
        return {'uid': uid, 'ds_id': ds_id, 'list_id': list_id}


def set_default_list_id():
    """

    Sets the default list ID for a dataset.

    The user must have access to both the list and the dataset.
    i.e. must either be part of the group that owns it or the direct
    owner of that entity.
    
    """
    user_input = get_user_input()

    list_id = user_input['list_id']
    ds_id = user_input['ds_id']
    uid = user_input['uid']

    # If we have user input, set the default list ID
    if user_input:
        ds_model = DatasetModel()
        success_or_error_message = ds_model.set_default_list_id(uid, ds_id, list_id)

        print(success_or_error_message)


def get_default_list_id():
    # Get the dataset of interest ID from the user
    ds_id = int(input("Please enter the dataset ID: "))

    print("You entered, dataset_id: ", ds_id)
    selection = input("(y/n) to continue: ")


    if selection.lower() != "y":
        print("Wrong selection. Please try again.")
        return None

    ds_model = DatasetModel()
    print("Default list_id: ", ds_model.get_default_list_id(ds_id))



def delete_default_list_id():
    uid = int(input("Please enter your user ID (must have access to both the dataset and the list): "))

    ds_id = int(input("Please enter the dataset ID: "))
    
    print("You entered, user_id: ", uid, " dataset_id: ", ds_id, " confirm removal of list_id.")
    selection = input("(y/n) to continue: ")


    if selection.lower() != "y":
        print("Wrong selection. Please try again.")
        return None

    ds_model = DatasetModel()
    print("Removed list_id: ", ds_model.delete_default_list_id(uid, ds_id))


def choose_action():
    action = int(input("Select: \n 1 for set default list id \n 2 for get default list id \n 3 for remove default list id \n Please enter your choice: "))

    if action == 1:
        return set_default_list_id()

    if action == 2:
        return get_default_list_id()
    
    if action == 3: 
        return delete_default_list_id()

    print("Incorrect response, terminating.")
    return None



# Let the user choose which action
choose_action()
